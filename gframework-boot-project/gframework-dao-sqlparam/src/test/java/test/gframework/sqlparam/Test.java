package test.gframework.sqlparam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gframework.sqlparam.SqlParam;

public class Test {
	
	public static void main(String args[]) {
		List<Object> list = new ArrayList<>();
		Map<String,Object> map = new HashMap<>();
		SqlParam sp = SqlParam.and()
				.eq("a", "b").lt("age", 13).gt("name", "hello")
				.in("carid",new Object[]{1,23,"123"})
				.andNext(SqlParam.or()
						.ge("a", "b").isNull("nnn").isNotNull("fff")
						.orNext(SqlParam.and()
								.betweenAnd("sal", 1000, 2000)))
				.orNext(SqlParam.and()
						.like("name", "s%").notLike("name", "%s").ne("name", "world"))
				;
		sp.andNext(SqlParam.or()
				.le("name", 12).ge("name", new Date()).in("name", Arrays.asList(1,2,3)));
		System.out.println(sp.getSql(map));
		System.out.println(map);
		System.out.println();
		System.out.println(sp.getSql(list));
		System.out.println(list);
	}

}
