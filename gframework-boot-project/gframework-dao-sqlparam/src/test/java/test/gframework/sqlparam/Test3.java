package test.gframework.sqlparam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gframework.sqlparam.ParamMode;
import com.gframework.sqlparam.SqlParam;
import com.gframework.sqlparam.ParamMode.LikeMode;
import com.gframework.sqlparam.ParamMode.ParamModeType;

public class Test3 {
	public static void main(String[] args) {
		List<Object> list = new ArrayList<>();
		Map<String,Object> map = new HashMap<>();
		SqlParam sp = SqlParam.buildByParamModeAnd(new Dto());
		System.out.println(sp.getSql(list));
		System.out.println(list);
		System.out.println();
		System.out.println(sp.getSql(map));
		System.out.println(map);
		System.out.println();
		System.out.println(sp.getParamSize());
		System.out.println(sp.getParamList());
	}
}


class Dto {

	@ParamMode(modeType = ParamModeType.LIKE, likeMode = LikeMode.ALL)
	private String name = "Hello";
	@ParamMode(modeType = ParamModeType.EQ)
	private int age = 13;
	@ParamMode(modeType = ParamModeType.LE)
	private Date birthday = new Date();
	@ParamMode(modeType = ParamModeType.GE)
	private double sal = 100.23;
	@ParamMode(modeType = ParamModeType.NOTLIKE, likeMode = LikeMode.LEFT)
	private String message = "你好";
	@ParamMode(modeType = ParamModeType.NE)
	private String remark = "描述";

	@ParamMode(modeType = ParamModeType.IN)
	private String[] ids = {"A","B","C"};
	@ParamMode(modeType = ParamModeType.NOTIN)
	private Collection<Integer> values = Arrays.asList(100,200,300);

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public double getSal() {
		return this.sal;
	}

	public void setSal(double sal) {
		this.sal = sal;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	public Collection<Integer> getValues() {
		return this.values;
	}

	public void setValues(Collection<Integer> values) {
		this.values = values;
	}

}