package test.gframework.sqlparam;

import com.gframework.sqlparam.MybatisParamNameCacheGenerator;

public class Test2 {
	public static void main(String[] args) {
		MybatisParamNameCacheGenerator g = new MybatisParamNameCacheGenerator();
		long start = System.currentTimeMillis();
		System.out.println("start ...");
		for (int x = 0 ; x < 21474836 ; x ++) {
			String s = g.next();
			if (!s.equals("#{!" + x + "}")) {
				System.out.println("error " + s);
				break ;
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("end ... " + (end - start));
	}
}
