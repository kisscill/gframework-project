package com.gframework.sqlparam;

import java.io.Serializable;
import java.util.function.Function;

/**
 * 一个支持序列化的供给形函数式接口
 * @since 2.0.0
 * @author Ghwolf
 *
 * @param <T> 参数类型/执行当前对象this
 * @param <R> 返回值类型
 */
public interface SerFunction<T,R> extends Function<T,R>,Serializable {

}
