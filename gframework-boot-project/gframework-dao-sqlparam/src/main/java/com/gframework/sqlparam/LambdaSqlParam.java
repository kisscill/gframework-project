package com.gframework.sqlparam;

import java.io.Serializable;

import com.gframework.lang.ObjectRange;

/**
 * 这是一个必须支持lambda表达式的SqlParam处理操作标准接口.
 * 
 * @since 2.0.0
 * @author Ghwolf
 *
 */
interface LambdaSqlParam {
	/**
	 * 添加一个等值判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam eq(SerSupplier<Object> supplier) ;

	/**
	 * 添加一个不等判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam ne(SerSupplier<Object> supplier) ;
	
	/**
	 * 添加一个IN判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * <br><strong>特别注意，此对象可以返回一个数组或者一个Collection集合接口的子类。也可以是一个单独的对象，此时会按照长度为1的数组来处理！</strong>
	 * @return 返回当前调用对象
	 */
	public SqlParam in(SerSupplier<Object> supplier) ;


	/**
	 * 添加一个NOT IN判断规则
	 *  
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * <br><strong>特别注意，此对象可以返回一个数组或者一个Collection集合接口的子类。也可以是一个单独的对象，此时会按照长度为1的数组来处理！</strong>
	 * @return 返回当前调用对象
	 */
	public SqlParam notIn(SerSupplier<Object> supplier) ;

	/**
	 * 添加一个LIKE判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam like(SerSupplier<String> supplier) ;

	/**
	 * 添加一个NOT LIKE判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam notLike(SerSupplier<String> supplier) ;

	/**
	 * 添加一个IS NULL判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，仅用于获取名称，不用于获取值
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * @return 返回当前调用对象
	 */
	public SqlParam isNull(SerSupplier<Object> supplier) ;

	/**
	 * 添加一个IS NOT NULL判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，仅用于获取名称，不用于获取值
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * @return 返回当前调用对象
	 */
	public SqlParam isNotNull(SerSupplier<Object> supplier) ;

	/**
	 * 添加一个小于{@code <}判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam lt(SerSupplier<Object> supplier);

	/**
	 * 添加一个小于等于{@code <=}判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam le(SerSupplier<Object> supplier);

	/**
	 * 添加一个大于{@code >}判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam gt(SerSupplier<Object> supplier);

	/**
	 * 添加一个大于等于{@code >=}判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值可以是null，返回null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 */
	public SqlParam ge(SerSupplier<Object> supplier);

	/**
	 * 添加一个BETWEEN AND判断规则
	 * 
	 * @param supplier 供给型函数式接口，例如可以是getter方法。
	 * <br>参数不能为null，但是返回值或ObjectRange的返回值可以是null，如果存在null则什么都不做
	 * <br>名称会自动去掉开头的"is"或"get"并进行驼峰转下换线命名规范转换。
	 * <br>你也可以通过在此方法上添加{@link javax.persistence.Column}注解来强制指定名称。
	 * <br>方法值则会作为条件参数。
	 * @return 返回当前调用对象
	 * @param <T> 泛型的目的保证start和end数据类型一致
	 */
	public <T extends Serializable> SqlParam betweenAnd(SerSupplier<ObjectRange<T>> supplier) ;
}
