package com.gframework.sqlparam;

import java.lang.reflect.Member;
import java.util.Collection;

/**
 * in匹配操作
 *
 * @since 1.0.0
 * @author Ghwolf
 */
class InModeHandler extends AbstractParamModeHandler {

	protected InModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	protected boolean validateReturnType(ParamMode paramMode, Class<?> returnType) {
		return Collection.class.isAssignableFrom(returnType) || returnType.isArray();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void handle(SqlParam sp, Object value) {
		if (value instanceof Collection) {
			sp.in(this.column, (Collection<Object>) value);
		} else if (value.getClass().isArray()) {
			sp.in(this.column, (Object[]) value);
		} else {
			throw new ParamModeException("in操作值只能是Object[]或Collection集合类型对象！但实际上是：" + value.getClass());
		}
	}

}
