package com.gframework.sqlparam;

/**
 * 字符串包装器，用于对一个字符串前后缀进行处理
 * @since 2.0.0
 * @author Ghwolf
 */
interface StringWrapper {
	/**
	 * 处理一个字符串，返回包装后的新字符串
	 * @param str 要处理的字符串
	 * @return 返回处理后的字符串
	 */
	public String wrapper(String str);
}
