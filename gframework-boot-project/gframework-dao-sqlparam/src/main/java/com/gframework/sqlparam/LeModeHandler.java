package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 小于等于匹配操作
 *
 * @since 1.0.0
 * @author Ghwolf
 */
class LeModeHandler extends AbstractParamModeHandler {

	protected LeModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	public void handle(SqlParam sp, Object value) {
		sp.le(this.column, value);
	}

}
