package com.gframework.sqlparam;

import org.springframework.core.NestedRuntimeException;

/**
 * 错误的设置或绑定了参数，则抛出此异常。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class BadParamException extends NestedRuntimeException {

	public BadParamException(String msg) {
		super(msg);
	}

	public BadParamException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
