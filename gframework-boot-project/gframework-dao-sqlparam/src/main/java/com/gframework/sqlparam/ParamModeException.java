package com.gframework.sqlparam;

import org.springframework.core.NestedRuntimeException;

/**
 * 使用 {@link ParamMode} 注解时，如果注解配置错误 或者在执行相关操作出现异常时，则会出现此异常.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class ParamModeException extends NestedRuntimeException {

	public ParamModeException(String msg) {
		super(msg);
	}

	public ParamModeException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
