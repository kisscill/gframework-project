package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 大于匹配操作
 *
 * @since 1.0.0
 * @author Ghwolf
 */
class GtModeHandler extends AbstractParamModeHandler {

	protected GtModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	public void handle(SqlParam sp, Object value) {
		sp.gt(this.column, value);
	}

}
