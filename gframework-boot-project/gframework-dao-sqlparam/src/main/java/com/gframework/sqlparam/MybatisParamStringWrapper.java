package com.gframework.sqlparam;

/**
 * mybatis sql参数在sql中对参数的包装器
 * @since 2.0.0
 * @author Ghwolf
 */
class MybatisParamStringWrapper implements StringWrapper{
	private static final MybatisParamStringWrapper INSTANCE = new MybatisParamStringWrapper();
	private MybatisParamStringWrapper(){}
	public static MybatisParamStringWrapper get(){
		return INSTANCE ;
	}
	@Override
	public String wrapper(String str) {
		return "#{" + str + "}";
	}

}
