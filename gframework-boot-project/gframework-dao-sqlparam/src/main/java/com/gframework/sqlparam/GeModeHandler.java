package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 大于等于匹配操作
 *
 * @since 1.0.0
 * @author Ghwolf
 */
class GeModeHandler extends AbstractParamModeHandler {

	protected GeModeHandler(ParamMode paramMode,Member member) {
		super(paramMode, member);
	}

	@Override
	public void handle(SqlParam sp, Object value) {
		sp.ge(this.column, value);
	}

}
