package com.gframework.sqlparam;

import com.gframework.annotation.ThreadSafe;

/**
 * 这是一个线程安全且唯一的sql标记名称生成器.
 * <p>此生成器固定返回 <code>"?"</code> ，因为在基本的jdbc中，采用 <code>"?"</code> 问号进行预编译参数的填充。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@ThreadSafe
public final class SqlMarkNameGenerator implements NameGenerator{
	
	private static final SqlMarkNameGenerator INSTANCE = new SqlMarkNameGenerator();
	
	private SqlMarkNameGenerator(){}
	
	/**
	 * 获取本类对象
	 * @return 返回本类对象
	 */
	public static SqlMarkNameGenerator build(){
		return INSTANCE;
	}

	@Override
	public String next() {
		return "?";
	}

}
