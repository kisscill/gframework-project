package com.gframework.sqlparam;

/**
 * 参数名称生成器.
 * 每一次调用都可以生成一个唯一的完全不同的名称。
 * 此名称主要用于做sql参数绑定。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see MybatisParamNameCacheGenerator
 *
 */
public interface NameGenerator {

	/**
	 * 取得下一个名称
	 * @return 返回下一个名称
	 */
	public String next() ;
	
}
