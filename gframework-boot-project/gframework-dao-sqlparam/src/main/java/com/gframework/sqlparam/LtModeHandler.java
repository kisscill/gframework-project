package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 小于匹配操作
 *
 * @since 1.0.0
 * @author Ghwolf
 */
class LtModeHandler extends AbstractParamModeHandler {

	protected LtModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	public void handle(SqlParam sp, Object value) {
		sp.lt(column, value);
	}

}
