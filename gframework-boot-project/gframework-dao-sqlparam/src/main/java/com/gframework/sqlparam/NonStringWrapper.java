package com.gframework.sqlparam;

/**
 * 一个什么都不做的字符串包装器
 * @since 2.0.0
 * @author Ghwolf
 */
class NonStringWrapper implements StringWrapper{
	private static final NonStringWrapper INSTANCE = new NonStringWrapper();
	private NonStringWrapper(){}
	public static NonStringWrapper get(){
		return INSTANCE ;
	}
	@Override
	public String wrapper(String str) {
		return str;
	}

}
