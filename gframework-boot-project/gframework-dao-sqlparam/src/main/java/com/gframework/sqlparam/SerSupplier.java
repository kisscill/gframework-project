package com.gframework.sqlparam;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * 一个支持序列化的供给形函数式接口
 * @since 2.0.0
 * @author Ghwolf
 *
 * @param <T> 返回值类型
 */
public interface SerSupplier<T> extends Supplier<T>,Serializable {

}
