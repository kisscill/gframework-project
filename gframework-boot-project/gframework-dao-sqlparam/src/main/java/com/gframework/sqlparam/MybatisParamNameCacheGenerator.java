package com.gframework.sqlparam;


/**
 * 一个线程不安全的，有缓存的基于mybatis参数命名风格的名称生成器.
 * 本类生成的名称是以感叹号"!"开头，后面的数字会一直递增，
 * <p>如：<br>
 * #{!0},#{!1},#{!2},#{!3},...。<br>
 * 一个新的本类对象会从0开始生成。
 * </p>
 * 
 * <p>
 * 有缓存：如同Integer对{@code -128 ~ 127}存在缓存一样，
 * 对于前几位名称，不会重复生产字符串，而是将其缓存重用。
 * </p>
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public class MybatisParamNameCacheGenerator implements NameGenerator{
	/**
	 * 名称缓存
	 */
	private static final String[] NAME_CACHE ;
	private static final String FLAG = "!";
	static {
		NAME_CACHE = new String[32];
		for (int x = 0 ; x < NAME_CACHE.length ; x ++) {
			NAME_CACHE[x] = FLAG + x ;
		}
	}
	
	private int current = 0;
	
	@Override
	public String next() {
		String n;
		if (current < NAME_CACHE.length) {
			n = NAME_CACHE[current];
		} else {
			n = FLAG + current;
		}
		current ++;
		return n;
	}

}
