package com.gframework.sqlparam;

import java.lang.reflect.InvocationTargetException;

/**
 * 参数判断模式具体实施操作接口.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@FunctionalInterface
interface ParamModeHandler {

	/**
	 * 将指定对象的特定内容设置到SqlParam中。
	 * 
	 * @param sp SqlParam类实例化对象
	 * @param obj 存储参数信息的主体对象
	 */
	public void doSet(SqlParam sp, Object obj)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;

}
