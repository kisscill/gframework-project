package com.gframework.sqlparam;

import java.io.Serializable;

/**
 * {@code between...and...} 条件参数封装对象<br>
 *
 * @since 1.0.0
 * @author Ghwolf
 *
 */
class BetweenAndQuery<E extends Serializable> implements Serializable{
	private static final long serialVersionUID = 8358080144954283511L;
	/**
	 * 起始值
	 */
	private E begin;
	/**
	 * 结束值
	 */
	private E end;

	public BetweenAndQuery() {
		super();
	}

	public BetweenAndQuery(E begin, E end) {
		super();
		this.begin = begin;
		this.end = end;
	}

	public E getBegin() {
		return this.begin;
	}

	public void setBegin(E begin) {
		this.begin = begin;
	}

	public E getEnd() {
		return this.end;
	}

	public void setEnd(E end) {
		this.end = end;
	}

}
