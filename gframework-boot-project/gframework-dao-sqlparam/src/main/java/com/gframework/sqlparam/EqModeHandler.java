package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 等值匹配操作
 *
 * @since 1.0.0
 * @author Ghwolf
 *
 */
class EqModeHandler extends AbstractParamModeHandler{
	
	protected EqModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	public void handle(SqlParam sp, Object value) {
		sp.eq(this.column, value);
	}

}
