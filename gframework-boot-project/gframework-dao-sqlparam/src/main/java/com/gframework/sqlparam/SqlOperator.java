package com.gframework.sqlparam;

/**
 * sql运算符
 * @since 2.0.0
 * @author Ghwolf
 */
interface SqlOperator {

	/**
	 * where条件and连接符
	 */
	String AND_STR = " and ";
	/**
	 * where条件or连接符
	 */
	String OR_STR = " or ";
	
	String EQ_STR = " = ";
	String NE_STR = " != ";
	
	String LT_STR = " < ";
	String LE_STR = " <= ";
	String GT_STR = " > ";
	String GE_STR = " >= ";
	
	String IN_STR = " IN ";
	String NI_STR = " NOT IN ";
	String LIKE_STR = " LIKE ";
	String NOT_LIKE_STR = " NOT LIKE ";
	
	String IS_NULL_STR = " IS NULL";
	String IS_NOT_NULL_STR = " IS NOT NULL";
	
}
