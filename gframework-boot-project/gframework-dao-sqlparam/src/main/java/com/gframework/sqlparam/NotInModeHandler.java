package com.gframework.sqlparam;

import java.lang.reflect.Member;
import java.util.Collection;

/**
 * notin匹配操作
 *
 * @since 1.0.0 
 * @author Ghwolf
 */
class NotInModeHandler extends AbstractParamModeHandler {

	protected NotInModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	protected boolean validateReturnType(ParamMode paramMode, Class<?> returnType) {
		return Collection.class.isAssignableFrom(returnType) || returnType.isArray();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handle(SqlParam sp, Object value) {
		if (value instanceof Collection) {
			sp.notIn(this.column, (Collection<Object>) value);
		} else if (value.getClass().isArray()) {
			sp.notIn(this.column, (Object[]) value);
		} else {
			throw new ParamModeException("not in操作值只能是Object[]或Collection集合类型对象！但实际上是：" + value.getClass());
		}
	}
	
}
