package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 模糊查询操作
 *
 * @since 1.0.0
 * @author Ghwolf
 *
 */
class LikeModeHandler extends AbstractParamModeHandler{

	/**
	 * 模糊查询类型
	 */
	private ParamMode.LikeMode likeMode;
	
	protected LikeModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
		this.likeMode = paramMode.likeMode();
	}
	
	@Override
	protected boolean validateReturnType(ParamMode paramMode, Class<?> returnType) {
		return returnType == String.class;
	}
	
	@Override
	public void handle(SqlParam sp, Object value) {
		if (value == null) return ;
		sp.like(column, this.likeMode.handleValue(value.toString()));
	}

}
