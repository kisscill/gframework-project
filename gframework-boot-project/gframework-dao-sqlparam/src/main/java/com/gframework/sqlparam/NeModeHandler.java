package com.gframework.sqlparam;

import java.lang.reflect.Member;

/**
 * 不等值匹配操作
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
class NeModeHandler extends AbstractParamModeHandler {

	protected NeModeHandler(ParamMode paramMode, Member member) {
		super(paramMode, member);
	}

	@Override
	public void handle(SqlParam sp, Object value) {
		sp.ne(this.column, value);
	}

}
