package com.gframework.boot.mvc.controller.session.cas;

import javax.servlet.http.HttpServletRequest;

import org.jasig.cas.client.validation.Assertion;

import com.gframework.boot.mvc.controller.session.ConversationIdentity;
import com.gframework.boot.mvc.controller.session.interceptor.ConversationCreater;

/**
 * 默认的用户会话信息创建类
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see CasSessionUserCreateFilter 配置bean
 * @see DefaultCasSessionUser
 */
class DefaultConversationCreater implements ConversationCreater{

	@Override
	public ConversationIdentity createConversation(Object principal, HttpServletRequest request) {
		if (principal instanceof Assertion) {
			return new DefaultCasSessionUser((Assertion) principal, request);
		} else {
			throw new UnsupportedOperationException("用户凭证基础类对象不是Assertion类型或为空，请检查你的项目是否使用了cas且当前版本基础cas凭证信息封装接口是Assertion。");
		}
	}

}
