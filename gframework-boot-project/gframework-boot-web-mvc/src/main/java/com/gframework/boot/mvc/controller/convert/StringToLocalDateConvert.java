package com.gframework.boot.mvc.controller.convert;

import java.time.LocalDate;

import org.springframework.core.convert.converter.Converter;

/**
 * 字符串转日期转换器.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class StringToLocalDateConvert implements Converter<String, LocalDate> {

	private final UnifiedDateConverter dateConvert;

	public StringToLocalDateConvert(UnifiedDateConverter dateConvert) {
		this.dateConvert = dateConvert;
	}

	@Override
	public LocalDate convert(String source) {
		return this.dateConvert.stringToLocalDate(source);
	}

}
