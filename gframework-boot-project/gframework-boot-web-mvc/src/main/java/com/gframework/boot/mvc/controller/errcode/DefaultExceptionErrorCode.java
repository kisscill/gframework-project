package com.gframework.boot.mvc.controller.errcode;

import java.util.Map;

import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 本类中包含有一些在代码中写死的，也是永恒不会变的一些异常和状态号的映射关系.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
class DefaultExceptionErrorCode{
	
	private DefaultExceptionErrorCode(){}

	static void loadDefault(Map<Class<? extends Throwable>, Integer> map){
		// 找不到路径
		map.put(NoHandlerFoundException.class, 404);
	}

}
