package com.gframework.boot.mvc.controller.session.interceptor;

import org.springframework.core.NestedRuntimeException;


/**
 * 用户会话信息创建异常.
 * <p>例如创建的用户类型在某些条件下必须符合一些要求，当然大部分时候都是会兼容全部类型的。
 * <p>或者返回值是null等信息。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@SuppressWarnings("serial")
public class ConversationCreaterException extends NestedRuntimeException {

	public ConversationCreaterException(String msg) {
		super(msg);
	}
	
	public ConversationCreaterException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
