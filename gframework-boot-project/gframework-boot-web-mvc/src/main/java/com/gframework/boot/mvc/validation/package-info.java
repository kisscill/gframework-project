/**
 * 此包中存放的是扩展JSR验证注解.
 * <p>
 * <strong>自定义注解方式：</strong>
 * <ol>
 * <li>编写一个RUNTIME的注解</li>
 * <li>给予@Constraint()注解，此注解用于声明这个注解注释的属性由哪个类来进行验证处理。
 * 而用于验证的这个类必须实现{@link javax.validation.ConstraintValidator}接口。其中第一个泛型表示注解类型，第二个表示验证数据类型（支持验证该类型及其所有子类型，如果配置的类型不是该类型或子类型，则抛出异常）。</li>
 * <li>
 * 自定义注解必须至少包含以下三个方法：
 * <ol>
 * 	<li>String message() default ""; 定义错误内容描述信息，用 "{}" 括起来表示读取资源文件内容</li>
 * 	<li>Class<?>[] groups() default {}; 定义组，可以在验证的时候设置组，这样只会验证组相同的字段。</li>
 * 	<li>Class<? extends Payload>[] payload() default {};</li>
 * </ol>
 * </li>
 * <li>
 * 在2.0版本，可以重复定义多个同样的注解进行多重验证。但是之前版本可能有一种方法，更为合理，但是2.0版本测试无效：
 * <pre>
 * public {@code @interface} NotNull {
 *	  {@code @Target}({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
 *	  {@code @Retention}(RUNTIME)
 *	  {@code @Documented}
 *	  {@code @interface} List {
 *		  NotNull[] value();
 *	  }
 * }
 * </pre>
 * 每一个注解中药定义一个List内部注解，通过标记次注解，并在这个注解内配置多个NotNull，就可以实现多重验证。但是2.0测试无效。原因未知
 * </li>
 * <li>验证器只会被实例化一次，复写init*方法，可以在初始化的时候被调用，同时获取注解类型。</li>
 * </ol>
 * </p>
 * <pre>
 * springmvc开启验证的方法： 
 * 方法bean参数上增加 {@link javax.validation.Valid}注解可以开启验证，方法普通参数需要在类上增加{@link org.springframework.validation.annotation.Validated}注解可以开启验证
 * 手动验证的方法：
 * 注入  {@link javax.validation.Validator} bean，执行 validate方法即可。如果有返回值，则说明验证失败，返回内容是错误信息。如果注解配置错误，则抛出异常。也可以只验证其中某一个属性
 * </pre> 
 * <p>
 * <strong>【关于验证注解的继承关系】</strong><br>
 * 验证注解可以用于属性上，也可以用于getter方法上，建议放到getter方法上，因为注解是可以继承的。如果子父类的getter方法上都有验证注解，那么两个都会进行验证，只要有一个不符合就不通过。
 * </p>
 * 
 * @author Ghwolf
 */
package com.gframework.boot.mvc.validation;
