package com.gframework.boot.mvc.controller.convert;

import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/**
 * 字符串转日期转换器.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class StringToDateConvert implements Converter<String, Date>{

	private final UnifiedDateConverter dateConvert ;
	
	public StringToDateConvert(UnifiedDateConverter dateConvert){
		this.dateConvert = dateConvert;
	}
	
	@Override
	public Date convert(String source) {
		return this.dateConvert.stringToDate(source);
	}

}
