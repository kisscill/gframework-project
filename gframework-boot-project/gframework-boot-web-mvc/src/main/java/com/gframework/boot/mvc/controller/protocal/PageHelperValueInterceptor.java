package com.gframework.boot.mvc.controller.protocal;

import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;

import com.gframework.boot.mvc.entity.dto.PageDataDTO;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * PageHelper的{@link Page}对象 拦截处理类.
 * <p>因为Page类是ArrayList的子类，所以会被当作集合处理，这会有问题。
 * 因此次拦截器将他转换为合理的{@link PageDataDTO}类返回。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@ConditionalOnMissingBean(PageHelperValueInterceptor.class)
@ConditionalOnClass({Page.class,PageHelper.class})
public class PageHelperValueInterceptor implements ControllerReturnValueInterceptor{
	
	public PageHelperValueInterceptor(){
		LoggerFactory.getLogger(PageHelperValueInterceptor.class).info(
				"====> 已启用 PageHelperValueInterceptor 类，PageHelper返回的{}类对象可直接通过controller返回，将会自动转换为{}类对象返回！",
				Page.class.getName(), PageDataDTO.class.getName());
	}

	@Override
	public boolean supportInterceptor(Class<?> returnValueType) {
		return Page.class.isAssignableFrom(returnValueType);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object doInterceptor(Object obj) {
		return new PageDataDTO<>((Page)obj);
	}

	@Override
	public int getOrder() {
		return 100;
	}

}
