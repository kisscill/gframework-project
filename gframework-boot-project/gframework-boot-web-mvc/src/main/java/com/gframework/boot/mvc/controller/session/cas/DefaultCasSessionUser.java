package com.gframework.boot.mvc.controller.session.cas;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;

import com.gframework.boot.mvc.controller.session.ConversationIdentity;
import com.gframework.boot.mvc.controller.session.user.AbstractBasicUserInfo;

/**
 * 默认的CAS会话用户信息存储对象.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 * @see DefaultConversationCreater
 */
class DefaultCasSessionUser extends AbstractBasicUserInfo implements Assertion, ConversationIdentity {

	private static final long serialVersionUID = 1L;

	/**
	 * 代理cas的Assertion接口对象
	 */
	private final Assertion target;

	/**
	 * 用户身份标识
	 */
	private final String identity ;
	

	/**
	 * 实例化本类对象需要传递cas的会话信息接口对象
	 * 
	 * @param target Assertion接口对象
	 * @throws NullPointerException 参数为null
	 */
	public DefaultCasSessionUser(Assertion target, HttpServletRequest request) {
		if (target == null) {
			throw new NullPointerException();
		}
		this.identity = request.getSession().getId();
		this.target = target;
		AttributePrincipal prin = target.getPrincipal();
		this.setUserId(prin.getName());
	}


	@Override
	public Date getValidFromDate() {
		return this.target.getValidFromDate();
	}

	@Override
	public Date getValidUntilDate() {
		return this.target.getValidUntilDate();
	}

	@Override
	public Date getAuthenticationDate() {
		return this.target.getAuthenticationDate();
	}

	@Override
	public Map<String, Object> getAttributes() {
		return this.target.getAttributes();
	}

	@Override
	public AttributePrincipal getPrincipal() {
		return this.target.getPrincipal();
	}

	@Override
	public boolean isValid() {
		return this.target.isValid();
	}

	@Override
	public Serializable getIdentity() {
		return this.identity;
	}


}
