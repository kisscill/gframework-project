package com.gframework.boot.mvc.entity.dto;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 分页查询结果封装对象.
 * <p>本类主要是为了弥补 {@link PageHelper} 操作处理结果对象 {@link Page} 被jackson解析
 * 数据不完整的问题。本类构造参数必须要传入一个 {@link Page} 类对象，而后本类会提取此对象中的
 * 分页相关参数以及数据信息，从而将本类对象自己变成一个可以合理被jackson解析，并符合rest接口风格的数据格式。
 * <pre>
 * {
 * 	result:[],
 *	pageNum:当前页码,
 *	pageSize:每页显示行数,
 *	startRow:起始行,
 *	endRow:截止行,
 *	total:总数,
 *	pages:总页数,
 *	orderBy:"排序语句"
 * }
 * </pre>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class PageDataDTO<T> {

	/**
	 * 存储结果对象，Page对象本质上就是一个List对象
	 */
	private Page<T> result;

	/**
	 * @param page 分页参数对象
	 * @throws NullPointerException 参数为null
	 */
	public PageDataDTO(Page<T> page) {
		this.result = page;
	}

	public Page<T> getResult() {
		return this.result;
	}

	public void setResult(Page<T> result) {
		this.result = result;
	}

	/**
	 * 页码，从1开始
	 */
	public int getPageNum() {
		return this.result.getPageNum();
	}

	/**
	 * 每页显示行数
	 */
	public int getPageSize() {
		return this.result.getPageSize();
	}

	/**
	 * 起始行
	 */
	public int getStartRow() {
		return this.result.getStartRow();
	}

	/**
	 * 末行
	 */
	public int getEndRow() {
		return this.result.getEndRow();
	}

	/**
	 * 总数
	 */
	public long getTotal() {
		return this.result.getTotal();
	}

	/**
	 * 总页数
	 */
	public int getPages() {
		return this.result.getPages();
	}

	/**
	 * 排序
	 */
	public String getOrderBy() {
		return this.result.getOrderBy();
	}

}
