package com.gframework.boot.mvc.controller.session;

import org.springframework.lang.Nullable;

/**
 * 用户登录会话信息注册/获取类.
 * <p>在请求开始时应当由相应的用户认证框架主动注册，并在结束后销毁，这一步操作可以再任何filter中完成。但必须要在进入servlet前完成。
 * <p>实现本类的目的是因为各大验证框架都存在各自的全局存储逻辑，为了能够兼容各种情况，因此实现此类。
 * <p>本类 只会存储Conversation接口对象，扩展验证类可以根据当前环境使用的实际类型，在提供一个方法，
 * 通过此类获取到后向下转型返回。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see ConversationIdentity
 */
public class ConversationHolder {
	private ConversationHolder(){}
	
	private static final ThreadLocal<ConversationIdentity> threadLocal = new ThreadLocal<>();
	/**
	 * 获取当前用户身份对象.
	 * <p>如果当前不存在请求，或者没有登录，或者相关认证框架没有向此类进行注册，那么将无法获取到。
	 * @return 如果存在则返回ConversationIdentity接口对象，否则返回null
	 */
	@Nullable
    public static ConversationIdentity getConversation() {
        return threadLocal.get();
    }

	/**
	 * 设置当前用户会话对象.
	 * <p>这通常在请求开始的时候，由相关认证框架进行注册，并在进入servlet前完成设置。
	 * @param conversation 用户会话信息对象
	 */
    public static void setConversation(final ConversationIdentity conversation) {
        threadLocal.set(conversation);
    }

    /**
     * 删除当前会话信息，这通常在请求结束后，在finally中调用此方法
     */
    public static void clear() {
        threadLocal.remove();
    }
    
}
