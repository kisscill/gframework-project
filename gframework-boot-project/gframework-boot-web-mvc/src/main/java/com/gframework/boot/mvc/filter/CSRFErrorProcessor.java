package com.gframework.boot.mvc.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * csrf 验证异常的错误处理器，用于{@link CSRFSafeFilter} 验证失败后的处理操作.
 * <p>如果不使用配置此处理器，则会采用默认策略。
 * <p>需要注意的是，此处理器并不能够挽救失败的结果，而是可以让你返回一个正确的信息给客户端，在执行完此类方法后，一定会返回不继续往下执行。
 * 
 * @since 2.0.0
 * @author Ghwolf
 * @see CSRFSafeFilter
 */
@FunctionalInterface
public interface CSRFErrorProcessor {
	/**
	 * csrf验证异常处理操作
	 * @param request http请求对象
	 * @param response http响应对象
	 * @return 如果返回false，则会继续采用默认策略
	 */
	public boolean handle(HttpServletRequest request,HttpServletResponse response);
}
