package com.gframework.boot.mvc.controller.session;

import java.io.Serializable;

import org.springframework.lang.Nullable;

/**
 * 会话身份信息类标识接口.
 * <p>在 服务器/客户端 架构中，客户端的每一个请求都是一个会话，会话可能会带有身份信息，这通常是一个id或cookie，
 * 而后又后台进行用户信息查询并存储到session中，而用户信息对象接口就是本接口。
 * <p>本接口只负责标识用户信息，不负责解析和生成用户信息。
 * <p>同时为了考虑到分布式开发的问题，所有存储在会话中的内容都必须是能够被序列化的，
 * 因此，所有的对象都是 {@link Serializable} 接口的子类对象
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see NoConversationIdentity
 * @see ConversationHolder
 *
 */
public interface ConversationIdentity extends Serializable{
	/**
	 * 取得用户业务id.
	 * <p>此方法是专门用于获取与用户本身关联的id值，例如数据库中的主键，username等。
	 * @return 返回用户真实id
	 */
	public Serializable getUserId();
	/**
	 * 获取用户身份标识符.
	 * <p>身份标识符是类似于门票一样，在用户请求时锁携带的一个标识串，后台可以通过此标识串来获取用户真实信息。
	 * @return 返回请求身份标识，如果没有则为null
	 */
	@Nullable
	public Serializable getIdentity();
	
}
