package com.gframework.boot.mvc.controller.session.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gframework.boot.mvc.controller.session.ConversationIdentity;

/**
 * 基本用户信息抽象基类.
 * <p>本类不强制去使用，仅作为一个用户类信息简单抽象封装的实现。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public abstract class AbstractBasicUserInfo implements Serializable, ConversationIdentity {

	private static final long serialVersionUID = 6358090833420568816L;

	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 用户名，通常是登录账户
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 拥有权限.<br>
	 * 本类对象对权限并没有做一个明确的定义，因此你可以将字符串当做权限，也可以是一个对象，但必须是可被序列化的
	 */
	protected List<Serializable> perms = new ArrayList<>();
	/**
	 * 拥有角色.<br>
	 * 本类对象对角色并没有做一个明确的定义，因此你可以将字符串当做角色，也可以是一个对象，但必须是可被序列化的
	 */
	protected List<Serializable> roles = new ArrayList<>();
	
	/**
	 * 清空所有权限
	 */
	protected void clearPerms() {
		this.perms.clear();
	}
	/**
	 * 清空所有角色
	 */
	protected void clearRoles() {
		this.roles.clear();
	}
	/**
	 * 重置权限
	 * @param perms 重置权限集合
	 */
	protected void resetPerms(List<Serializable> perms) {
		this.perms = perms == null ? Collections.emptyList() : perms;
	}
	/**
	 * 重置角色
	 * @param roles 重置角色集合
	 */
	protected void resetRoles(List<Serializable> roles) {
		this.roles = roles == null ? Collections.emptyList() : roles;
	}
	
	/**
	 * 添加权限
	 * 
	 * @param perm 权限信息
	 */
	public void addPerm(Serializable perm) {
		this.perms.add(perm);
	}
	
	/**
	 * 添加角色
	 * 
	 * @param role 角色信息
	 */
	public void addRole(Serializable role) {
		this.roles.add(role);
	}
	
	/**
	 * 取得权限集合（不可改）
	 * @return 取得当前用户权限集合，
	 */
	public List<Serializable> getPerms() {
		return Collections.unmodifiableList(this.perms);
	}

	/**
	 * 取得角色集合（不可改）
	 * @return 取得当前用户角色集合，
	 */
	public List<Serializable> getRoles() {
		return Collections.unmodifiableList(this.roles);
	}
	
	/**
	 * 是否没有任何权限
	 * @return 如果没有任何权限返回true，否则返回false
	 */
	public boolean isNotHavePerms() {
		return this.perms.isEmpty();
	}
	/**
	 * 是否没有任何角色
	 * @return 如果没有任何角色返回true，否则返回false
	 */
	public boolean isNotHaveRole() {
		return this.roles.isEmpty();
	}
	/**
	 * 判断当前用户是否存在指定的一个或多个权限
	 * @param perms 要进行判断的参数
	 * @return true表示全部都有，false表示存在未包含的，如果没有指定任何权限，返回true
	 */
	public boolean hasPerms(Serializable ... perms) {
		if (perms == null || perms.length == 0) return true ;
		for (Serializable perm : perms) {
			if (!this.perms.contains(perm)) {
				return false ;
			}
		}
		return true ;
	}
	
	/**
	 * 判断当前用户是否存在指定的一个或多个角色
	 * @param roles 要进行判断的参数
	 * @return true表示全部都有，false表示存在未包含的，如果没有指定任何权限，返回true
	 */
	public boolean hasRoles(Serializable ... roles) {
		if (roles == null || roles.length == 0) return true ;
		for (Serializable role : roles) {
			if (!this.roles.contains(role)) {
				return false ;
			}
		}
		return true ;
	}



	/**
	 * 取得用户id
	 * 
	 * @return 用户id
	 */
	public String getUserId() {
		return this.userId;
	}

	/**
	 * 设置用户id
	 * 
	 * @param userId 用户id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 取得用户名，通常是登录账户
	 * 
	 * @return 用户名，通常是登录账户
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * 设置用户名，通常是登录账户
	 * 
	 * @param username 用户名，通常是登录账户
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 取得密码，不保证一定能获取到，通常为了安全起见都会删除此字段
	 * 
	 * @return 如果有，则返回密码，否则返回null
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * 设置密码
	 * 
	 * @param password 密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
