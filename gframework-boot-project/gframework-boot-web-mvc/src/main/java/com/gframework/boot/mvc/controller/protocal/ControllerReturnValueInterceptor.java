package com.gframework.boot.mvc.controller.protocal;

import org.springframework.core.Ordered;
import org.springframework.lang.Nullable;

/**
 * restAPI controller返回结果拦截处理接口.
 * <p>如果要创建一个拦截器，只需要实现此接口子类并被spring管理即可。
 * 但是要防止多个拦截器冲突问题。
 * <p>需要注意的是，这个类型是由返回结果的真实类型决定的，而非方法返回值类型。所以如果返回结果位null，则不会做拦截处理。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @implSpec
 * 此接口子类不允许抛出任何异常。
 */
public interface ControllerReturnValueInterceptor extends Ordered {
	/**
	 * 是否进行拦截.
	 * <p>返回值类型不一定是方法返回的最基础的类型，因为可能结果已经被多个拦截器处理了，
	 * 返回值类型发生了很大的改变。因此参数类型是当前对象的类型。
	 * 
	 * @param returnValueType 返回值类型（不是null）
	 * @return 如果要进行拦截，返回true，否则返回false
	 */
	public boolean supportInterceptor(Class<?> returnValueType) ;
	/**
	 * 进行拦截处理，并将处理后的结果返回
	 * @param obj 原始返回结果
	 * @return 处理后的结果，如果返回null，则终止处理并返回null。
	 */
	@Nullable
	public Object doInterceptor(Object obj) ;
	
}
