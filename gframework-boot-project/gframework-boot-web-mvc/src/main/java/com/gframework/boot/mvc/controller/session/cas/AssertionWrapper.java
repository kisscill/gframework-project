package com.gframework.boot.mvc.controller.session.cas;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;

import com.gframework.boot.mvc.controller.session.ConversationIdentity;

/**
 * CAS 的Assertion接口包装类.
 * <p>主要是为了处理自定义的用户会话类不属于此接口子类的时候，进行的一个包装
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@SuppressWarnings("serial")
public class AssertionWrapper implements Assertion,ConversationIdentity {
	/**
	 * 自定义的用户会话封装对象
	 */
	private final ConversationIdentity conversation ;
	/**
	 * 原始Assertion接口对象
	 */
	private final Assertion target ;
	
	public AssertionWrapper(ConversationIdentity conversation,Assertion target) {
		this.conversation = conversation;
		this.target = target;
	}
	
	/**
	 * 获取 用户自定义的 用户会话信息封装对象
	 * @return 返回{@link ConversationIdentity}接口对象
	 */
	public ConversationIdentity getConversation() {
		return this.conversation;
	}
	
	@Override
	public Date getValidFromDate() {
		return this.target.getValidFromDate();
	}

	@Override
	public Date getValidUntilDate() {
		return this.target.getValidUntilDate();
	}

	@Override
	public Date getAuthenticationDate() {
		return this.target.getAuthenticationDate();
	}

	@Override
	public Map<String, Object> getAttributes() {
		return this.target.getAttributes();
	}

	@Override
	public AttributePrincipal getPrincipal() {
		return this.target.getPrincipal();
	}

	@Override
	public boolean isValid() {
		return this.target.isValid();
	}

	@Override
	public Serializable getUserId() {
		return this.conversation.getUserId();
	}

	@Override
	public Serializable getIdentity() {
		return this.conversation.getIdentity();
	}

}
