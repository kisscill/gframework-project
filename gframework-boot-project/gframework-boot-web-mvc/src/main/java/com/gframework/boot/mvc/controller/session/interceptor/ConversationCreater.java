package com.gframework.boot.mvc.controller.session.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.gframework.boot.mvc.controller.session.ConversationHolder;
import com.gframework.boot.mvc.controller.session.ConversationIdentity;

/**
 * 这个接口是用于初始化.
 * <p>这个接口是用于在用户验证成功后，进行的会话对象创建操作，他可以决定你当前项目环境使用哪个类作用用户信息封装类。
 * <p>实现一个此接口子类并加入spring管理即可进行拦截。
 * <p><strong>但是需要注意的是，此接口只能有唯一的一个spring子类bean</strong>
 * 
 * <p>用户会话三大拦截器：
 * <li>待实现：用户验证拦截器（仅一个，优先执行，用于决定用户信息是否正确）</li>
 * <li>ConversationCreater：用户对象创建拦截器（仅一个，第二个执行，用户决定使用哪个类去封装用户信息+）</li>
 * <li>ConversationInitInterceptor：用户其他信息初始化（可多个，最后执行）</li>
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see ConversationInitInterceptor
 * @see ConversationHolder
 */
public interface ConversationCreater {
	/**
	 * 根据用户凭证创建一个用户信息对象.
	 * 
	 * @param principal 用户凭证，可能是个类对象，或者是个字符串，或者是个json，具体与使用的验证框架有关
	 * @param request http请求对象
	 * @return 返回Conversation接口对象
	 */
	public ConversationIdentity createConversation(Object principal, HttpServletRequest request);
}
