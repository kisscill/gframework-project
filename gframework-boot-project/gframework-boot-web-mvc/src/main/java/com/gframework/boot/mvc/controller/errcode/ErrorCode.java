package com.gframework.boot.mvc.controller.errcode;
/**
 * 基础异常状态接口.
 * <p>如果要扩展更多的异常，可以编写此接口的子接口并添加全局异常常量
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see ErrorCodeException
 */
public interface ErrorCode {
	/** 
	 * 服务端已知的异常类型对应的http状态号，例如服务端预设了大量的自定义错误状态号.
	 * <p>这些均为已知异常，那么推荐再返回这些异常的时候，如果没有明确http状态号对于关系，那么就采用此状态号.
	 * <p>422和500的区别在于，422是服务端已知的错误类型，而500是未知的，通常是因为服务端代码有问题导致。 
	 */
	public static final int DEFAULT_EXCEPTION_TYPE_HTTP_STATUS = 422;
	/** 参数错误 */
	public static final ErrorCodeException PARAM_INCORRECT = new ErrorCodeException(400,400,"参数错误！");
	/** 没有权限 */
	public static final ErrorCodeException NO_AUTHORITY = new ErrorCodeException(403,403,"没有权限！");
	
}