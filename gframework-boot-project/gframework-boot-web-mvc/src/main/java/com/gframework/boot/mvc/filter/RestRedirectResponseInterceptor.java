package com.gframework.boot.mvc.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * rest方式请求，进行重定向的时候，对返回结果的拦截处理器.
 * <p>定义方法：框架已经包含了一个默认的处理操作，你可以自定义一个此接口子类，并被spring管理，即可覆盖默认拦截器
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface RestRedirectResponseInterceptor {
	/**
	 * 重定向拦截操作
	 * @param location 重定向地址
	 * @param response HttpServletResponse对象
	 * @param request HttpServletRequest对象
	 */
	public void response(String location,HttpServletResponse response,HttpServletRequest request) ;
	
}
