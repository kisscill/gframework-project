package com.gframework.boot.mvc.controller.session.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.gframework.boot.mvc.controller.session.ConversationHolder;
import com.gframework.boot.mvc.controller.session.ConversationIdentity;

/**
 * 用户会话信息初始化拦截器.
 * <p>此拦截器是在用户登录完成后，且已经认证完毕后，对用户信息进行一些初始化操作。
 * <p>实现一个此接口子类并加入spring管理即可进行拦截。可多个
 * 
 * <p>用户会话三大拦截器：
 * <li>待实现：用户验证拦截器（仅一个，优先执行，用于决定用户信息是否正确）</li>
 * <li>ConversationCreater：用户对象创建拦截器（仅一个，第二个执行，用户决定使用哪个类去封装用户信息+）</li>
 * <li>ConversationInitInterceptor：用户其他信息初始化（可多个，最后执行）</li>
 * 
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 * @see ConversationCreater
 * @see ConversationHolder
 */
public interface ConversationInitInterceptor {

	/**
	 * 对对象进行初始化操作
	 * @param user {@link ConversationIdentity} 类实例化对象
	 * @param request 当前登录请求request对象
	 * @return 提供返回结果操作可以让其有更高的灵活性，既你可以直接返回参数对象，也可以重新实例化拦截旧的操作。
	 */
	public ConversationIdentity init(ConversationIdentity user,HttpServletRequest request);
	
	/**
	 * 此方法会优先于init执行，此方法会告知当前session对象的类型，你需要决定你是否能够处理该类型的对象，
	 * 当返回true时，才会调用init方法进行初始化。
	 * 
	 * @param converType 当前用户会话封装类的类型
	 * @return 返回true表示可以初始化，否则不进行拦截
	 */
	public boolean canInterceptor(Class<? extends ConversationIdentity> converType) ;
	
}
