/**
 * 用户信息初始化拦截器相关操作类所在包.
 * <p>拦截器需要对相关验证框架做手动扩展才可以实现。例如目前已经对cas进行了扩展，有一个cas的filter会回调这些拦截器
 * 
 * @author Ghwolf
 */
package com.gframework.boot.mvc.controller.session.interceptor;
