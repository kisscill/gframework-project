package com.gframework.boot.mvc.controller.session.user;

/**
 * 人员用户会话信息封装类.
 * <p>本类是将一个人的基本信息做了一个简单封装，不作为强制使用，如果你的用户会话信息封装类需要用到这些信息，可以继承此类简化代码
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public abstract class AbstractMemberUserInfo extends AbstractBasicUserInfo {

	private static final long serialVersionUID = -3341285063105689794L;

	/**
	 * 真实姓名
	 */
	private String name;
	/**
	 * 身份证号
	 */
	private String idCard;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 电话号码
	 */
	private String telephone;
	/**
	 * qq号码
	 */
	private String qq;
	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 取得 真实姓名
	 * @return 真实姓名
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * 设置 真实姓名
	 * @param name 真实姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 取得 身份证号
	 * @return 身份证号
	 */
	public String getIdCard() {
		return this.idCard;
	}
	/**
	 * 设置 身份证号
	 * @param idCard 身份证号
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	/**
	 * 设置 性别
	 * @return 性别
	 */
	public String getSex() {
		return this.sex;
	}
	/**
	 * 取得 性别
	 * @param sex 性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * 取得 电话号码
	 * @return 电话号码
	 */
	public String getTelephone() {
		return this.telephone;
	}
	/**
	 * 设置 电话号码
	 * @param telephone 电话号码
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	/**
	 * 取得 qq号码
	 * @return qq号码
	 */
	public String getQq() {
		return this.qq;
	}
	/**
	 * 设置 qq号码
	 * @param qq qq号码
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	/**
	 * 取得 邮箱
	 * @return 邮箱
	 */
	public String getEmail() {
		return this.email;
	}
	/**
	 * 设置 邮箱
	 * @param email 邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
