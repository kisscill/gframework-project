package com.gframework.boot.mvc.controller.convert;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.lang.Nullable;

/**
 * 前后端统一日期格式转换器.
 * <p>springMVC原始的转换操作存在一些弊端，只能对接受数据进行处理而不能够对返回的日期格式进行处理，
 * 因此本组件进行了一系列扩展，通过复写ObjectMapper bean来实现统一封装。你只需要实现此接口的子类bean，即可做到统一前后端通讯日期格式。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see CompatibleObjectMapper
 */
public interface UnifiedDateConverter extends Serializable{
	/**
	 * 将一个字符串转换为日期对象
	 * @param source 日期字符串(不为null)
	 * @return 返回日期对象，可以为null
	 * @throws UnifiedDateConvertException 转换异常 
	 */
	@Nullable
	public Date stringToDate(String source) throws UnifiedDateConvertException;
	/**
	 * 将一个日期对象转换为字符串
	 * @param date 日期对象，不为null
	 * @return 返回转换后的字符串或数字型对象，可以为null
	 */
	@Nullable
	public Object dateToStringOrNumber(Date date) ;
	/**
	 * 将一个字符串转换为日期对象
	 * @param source 日期字符串(不为null)
	 * @return 返回日期对象，可以为null
	 * @throws UnifiedDateConvertException 转换异常 
	 */
	@Nullable
	public LocalDate stringToLocalDate(String source) throws UnifiedDateConvertException;
	/**
	 * 将一个日期对象转换为字符串
	 * @param date 日期对象，不为null
	 * @return 返回转换后的字符串或数字型对象，可以为null
	 */
	@Nullable
	public Object localDateToStringOrNumber(LocalDate date) ;
	/**
	 * 将一个字符串转换为日期对象
	 * @param source 日期字符串(不为null)
	 * @return 返回日期对象，可以为null
	 * @throws UnifiedDateConvertException 转换异常 
	 */
	@Nullable
	public LocalDateTime stringToLocalDateTime(String source) throws UnifiedDateConvertException;
	/**
	 * 将一个日期对象转换为字符串
	 * @param date 日期对象，不为null
	 * @return 返回转换后的字符串或数字型对象，可以为null
	 */
	@Nullable
	public Object localDateTimeToStringOrNumber(LocalDateTime date) ;

}
