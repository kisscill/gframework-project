package com.gframework.boot.mvc.controller.session;

import java.io.Serializable;

/**
 * 无会话状态的会话类.
 * <p>通过{@link NoConversationIdentity#INSTANCE}来获取本类对象
 * <p>在某些不需要session接口，且存在高并发调用的情况下，在进行隐士的session创建无疑会浪费大量的性能。
 * 你可以选择在web容器上关闭session，或者如果必须要有session，可以使用此类。
 * <p>此类会维护一个全局唯一的会话对象，任何人请求都是这个对象，而且这个对象的Principal是字符串0，永不失效。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@SuppressWarnings("serial")
public class NoConversationIdentity implements ConversationIdentity {

	public static final NoConversationIdentity INSTANCE = new NoConversationIdentity();
	
	private NoConversationIdentity(){}

	@Override
	public Serializable getUserId() {
		return "0" ;
	}

	@Override
	public Serializable getIdentity() {
		return null;
	}

}
