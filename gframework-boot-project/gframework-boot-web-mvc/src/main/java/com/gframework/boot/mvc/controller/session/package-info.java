/**
 * 登录用户会话信息存储/逻辑处理相关类所在包.
 * <p>首先需要说明一点的是，你可以完全自己去定义用户验证逻辑，但必须要符合某些要求：
 * <ul>
 * <li>1. 要能够支持拦截器的处理操作：相关接口在{@link com.gframework.boot.mvc.controller.session.interceptor}包下</li>
 * <li>2. 最终生成的用户信息封装对象必须实现{@link com.gframework.boot.mvc.controller.session.ConversationIdentity}接口</li>
 * <li>3. 最终生成的用户信息封装对象要在每次请求时注册到{@link com.gframework.boot.mvc.controller.session.ConversationHolder}
 * 同时要在请求结束后销毁（try...finally）</li>
 * </ul>
 * <p>这样一来，拦截器的核心逻辑几乎可以不用变化太多或者不用改变。
 * 
 * <p>目前已经支持有CAS单点登录认证逻辑，但是并未提供直接可以用的普通认证逻辑。
 * <p>你通过{@link com.gframework.annotation.EnableCasClientSecurity}
 * 启动注解即可启动{@link com.gframework.boot.mvc.config.CasConfig}配置，既开启Cas相关过滤器。
 * 同时以及提供好了一套拦截器处理操作方法。{@link com.gframework.boot.mvc.controller.session.cas.CasSessionUserCreateFilter}
 * 
 * @author Ghwolf
 */
package com.gframework.boot.mvc.controller.session;