package com.gframework.boot.mvc.controller.convert;

import java.time.LocalDateTime;

import org.springframework.core.convert.converter.Converter;

/**
 * 字符串转日期转换器.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class StringToLocalDateTimeConvert implements Converter<String, LocalDateTime> {

	private final UnifiedDateConverter dateConvert;

	public StringToLocalDateTimeConvert(UnifiedDateConverter dateConvert) {
		this.dateConvert = dateConvert;
	}

	@Override
	public LocalDateTime convert(String source) {
		return this.dateConvert.stringToLocalDateTime(source);
	}

}
