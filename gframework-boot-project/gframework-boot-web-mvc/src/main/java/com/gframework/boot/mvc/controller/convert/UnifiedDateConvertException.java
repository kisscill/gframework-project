package com.gframework.boot.mvc.controller.convert;

/**
 * 在统一前后端通讯协议时，发生日期转换异常，则抛出此异常
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class UnifiedDateConvertException extends RuntimeException {

	public UnifiedDateConvertException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnifiedDateConvertException(String message) {
		super(message);
	}

}
