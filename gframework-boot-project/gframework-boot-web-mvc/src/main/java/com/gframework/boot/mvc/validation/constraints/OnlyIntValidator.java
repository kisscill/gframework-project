package com.gframework.boot.mvc.validation.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * OnlyInt 验证注解验证器.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see OnlyInt
 */
public class OnlyIntValidator implements ConstraintValidator<OnlyInt, Number> {
	/**
	 * 是否允许为null
	 */
	private boolean notNull ;
	/**
	 * 匹配的值
	 */
	private int[] value ;
	
	@Override
	public void initialize(OnlyInt constraintAnnotation) {
		this.notNull = constraintAnnotation.notNull();
		this.value = constraintAnnotation.value();
	}

	@Override
	public boolean isValid(Number value, ConstraintValidatorContext context) {
		if (value == null) {
			return !this.notNull;
		}
		int va = value.intValue();
		
		for (int v : this.value) {
			if (v == va) return true;
		}
		return false ;
	}

}
