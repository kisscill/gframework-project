package com.gframework.autoconfigure.mvc;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * mvc统一转换操作配置信息
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@ConfigurationProperties("gframework.mvc")
public class MvcProperties {

	/**
	 * 日期转换时区偏移，默认：+8.
	 * <p>
	 * 仅在使用统一日期转换器的时候才有效！
	 */
	private String convertDateZoneOffset = "+8";

	/**
	 * 如果controller正常返回结果，是否不进行统一前后端通讯格式包装。默认：false
	 */
	private boolean noPackagingWhenSuccess = false;

	/**
	 * 是否在（提交动作的）post请求成功后http状态码返回201，默认true
	 */
	private boolean return201WhenPost = true;

	/**
	 * 当使用post请求进行查询操作时，约定的uri后缀.
	 * <p>
	 * 由于rest风格要求查询使用get，但是get并不能提交大参数，那么第二推荐的请求方法为POST，
	 * 为了能够在POST上区分提交和查询，通常会约定一个uri规范。此规范则是通过uri后缀进行定义。
	 * <p>
	 * 此配置默认不会产生过大影响，目前仅仅和{@link #return201WhenPost}有交互
	 */
	private String postSearchUriSuffix = "_search";

	public String getPostSearchUriSuffix() {
		return this.postSearchUriSuffix;
	}

	public void setPostSearchUriSuffix(String postSearchUriSuffix) {
		this.postSearchUriSuffix = postSearchUriSuffix;
	}

	public boolean isReturn201WhenPost() {
		return this.return201WhenPost;
	}

	public void setReturn201WhenPost(boolean return201WhenPost) {
		this.return201WhenPost = return201WhenPost;
	}

	public boolean isNoPackagingWhenSuccess() {
		return this.noPackagingWhenSuccess;
	}

	public void setNoPackagingWhenSuccess(boolean noPackagingWhenSuccess) {
		this.noPackagingWhenSuccess = noPackagingWhenSuccess;
	}

	public String getConvertDateZoneOffset() {
		return this.convertDateZoneOffset;
	}

	public void setConvertDateZoneOffset(String convertDateZoneOffset) {
		this.convertDateZoneOffset = convertDateZoneOffset;
	}

}
