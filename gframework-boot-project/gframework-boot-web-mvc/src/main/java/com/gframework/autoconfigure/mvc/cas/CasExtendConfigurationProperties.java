package com.gframework.autoconfigure.mvc.cas;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * cas 非标准扩展配置类.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@ConfigurationProperties(prefix = "gframework.mvc.casconfig")
public class CasExtendConfigurationProperties {

	/**
	 * 不进行过滤的url正则表达式.
	 * 注意：用于匹配的url是完整的url，包括协议，参数等，而非只是从根路径开始。
	 */
	private String ignorePattern;
	/**
	 * 是否允许post方式提交ticket
	 */
	private boolean allowPostTicket;

	public boolean isAllowPostTicket() {
		return this.allowPostTicket;
	}

	public void setAllowPostTicket(boolean allowPostTicket) {
		this.allowPostTicket = allowPostTicket;
	}

	public void setIgnorePattern(String ignorePattern) {
		this.ignorePattern = ignorePattern;
	}

	public String getIgnorePattern() {
		return this.ignorePattern;
	}

}
