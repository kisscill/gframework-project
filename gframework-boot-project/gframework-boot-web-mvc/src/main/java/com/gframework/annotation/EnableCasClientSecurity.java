package com.gframework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.gframework.boot.mvc.config.CasConfig;
import com.gframework.boot.mvc.controller.session.cas.CasSessionUserCreateFilter;

import net.unicon.cas.client.configuration.EnableCasClient;

/**
 * 开启给予cas用户验证的相关操作.
 * <p>此注解基于 {@link EnableCasClient} 进行扩展
 * <p>如果要使用此注解开启CAS，同时要依赖此组件到pom中
 * <pre>
 *  {@code <dependency>}
 *    {@code <groupId>net.unicon.cas</groupId>}
 *    {@code <artifactId>cas-client-autoconfig-support</artifactId>}
 *  {@code </dependency>}
 * </pre>
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

@EnableCasClient
@Import({CasConfig.class,CasSessionUserCreateFilter.class})
public @interface EnableCasClientSecurity {

}
