package com.gframework.datastructure.trie;

/**
 * 单词查找树(Trie)操作接口.
 * <p>
 * 单词查找树针对单词具有公共前缀的特定进行搜索，以拥有高性能的查询效率.<br>
 * 使用此操作，可以获取字符串中每一串特定的单词索引并进行处理操作。这比直接使用replaceAll进行整体全局替换要快的多。
 * </p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface TrieTree {

	/**
	 * 遍历指定字符串中的每一个特定单词(最长匹配原则)，并进行指定的处理操作.
	 * <p>
	 * 例如，你需要将一个字符串的特定词汇屏蔽时，可以使用此方法。这会比直接使用replace要快的多。
	 * </p>
	 * 
	 * @param str 要处理的字符串，如果为null则不做任何事情。
	 * @param handler 处理特殊词汇的操作方法，不能为null
	 * @return 返回处理完毕的结果
	 */
	public String each(String str, StringHandleFunction handler);

	/**
	 * 判断一个字符串是否存在任意已经指定的特殊词汇.
	 * 
	 * @param str 要判断的字符串，如果为null，则返回false
	 * @return 如果存在则返回true，无特殊词汇返回false
	 */
	public boolean match(String str);

	/**
	 * 查询一个字符串中第一个任意特殊词汇的开始索引.
	 * 
	 * @param str 要判断的字符串，如果为null，则返回-1
	 * @return 返回第一个特定特殊词汇的开始索引，没有则返回-1
	 */
	public int indexOf(String str);

	/**
	 * 从指定索引开始查询一个字符串中第一个任意特殊词汇的开始索引.
	 * 
	 * @param str 要判断的字符串，如果为null，则返回-1
	 * @param fromIndex 从哪个索引开始
	 * @return 返回从指定索引开始后的第一个特定特殊词汇的开始索引，没有则返回-1
	 */
	public int indexOf(String str, int fromIndex);
	
	/**
	 * 取得易于查看得格式化显示得树结构
	 * 
	 * @param title 图的首部标题，仅做展示用，可以为null
	 * @return 返回格式化树结构字符串
	 */
	public String getFormatString(String title);

}
