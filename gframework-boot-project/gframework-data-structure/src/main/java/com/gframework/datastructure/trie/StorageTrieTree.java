package com.gframework.datastructure.trie;

/**
 * 支持存储的单词查找树(Trie)操作接口.
 * <p>
 * 需要注意的是，本类的操作效率并没有直接使用Map高，如果你只想进行简单的{@code key-value}查询，那么不应该使用此方法。
 * 除非以下几种或其他你认为需要的情况：
 * <ul>
 * <li>如果你需要进行特殊字符串规则的查询，例如{@link WildcardTrieTree}支持通配符查询和优先级匹配的查询操作，
 * 这种key与value并非一一对应而是存在其他逻辑关系需要大量查询的情况。</li>
 * <li>map的存储到了2000w左右基本上就很难再存入新数据了，但是本类可以支持存储更多的数据，
 * 因为trie数是根据单词进行分层的，不会全部存在一个map中。但这并不意味着有比map更高的查询效率。</li>
 * <li>你需要基于此接口或{@link SeparatorTrieTree}进行扩展时，可以使用它实现自己更多的逻辑。</li>
 * </ul>
 * </p>
 * <p>
 * 因此trie树并不适合做数据存储，他的作用是单词匹配和特定规则的高速查询。
 * 同时key值只能是字符串。而且trie树得key不能是null或空字符串，因为毫无意义。
 * </p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @param <T> 存储得内容类型
 * @see SeparatorTrieTree
 */
public interface StorageTrieTree<T> extends TrieTree {

	/**
	 * 尝试寻找一个数据，如果存在，则返回，否则返回null.
	 * 
	 * @param key 存储数据的键值
	 * @return 返回匹配得内容，没有返回null。如果key值为null或空字符串，则返回null
	 */
	public T find(String key);

	/**
	 * 判断一个key是否存在于节点中
	 * 
	 * @param key 要判断的key值
	 * @return 如果此key值存在于树中，可以被获取到，则返回true，否则返回false
	 */
	public boolean containsKey(String key);

	/**
	 * 尝试删除一个节点数据，如果时可删除得，则删除该节点并返回其对应得内容，否则返回null.<br>
	 * 
	 * <pre>
	 * 需要注意的是，此方法只会删除数据，不会删除节点，节点本身还是会保留。
	 * 除非被删除的数据所在节点没有任何叶子节点，那么同时也会删除此单一节点。
	 * </pre>
	 * <p>
	 * trie树并不一定时和Map一样key与value呈一一对应关系，可能某些节点可以匹配多种key值类型。
	 * 因此如果不是完全精准匹配，那么就无法删除。
	 * </p>
	 * 
	 * @param key 要删除得节点
	 * @return 如果时可删除得，则删除该节点并返回其对应得内容，否则返回null
	 */
	public T removeValue(String key);

	/**
	 * 此方法将会删除一整个节点.
	 * 
	 * <pre>
	 * 例如，你已经有了
	 * a.b.c.d
	 * a.b
	 * a.b.d
	 * 如果你删除a.b，那么以上三个全部都会被删除
	 * 
	 * @param key 要删除得整个叶子节点key前缀
	 */
	public void remove(String key);

	/**
	 * 添加一个新的参数到节点，key必须是一个有意义的字符串，不能是null或空字符串。
	 * 
	 * @param key 参数key值
	 * @param value 要存储的数据
	 * @return 如果当前得设置覆盖了已有的值，则会将旧的值返回，否则返回null
	 */
	public T add(String key, T value);

	@Override
	default String each(String str, StringHandleFunction handler) {
		throw new UnsupportedOperationException("StorageTrieTree不支持检索型操作：each、indexOf、match");
	}

	@Override
	default int indexOf(String str) {
		throw new UnsupportedOperationException("StorageTrieTree不支持检索型操作：each、indexOf、match");
	}

	@Override
	default int indexOf(String str, int fromIndex) {
		throw new UnsupportedOperationException("StorageTrieTree不支持检索型操作：each、indexOf、match");
	}

	@Override
	default boolean match(String str) {
		throw new UnsupportedOperationException("StorageTrieTree不支持检索型操作：each、indexOf、match");
	}

}
