package com.gframework.datastructure.trie;

/**
 * 处理一个字符串中的子字符串时的操作函数.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@FunctionalInterface
public interface StringHandleFunction {
	
	/**
	 * 处理子字符串操作.
	 * 
	 * @param source 原始字符串
	 * @param str 当前截取出来的子字符串
	 * @param begin 当前字符串所在的开始索引（第一个字符所在索引），begin和end是可能相同的，即匹配的单词只有一个字符
	 * @param end 当前字符串所在的结束索引（最后一位字符所在索引）
	 * @return 返回处理后的字符串
	 */
	public String handle(String source,String str,int begin,int end) ;

}
