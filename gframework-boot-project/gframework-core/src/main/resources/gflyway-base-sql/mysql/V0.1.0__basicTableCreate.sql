/*==============================================================*/
/* Table: T_SYS_DATASOURCE                                      */
/*==============================================================*/
create table T_SYS_DATASOURCE
(
   DS_ID                varchar(50) not null comment '主键',
   REMARK               varchar(255) comment '描述',
   JNDI                 varchar(30) comment 'JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中',
   DRIVER_CLASS_NAME    varchar(60) comment '驱动包，可不填，会自动识别',
   DB_URL               varchar(255) not null comment '连接URL',
   USERNAME             varchar(32) not null comment '连接用户',
   PASSWORD             varchar(64) not null comment '连接密码(建议加密保存)',
   POOL_NAME            varchar(30) comment '连接池名称，会用于线程名称',
   MAX_POOL_SIZE        int comment '最大连接数[可选]',
   MIN_POOL_SIZE        int comment '最小连接数[可选]',
   IDLE_TIMEOUT	        int default 600000 comment '配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在',
   MAX_LIFETIME	        int default 28800000 comment '配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收',
   LOGIN_TIMEOUT        int default 0 comment '获取连接对象超时时间（秒）',
   VALIDATION_TIMEOUT   int default 0 comment '设置池等待连接验证为活动连接的最大毫秒数',
   CONNECTION_TIMEOUT   int default 0 comment '设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取',
   CONNECTION_TEST_QUERY  varchar(255) comment '设置要执行的SQL查询以测试连接的有效性',
   ENABLED              tinyint not null default 1 comment '是否启用,1:是，0:否',
   BUILTIN              tinyint not null default 0 comment '是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改',
   DELETED              tinyint not null default 0 comment '删除标记,1:是，0:否',
   primary key (DS_ID)
)
charset = UTF8
engine = InnoDB;

alter table T_SYS_DATASOURCE comment '数据源表，目前是按照HikariDataSource数据源来配置的';

/*==============================================================*/
/* Table: T_SYS_ICON                                            */
/*==============================================================*/
create table T_SYS_ICON
(
   ICON_ID              bigint not null auto_increment comment '主键',
   ICON_NAME            varchar(50) not null comment '图标名称',
   ICON_TYPE            varchar(20) comment '图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型',
   ICON_VALUE           varchar(128) comment '图标内容，非base64图标内容可以在这里存储',
   ICON_BASE64          text comment '图标BASE64值，当值是base64编码时，在这里存储',
   REMARK               varchar(255) comment '描述',
   primary key (ICON_ID)
)
charset = UTF8
engine = InnoDB;

alter table T_SYS_ICON comment '图标表，此表存储两种类型的图标：
1、base64小图标（最多64kb）
2、class指定的图';

/*==============================================================*/
/* Index: IDX_ICON_TYPE                                         */
/*==============================================================*/
create index IDX_ICON_TYPE on T_SYS_ICON
(
   ICON_TYPE
);

/*==============================================================*/
/* Table: T_SYS_PARAMETER                                       */
/*==============================================================*/
create table T_SYS_PARAMETER
(
   PARAM_ID             bigint not null auto_increment comment '主键',
   PARAM_KEY            varchar(64) not null comment '配置项名称',
   PARAM_VALUE          varchar(255) comment '配置项值',
   DEFAULT_VALUE        varchar(255) comment '默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。',
   REMARK               varchar(255) comment '描述',
   BUILTIN              tinyint not null default 0 comment '是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改',
   DELETED              tinyint not null default 0 comment '删除标记,1:是，0:否',
   primary key (PARAM_ID)
)
charset = UTF8
engine = InnoDB;

alter table T_SYS_PARAMETER comment '系统配置表';

/*==============================================================*/
/* Index: IDX_KEY                                               */
/*==============================================================*/
create index IDX_KEY on T_SYS_PARAMETER
(
   PARAM_KEY
);

/*==============================================================*/
/* Table: T_SYS_SCHEDULED                                       */
/*==============================================================*/
create table T_SYS_SCHEDULED
(
   SCHE_ID              bigint not null auto_increment comment '主键',
   TASK_NAME            varchar(50) not null comment '任务名称',
   REMARK               varchar(255) comment '描述',
   EXECUTE_BEAN_NAME    varchar(50) not null comment '执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制',
   DISPOSABLED          tinyint not null default 0 comment '是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间',
   START_TIME           datetime comment '生效时间，可不填，填写后表示此时间后定时器才有效',
   END_TIME             datetime comment '截止时间，可不填，填写后表示到此时间后将不会在生效',
   CRON                 varchar(30) comment 'CRON表达式',
   FIXED_DELAY          bigint comment '延迟执行间隔(毫秒)，一次执行结束后多久执行下一次',
   FIXED_DELAY_STRING   varchar(50) comment '延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次',
   FIXED_RATE           bigint comment '循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕',
   FIXED_RATE_STRING    varchar(50) comment '循环执行间隔(毫秒)（从配置获取）',
   INITIAL_DELAY        bigint comment '首次执行延迟时间(毫秒)',
   INITIAL_DELAY_STRING varchar(50) comment '首次执行延迟时间(毫秒)（从配置获取）',
   DISTRIBUTED_KEY      varchar(50) comment '分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行',
   LOCK_MODE            tinyint comment '分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间',
   LOCK_LEASE_TIME      int comment '分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒）',
   BUILTIN              tinyint not null default 0 comment '是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改',
   DELETED              tinyint not null default 0 comment '删除标记,1:是，0:否',
   primary key (SCHE_ID),
   unique key AK_KEY_TASK_NAME (TASK_NAME)
)
charset = UTF8
engine = InnoDB;

alter table T_SYS_SCHEDULED comment '定时器任务表，此表的核心思想是，可以执行任意实现了特定标准的bean的任意方法或特定方法';
