package com.gframework;

import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.gframework.annotation.flyway.EnableFlywayExtend;
import com.gframework.annotation.redis.EnableMultipartRedisson;
import com.gframework.autoconfigure.mvc.MvcFileUploadConfig;
import com.gframework.autoconfigure.thread.AsyncConfig;
import com.gframework.context.spring.AppContext;
import com.gframework.core.GhwolfAppConst;

/**
 * Gframework Boot程序启动类，你可以将此类加入到{@link SpringApplication#run(Class[], String[])}方法的class参数中。
 * <ol>
 * <li>默认装配redis操作被关闭，可以通过配置 <code>gframework.redis.enable=true</code> 来开启。</li>
 * <li>aop,scheduling,async已经被默认开启</li>
 * </ol>
 * @since 2.0.0
 * @author Ghwolf
 */
@EnableScheduling
@EnableAsync
// proxyTargetClass=true 强制使用cglib代理，否则基于接口代理
// exposeProxy=true，可以再类中利用AopContext去的当前aop类的代理类对象
@EnableAspectJAutoProxy
// @ServletComponentScan //
// 装配WebListener,WebFilter,WebServlet，所有的都需要走bean配置，不允许单独出现
@EnableMultipartRedisson
@EnableFlywayExtend
@Configuration(proxyBeanMethods = false)
@Import({ AsyncConfig.class, MvcFileUploadConfig.class})
@Order(Integer.MAX_VALUE)
public class GframeworkSpringBootStart implements ApplicationRunner{
	/**
	 * 启动时间
	 */
	public static final long START_TIMESTAMP = System.currentTimeMillis();
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		long end = System.currentTimeMillis();
		
		LoggerFactory.getLogger(GframeworkSpringBootStart.class)
		.info("\n--------------- GFrameWork {} start ---------------"
				+ "\n--------- {} 启动完毕！... 进程号：{}，端口号：{}，上下文：{}，花费时间：{} ---------",
				GhwolfAppConst.VERSION,
				AppContext.getProperty("spring.application.name",""),
				System.getProperty("PID"),
				AppContext.getProperty("server.port"),
				AppContext.getProperty("server.servlet.context-path","/"),
				end - START_TIMESTAMP);
	}

}
