package com.gframework.annotation.redis;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.gframework.core.redis.MultipartRedissonConfig;

/**
 * 启动多redisson配置.
 * <p>使用此注解后会关闭redis和redisson的默认装配类
 * @author Ghwolf
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

@Import({MultipartRedissonConfig.class})
public @interface EnableMultipartRedisson {

}
