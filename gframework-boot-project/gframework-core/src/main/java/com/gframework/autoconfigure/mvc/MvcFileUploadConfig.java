package com.gframework.autoconfigure.mvc;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

/**
 * 文件上传参数配置类.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods=false)
public class MvcFileUploadConfig {
	
	@Bean
	public MultipartConfigElement getMultipartConfigElement(MultipartProperties multipartProperties) {
		if (StringUtils.isEmpty(multipartProperties.getLocation())) {
			multipartProperties.setLocation(System.getProperty("java.io.tmpdir"));
		}
		return multipartProperties.createMultipartConfig();
	}
}
