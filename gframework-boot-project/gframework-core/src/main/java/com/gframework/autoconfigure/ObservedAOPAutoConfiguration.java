package com.gframework.autoconfigure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gframework.core.service.observed.ObservedAOP;

/**
 * 观察者AOP操作组件装配类
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods=false)
public class ObservedAOPAutoConfiguration {
	
	@Bean
	public ObservedAOP getObservedAOP(){
		return new ObservedAOP();
	}
}
