package com.gframework.autoconfigure;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.IntrospectorCleanupListener;

/**
 * 一些起辅助功能的监听器配置.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods=false)
public class AuxiliaryListenerAutoConfiguration {

	/**
	 * Spring 刷新Introspector防止内存泄露监听器.
	 * <p>
	 * jdk本身有一套对类进行结构解析的操作api，但是需要手动清空不需要的垃圾对象，部分第三方框架并没有这么做，所以此监听器就是为了解决这个问题。
	 * </p>
	 * @return {@link IntrospectorCleanupListener}
	 */
	@Bean
	public ServletListenerRegistrationBean<IntrospectorCleanupListener> getIntrospectorCleanupListener() {
		ServletListenerRegistrationBean<IntrospectorCleanupListener> bean = new ServletListenerRegistrationBean<>();
		bean.setListener(new IntrospectorCleanupListener());
		return bean ;
	}
	
}
