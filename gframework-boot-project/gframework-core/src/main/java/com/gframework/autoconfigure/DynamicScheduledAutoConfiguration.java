package com.gframework.autoconfigure;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gframework.core.service.lock.DistributedLock;
import com.gframework.core.service.scheduled.DynamicScheduledFactory;

/**
 * 动态定时任务管理器装配类
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods=false)
@AutoConfigureAfter(DistributedLockAutoConfiguration.class)
public class DynamicScheduledAutoConfiguration {

	@Bean
	public DynamicScheduledFactory getDynamicScheduledFactory(ObjectProvider<DistributedLock> dlock){
		return new DynamicScheduledFactory(dlock);
	}
}
