package com.gframework.autoconfigure;

import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gframework.core.service.lock.DistributedLock;
import com.gframework.core.service.lock.RedisDistributedLock;

/**
 * 分布式锁组件装配类
 * 
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods = false)
public class DistributedLockAutoConfiguration {

	/**
	 * 基于redisson实现的分布式锁
	 */
	@Bean
	@ConditionalOnClass(RedissonClient.class)
	@ConditionalOnSingleCandidate(RedissonClient.class)
	@ConditionalOnMissingBean(DistributedLock.class)
	public RedisDistributedLock getRedisDistributedLock(RedissonClient redisson) {
		return new RedisDistributedLock(redisson);
	}
}
