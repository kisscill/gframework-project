package com.gframework.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * gframework框架 flyway相关配置项
 * 
 * @author Ghwolf
 */
@ConfigurationProperties("gframework.flyway")
public class GFrameworkFlywayProperties {

	/**
	 * 是否初始化 gframework 框架的基础支持表，默认true
	 */
	private boolean initGframeworkBasicTable = true;

	/**
	 * 初始化数据库的T_SYS_PARAMETER表的参数到spring环境变量中
	 */
	private boolean initDbSysParameter = true;
	/**
	 * 初始化T_SYS_DATASOURCE表里的数据源信息
	 */
	private boolean initDbDataSource = true;

	public boolean isInitGframeworkBasicTable() {
		return this.initGframeworkBasicTable;
	}

	public void setInitGframeworkBasicTable(boolean initGframeworkBasicTable) {
		this.initGframeworkBasicTable = initGframeworkBasicTable;
	}

	public boolean isInitDbSysParameter() {
		return this.initDbSysParameter;
	}

	public void setInitDbSysParameter(boolean initDbSysParameter) {
		this.initDbSysParameter = initDbSysParameter;
	}

	public boolean isInitDbDataSource() {
		return this.initDbDataSource;
	}

	public void setInitDbDataSource(boolean initDbDataSource) {
		this.initDbDataSource = initDbDataSource;
	}

}
