package com.gframework.autoconfigure;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import com.gframework.core.middleware.CommonHttpClientProperties;
import com.gframework.core.middleware.HttpClientConfig;

/**
 * 可共用的httpclient bean装配类
 * 
 * @author Ghwolf
 */
@EnableConfigurationProperties(CommonHttpClientProperties.class)
@Import(HttpClientConfig.class)
public class CommonHttpClientAutoConfiguration {

}
