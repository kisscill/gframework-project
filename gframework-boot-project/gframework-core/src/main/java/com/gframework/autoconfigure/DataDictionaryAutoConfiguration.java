package com.gframework.autoconfigure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gframework.core.service.spring.DataDictionary;
import com.gframework.core.service.spring.ExtendDataDictionary;

/**
 * 数据字典自动装配类
 * 
 * @since 2.0.0
 * @author Ghwolf
 *
 */
@Configuration(proxyBeanMethods = false)
public class DataDictionaryAutoConfiguration {
	/**
	 * 数据字典操作对象
	 */
	@Bean
	public DataDictionary getDataDictionary(ExtendDataDictionary[] extendDataDictionary){
		return new DataDictionary(extendDataDictionary);
	}
}
