/**
 * 框架部分内容简介.
 * 
 * <p><strong>公共标识性注解，这些注解目前暂无实际用处，但是都是解释说明用于更好地说明代码用的。</strong>
 * 
 * <ul>
 * <li>{@link org.springframework.lang.Nullable}：spring的可为空注解。
 * 对于可为空的参数或返回值建议加上次注解。</li>
 * <li>{@link com.gframework.paramparse.core.ParamSupport}：支持参数解析的标识性注解。
 * 可用于bean的属性，或者泛型的类型上，标识这个地方的值支持参数解析。参数解析器则是：{@link com.gframework.paramparse.core.ParamParseContext}</li>
 * <li>{@link com.gframework.annotation.ThreadSafe}：线程安全标识注解，用于类，标识此类的操作时线程安全的。</li>
 * <li>{@link com.gframework.annotation.LogicalDelete}：逻辑删除标识注解，用于表示当前方法中的查询或删除操作都走逻辑删除过滤。</li>
 * </ul>
 * 
 * <p><strong>spring钩子接口</strong>
 * <ul>
 * <li>{@link org.springframework.beans.factory.config.BeanPostProcessor} bean创建钩子接口</li>
 * </ul>
 * 
 * <p><strong>spring配置加载部分核心类/接口</strong>
 * <ul>
 * <li>{@link org.springframework.core.env.Environment} 这个是资源环境配置获取接口，注入即可获取到。</li>
 * <li>{@link org.springframework.boot.context.properties.bind.Binder} yaml配置的数组必须通过key[0],key[1]获取，map也很难转换。通过Binder可以实现配置和bean的真正互相转换</li>
 * <li>{@link org.springframework.jdbc.core.JdbcTemplate} 如果不想用mybatis，直接注入jdbc也可以用，并且共享事务</li>
 * </ul>
 * 
 * <p><strong>spring事务工具类</strong>
 * <ul>
 * <li>{@link org.springframework.transaction.support.TransactionSynchronizationManager}可以获取当前是否处于事务中以及其他信息的工具类</li>
 * <li>{@link org.springframework.transaction.interceptor.TransactionAspectSupport}可以获取当前事务状态（新旧），以及强制设置回滚的工具类</li>
 * <li>{@link org.springframework.jdbc.datasource.DataSourceUtils}这事spring事务管理的核心数据源获取工具类。
 * 首先在一个事物中，你可以既使用mybatis，也使用jdbc，但是前提是获取Connection必须通过DataSourceUtils，他会检测事务状态并决定返回一个新的还是旧的连接对象。
 * 如果在非事务中，则每次都会获取一个新的。JdbcTemplate就是用的他。</li>
 * </ul>
 * 
 * <p><strong>spring核心bean</strong>
 * <ul>
 * <li>{@link org.springframework.core.convert.ConversionService} spring mvc的类型转换器用的就是他，我们可以直接注入此bean来使用它的类型转换器</li>
 * <li>{@link javax.validation.Validator} 执行 validate方法即可。如果有返回值，则说明验证失败，返回内容是错误信息。如果注解配置错误，则抛出异常。也可以只验证其中某一个属性</li>
 * </ul>
 * 
 * <p>
 * <strong>java官方提供的很好用的类</strong>
 * <ul>
 * <li>编码设置优先使用java官方提供的{@link java.nio.charset.StandardCharsets}类获取</li>
 * </ul>
 * 
 * <p>
 * <strong>apache common 好用的工具类</strong>
 * <ul>
 * <li>{@link org.apache.commons.io.IOUtils} 非常好用的IO工具类</li>
 * <li>{@link org.apache.commons.codec.digest.DigestUtils} 非常好用的加密工具类（单向）</li>
 * <li>{@link org.apache.commons.lang.StringEscapeUtils} 防止sql注入和xss攻击的，html、java、js、sql等转义处理工具类，性能高于esapi</li>
 * <li>{@link org.apache.commons.lang.time.DateFormatUtils} 日期格式化工具类</li>
 * <li>{@link org.apache.commons.lang.WordUtils} 比较好用的字符串处理工具类，如提取首字母，首字母大写等</li>
 * <li>{@link org.apache.commons.lang.ClassUtils} 反射的辅助工具类</li>
 * <li>{@link org.apache.commons.lang.BooleanUtils} 关于boolean的工具类，而且可以支持boolean，int，string的相互转换和判断</li>
 * <li>{@link org.apache.commons.lang.RandomStringUtils} 生成随机字符串的工具类</li>
 * <li>{@link org.apache.commons.lang.SerializationUtils} 序列化工具类</li>
 * <li>{@link org.apache.commons.lang.StringUtils} 字符串辅助工具类</li>
 * <li>{@link org.apache.commons.lang.ArrayUtils} 非常好用的数组工具类，而且内置很多空数组常量</li>
 * <li>{@link org.apache.commons.lang.SystemUtils} 系统环境工具类，可以获取java版本，javahome目录以及iotmp目录</li>
 * <li>{@link org.apache.commons.lang.CharEncoding} 内涵编码常量，不用再手写UTF-8，但是因为只是字符串，如果需要Charset对象，还是要用{@link java.nio.charset.StandardCharsets}java官方提供的类常量获取。</li>
 * <li>FtpClient (需要 commons-net 依赖包)：好用的ftp工具类，但是默认没有提供连接池。</li>
 * <li>GenericObjectPool： (需要 commons-pool2 依赖包)：非常好用的连接池工具。</li>
 * </ul>
 *  
 * <p>
 * <strong>spring 好用的工具类</strong>
 * <ul>
 * <li>{@link org.springframework.web.util.WebUtils} 包含cookie,request,context等操作工具类</li>
 * <li>{@link org.springframework.util.ObjectUtils} 提供了很多null安全的对象处理方法，如toString，equals，hash等</li>
 * <li>{@link org.springframework.beans.BeanUtils} bean相关的反射操作工具类</li>
 * <li>{@link org.springframework.util.Assert} 断言工具类，可以在方法开头做参数判断</li>
 * <li>{@link org.springframework.util.CollectionUtils} 非常好用的集合操作工具类</li>
 * <li>{@link org.springframework.aop.support.AopUtils} spring aop工具类，可以判断是否代理类</li>
 * </ul>
 * 
 * <p>
 * <strong>spring 其他接口</strong>
 * <ul>
 * <li>{@link org.springframework.boot.ApplicationRunner} 系统启动完毕后执行的接口</li>
 * </ul>
 * 
 * @author Ghwolf
 *
 */
package com.gframework;
