package com.gframework.biz.sys.dao;

import com.gframework.biz.sys.entity.pojo.SysScheduled;
import com.gframework.mybatis.dao.IPrimaryDAO;
import com.gframework.mybatis.dao.ITableBasicDAO;

/** 
 * t_sys_scheduled表对应的DAO操作接口.<br>
 * 定时器任务表，此表的核心思想是，可以执行任意实现了特定标准的bean的任意方法或特定方法
 * 
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
public interface ISysScheduledDAO extends ITableBasicDAO<SysScheduled>, IPrimaryDAO<Long, SysScheduled> {
}