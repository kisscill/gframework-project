package com.gframework.biz.sys.entity.pojo;

import com.gframework.mybatis.entity.pojo.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/** 
 * {@value #TABLE_NAME} 表对应的POJO实体类.<br>
 * 定时器任务表，此表的核心思想是，可以执行任意实现了特定标准的bean的任意方法或特定方法
 *
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
@Table(name=SysScheduled.TABLE_NAME)
public class SysScheduled implements Serializable {
	private static final long serialVersionUID = 437711893L;

	/**
	 * 主键.
	 */
	@Column(name=SCHE_ID, nullable=false, precision=19)
	@Id
	@Authincrement
	private Long scheId;

	/**
	 * 任务名称.
	 */
	@Column(name=TASK_NAME, nullable=false, length=50)
	private String taskName;

	/**
	 * 描述.
	 */
	@Column(name=REMARK, nullable=true, length=255)
	private String remark;

	/**
	 * 执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制.
	 */
	@Column(name=EXECUTE_BEAN_NAME, nullable=false, length=50)
	private String executeBeanName;

	/**
	 * 是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间.
	 */
	@Column(name=DISPOSABLED, nullable=false, precision=3)
	private Integer disposabled;

	/**
	 * 生效时间，可不填，填写后表示此时间后定时器才有效.
	 */
	@Column(name=START_TIME, nullable=true)
	private Date startTime;

	/**
	 * 截止时间，可不填，填写后表示到此时间后将不会在生效.
	 */
	@Column(name=END_TIME, nullable=true)
	private Date endTime;

	/**
	 * CRON表达式.
	 */
	@Column(name=CRON, nullable=true, length=30)
	private String cron;

	/**
	 * 延迟执行间隔(毫秒)，一次执行结束后多久执行下一次.
	 */
	@Column(name=FIXED_DELAY, nullable=true, precision=19)
	private Long fixedDelay;

	/**
	 * 延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次.
	 */
	@Column(name=FIXED_DELAY_STRING, nullable=true, length=50)
	private String fixedDelayString;

	/**
	 * 循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕.
	 */
	@Column(name=FIXED_RATE, nullable=true, precision=19)
	private Long fixedRate;

	/**
	 * 循环执行间隔(毫秒)（从配置获取）.
	 */
	@Column(name=FIXED_RATE_STRING, nullable=true, length=50)
	private String fixedRateString;

	/**
	 * 首次执行延迟时间(毫秒).
	 */
	@Column(name=INITIAL_DELAY, nullable=true, precision=19)
	private Long initialDelay;

	/**
	 * 首次执行延迟时间(毫秒)（从配置获取）.
	 */
	@Column(name=INITIAL_DELAY_STRING, nullable=true, length=50)
	private String initialDelayString;

	/**
	 * 分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行.
	 */
	@Column(name=DISTRIBUTED_KEY, nullable=true, length=50)
	private String distributedKey;

	/**
	 * 分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间.
	 */
	@Column(name=LOCK_MODE, nullable=true, precision=3)
	private Integer lockMode;

	/**
	 * 分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒）.
	 */
	@Column(name=LOCK_LEASE_TIME, nullable=true, precision=10)
	private Integer lockLeaseTime;

	/**
	 * 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 */
	@Column(name=BUILTIN, nullable=false, precision=3)
	private Integer builtin;

	/**
	 * 删除标记,1:是，0:否.
	 */
	@Column(name=DELETED, nullable=false, precision=3)
	private Integer deleted;

	/** 当前POJO对应表名称：{@value} */
	public static final String TABLE_NAME = "t_sys_scheduled";

	/** scheId 属性 对应的列名称：{@value}。 主键 */
	public static final String SCHE_ID = "SCHE_ID";

	/** taskName 属性 对应的列名称：{@value}。 任务名称 */
	public static final String TASK_NAME = "TASK_NAME";

	/** remark 属性 对应的列名称：{@value}。 描述 */
	public static final String REMARK = "REMARK";

	/** executeBeanName 属性 对应的列名称：{@value}。 执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制 */
	public static final String EXECUTE_BEAN_NAME = "EXECUTE_BEAN_NAME";

	/** disposabled 属性 对应的列名称：{@value}。 是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间 */
	public static final String DISPOSABLED = "DISPOSABLED";

	/** startTime 属性 对应的列名称：{@value}。 生效时间，可不填，填写后表示此时间后定时器才有效 */
	public static final String START_TIME = "START_TIME";

	/** endTime 属性 对应的列名称：{@value}。 截止时间，可不填，填写后表示到此时间后将不会在生效 */
	public static final String END_TIME = "END_TIME";

	/** cron 属性 对应的列名称：{@value}。 CRON表达式 */
	public static final String CRON = "CRON";

	/** fixedDelay 属性 对应的列名称：{@value}。 延迟执行间隔(毫秒)，一次执行结束后多久执行下一次 */
	public static final String FIXED_DELAY = "FIXED_DELAY";

	/** fixedDelayString 属性 对应的列名称：{@value}。 延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次 */
	public static final String FIXED_DELAY_STRING = "FIXED_DELAY_STRING";

	/** fixedRate 属性 对应的列名称：{@value}。 循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕 */
	public static final String FIXED_RATE = "FIXED_RATE";

	/** fixedRateString 属性 对应的列名称：{@value}。 循环执行间隔(毫秒)（从配置获取） */
	public static final String FIXED_RATE_STRING = "FIXED_RATE_STRING";

	/** initialDelay 属性 对应的列名称：{@value}。 首次执行延迟时间(毫秒) */
	public static final String INITIAL_DELAY = "INITIAL_DELAY";

	/** initialDelayString 属性 对应的列名称：{@value}。 首次执行延迟时间(毫秒)（从配置获取） */
	public static final String INITIAL_DELAY_STRING = "INITIAL_DELAY_STRING";

	/** distributedKey 属性 对应的列名称：{@value}。 分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行 */
	public static final String DISTRIBUTED_KEY = "DISTRIBUTED_KEY";

	/** lockMode 属性 对应的列名称：{@value}。 分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间 */
	public static final String LOCK_MODE = "LOCK_MODE";

	/** lockLeaseTime 属性 对应的列名称：{@value}。 分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒） */
	public static final String LOCK_LEASE_TIME = "LOCK_LEASE_TIME";

	/** builtin 属性 对应的列名称：{@value}。 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改 */
	public static final String BUILTIN = "BUILTIN";

	/** deleted 属性 对应的列名称：{@value}。 删除标记,1:是，0:否 */
	public static final String DELETED = "DELETED";

	public SysScheduled() {
		
	}

	/**
	 * 设置 主键.
	 * @param scheId 主键
	 */
	public void setScheId(Long scheId) {
		this.scheId = scheId ;
	}

	/**
	 * 取得 主键.
	 * @return 主键
	 */
	@Column(name=SCHE_ID, nullable=false, precision=19)
	@Id
	@Authincrement
	public Long getScheId() {
		return this.scheId ;
	}

	/**
	 * 设置 任务名称.
	 * @param taskName 任务名称
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName ;
	}

	/**
	 * 取得 任务名称.
	 * @return 任务名称
	 */
	@Column(name=TASK_NAME, nullable=false, length=50)
	public String getTaskName() {
		return this.taskName ;
	}

	/**
	 * 设置 描述.
	 * @param remark 描述
	 */
	public void setRemark(String remark) {
		this.remark = remark ;
	}

	/**
	 * 取得 描述.
	 * @return 描述
	 */
	@Column(name=REMARK, nullable=true, length=255)
	public String getRemark() {
		return this.remark ;
	}

	/**
	 * 设置 执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制.
	 * @param executeBeanName 执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制
	 */
	public void setExecuteBeanName(String executeBeanName) {
		this.executeBeanName = executeBeanName ;
	}

	/**
	 * 取得 执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制.
	 * @return 执行BEAN名称，这个bean要求是实现了特定接口的，这个由程序自己控制
	 */
	@Column(name=EXECUTE_BEAN_NAME, nullable=false, length=50)
	public String getExecuteBeanName() {
		return this.executeBeanName ;
	}

	/**
	 * 设置 是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间.
	 * @param disposabled 是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间
	 */
	public void setDisposabled(Integer disposabled) {
		this.disposabled = disposabled ;
	}

	/**
	 * 取得 是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间.
	 * @return 是否一次性任务,1：是，0：否，如果为1，那么执行时间就是生效时间
	 */
	@Column(name=DISPOSABLED, nullable=false, precision=3)
	public Integer getDisposabled() {
		return this.disposabled ;
	}

	/**
	 * 设置 生效时间，可不填，填写后表示此时间后定时器才有效.
	 * @param startTime 生效时间，可不填，填写后表示此时间后定时器才有效
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime ;
	}

	/**
	 * 取得 生效时间，可不填，填写后表示此时间后定时器才有效.
	 * @return 生效时间，可不填，填写后表示此时间后定时器才有效
	 */
	@Column(name=START_TIME, nullable=true)
	public Date getStartTime() {
		return this.startTime ;
	}

	/**
	 * 设置 截止时间，可不填，填写后表示到此时间后将不会在生效.
	 * @param endTime 截止时间，可不填，填写后表示到此时间后将不会在生效
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime ;
	}

	/**
	 * 取得 截止时间，可不填，填写后表示到此时间后将不会在生效.
	 * @return 截止时间，可不填，填写后表示到此时间后将不会在生效
	 */
	@Column(name=END_TIME, nullable=true)
	public Date getEndTime() {
		return this.endTime ;
	}

	/**
	 * 设置 CRON表达式.
	 * @param cron CRON表达式
	 */
	public void setCron(String cron) {
		this.cron = cron ;
	}

	/**
	 * 取得 CRON表达式.
	 * @return CRON表达式
	 */
	@Column(name=CRON, nullable=true, length=30)
	public String getCron() {
		return this.cron ;
	}

	/**
	 * 设置 延迟执行间隔(毫秒)，一次执行结束后多久执行下一次.
	 * @param fixedDelay 延迟执行间隔(毫秒)，一次执行结束后多久执行下一次
	 */
	public void setFixedDelay(Long fixedDelay) {
		this.fixedDelay = fixedDelay ;
	}

	/**
	 * 取得 延迟执行间隔(毫秒)，一次执行结束后多久执行下一次.
	 * @return 延迟执行间隔(毫秒)，一次执行结束后多久执行下一次
	 */
	@Column(name=FIXED_DELAY, nullable=true, precision=19)
	public Long getFixedDelay() {
		return this.fixedDelay ;
	}

	/**
	 * 设置 延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次.
	 * @param fixedDelayString 延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次
	 */
	public void setFixedDelayString(String fixedDelayString) {
		this.fixedDelayString = fixedDelayString ;
	}

	/**
	 * 取得 延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次.
	 * @return 延迟执行间隔(毫秒)（从配置获取），一次执行结束后多久执行下一次
	 */
	@Column(name=FIXED_DELAY_STRING, nullable=true, length=50)
	public String getFixedDelayString() {
		return this.fixedDelayString ;
	}

	/**
	 * 设置 循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕.
	 * @param fixedRate 循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕
	 */
	public void setFixedRate(Long fixedRate) {
		this.fixedRate = fixedRate ;
	}

	/**
	 * 取得 循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕.
	 * @return 循环执行间隔(毫秒)，每隔一定时间创建一个任务，不管之前有没有执行完毕
	 */
	@Column(name=FIXED_RATE, nullable=true, precision=19)
	public Long getFixedRate() {
		return this.fixedRate ;
	}

	/**
	 * 设置 循环执行间隔(毫秒)（从配置获取）.
	 * @param fixedRateString 循环执行间隔(毫秒)（从配置获取）
	 */
	public void setFixedRateString(String fixedRateString) {
		this.fixedRateString = fixedRateString ;
	}

	/**
	 * 取得 循环执行间隔(毫秒)（从配置获取）.
	 * @return 循环执行间隔(毫秒)（从配置获取）
	 */
	@Column(name=FIXED_RATE_STRING, nullable=true, length=50)
	public String getFixedRateString() {
		return this.fixedRateString ;
	}

	/**
	 * 设置 首次执行延迟时间(毫秒).
	 * @param initialDelay 首次执行延迟时间(毫秒)
	 */
	public void setInitialDelay(Long initialDelay) {
		this.initialDelay = initialDelay ;
	}

	/**
	 * 取得 首次执行延迟时间(毫秒).
	 * @return 首次执行延迟时间(毫秒)
	 */
	@Column(name=INITIAL_DELAY, nullable=true, precision=19)
	public Long getInitialDelay() {
		return this.initialDelay ;
	}

	/**
	 * 设置 首次执行延迟时间(毫秒)（从配置获取）.
	 * @param initialDelayString 首次执行延迟时间(毫秒)（从配置获取）
	 */
	public void setInitialDelayString(String initialDelayString) {
		this.initialDelayString = initialDelayString ;
	}

	/**
	 * 取得 首次执行延迟时间(毫秒)（从配置获取）.
	 * @return 首次执行延迟时间(毫秒)（从配置获取）
	 */
	@Column(name=INITIAL_DELAY_STRING, nullable=true, length=50)
	public String getInitialDelayString() {
		return this.initialDelayString ;
	}

	/**
	 * 设置 分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行.
	 * @param distributedKey 分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行
	 */
	public void setDistributedKey(String distributedKey) {
		this.distributedKey = distributedKey ;
	}

	/**
	 * 取得 分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行.
	 * @return 分布式锁,可以是空(需要程序支持)
            任意填写一个全局唯一的key。如果同名key已经执行了，那么将不会执行
	 */
	@Column(name=DISTRIBUTED_KEY, nullable=true, length=50)
	public String getDistributedKey() {
		return this.distributedKey ;
	}

	/**
	 * 设置 分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间.
	 * @param lockMode 分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间
	 */
	public void setLockMode(Integer lockMode) {
		this.lockMode = lockMode ;
	}

	/**
	 * 取得 分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间.
	 * @return 分布式锁模式，
            1：表示onlyOne模式，既同一阶段内，只要有一个执行了，其他的就不会执行。
            2：表示synchronized模式，都要执行，但是一次只有一个会执行。这个模式需要设置租赁锁的时间
	 */
	@Column(name=LOCK_MODE, nullable=true, precision=3)
	public Integer getLockMode() {
		return this.lockMode ;
	}

	/**
	 * 设置 分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒）.
	 * @param lockLeaseTime 分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒）
	 */
	public void setLockLeaseTime(Integer lockLeaseTime) {
		this.lockLeaseTime = lockLeaseTime ;
	}

	/**
	 * 取得 分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒）.
	 * @return 分布式锁租赁时间，只有在synchronized模式下才会有这个值，这是为了防止死锁发生，你需要预估你当前任务可能的执行时间，并给与设置比他稍大一点的值
            （单位秒）
	 */
	@Column(name=LOCK_LEASE_TIME, nullable=true, precision=10)
	public Integer getLockLeaseTime() {
		return this.lockLeaseTime ;
	}

	/**
	 * 设置 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 * @param builtin 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改
	 */
	public void setBuiltin(Integer builtin) {
		this.builtin = builtin ;
	}

	/**
	 * 取得 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 * @return 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改
	 */
	@Column(name=BUILTIN, nullable=false, precision=3)
	public Integer getBuiltin() {
		return this.builtin ;
	}

	/**
	 * 设置 删除标记,1:是，0:否.
	 * @param deleted 删除标记,1:是，0:否
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted ;
	}

	/**
	 * 取得 删除标记,1:是，0:否.
	 * @return 删除标记,1:是，0:否
	 */
	@Column(name=DELETED, nullable=false, precision=3)
	public Integer getDeleted() {
		return this.deleted ;
	}
}