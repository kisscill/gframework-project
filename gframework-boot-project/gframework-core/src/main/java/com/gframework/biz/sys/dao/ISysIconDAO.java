package com.gframework.biz.sys.dao;

import com.gframework.biz.sys.entity.pojo.SysIcon;
import com.gframework.mybatis.dao.IPrimaryDAO;
import com.gframework.mybatis.dao.ITableBasicDAO;

/** 
 * t_sys_icon表对应的DAO操作接口.<br>
 * 图标表，此表存储两种类型的图标：
1、base64小图标（最多64kb）
2、class指定的图
 * 
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
public interface ISysIconDAO extends ITableBasicDAO<SysIcon>, IPrimaryDAO<Long, SysIcon> {
}