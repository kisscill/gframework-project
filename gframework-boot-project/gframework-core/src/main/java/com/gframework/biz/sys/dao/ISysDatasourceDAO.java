package com.gframework.biz.sys.dao;

import com.gframework.biz.sys.entity.pojo.SysDatasource;
import com.gframework.mybatis.dao.IPrimaryDAO;
import com.gframework.mybatis.dao.ITableBasicDAO;

/** 
 * t_sys_datasource表对应的DAO操作接口.<br>
 * 数据源表
 * 
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
public interface ISysDatasourceDAO extends ITableBasicDAO<SysDatasource>, IPrimaryDAO<String, SysDatasource> {
}