package com.gframework.biz.sys.entity.pojo;

import com.gframework.mybatis.entity.pojo.*;
import java.io.Serializable;
import javax.persistence.*;

/** 
 * {@value #TABLE_NAME} 表对应的POJO实体类.<br>
 * 系统配置表
 *
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
@Table(name=SysParameter.TABLE_NAME)
public class SysParameter implements Serializable {
	private static final long serialVersionUID = -123108677L;

	/**
	 * 主键.
	 */
	@Column(name=PARAM_ID, nullable=false, precision=19)
	@Id
	@Authincrement
	private Long paramId;

	/**
	 * 配置项名称.
	 */
	@Column(name=PARAM_KEY, nullable=false, length=64)
	private String paramKey;

	/**
	 * 配置项值.
	 */
	@Column(name=PARAM_VALUE, nullable=true, length=255)
	private String paramValue;

	/**
	 * 默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。.
	 */
	@Column(name=DEFAULT_VALUE, nullable=true, length=255)
	private String defaultValue;

	/**
	 * 描述.
	 */
	@Column(name=REMARK, nullable=true, length=255)
	private String remark;

	/**
	 * 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 */
	@Column(name=BUILTIN, nullable=false, precision=3)
	private Integer builtin;

	/**
	 * 删除标记,1:是，0:否.
	 */
	@Column(name=DELETED, nullable=false, precision=3)
	private Integer deleted;

	/** 当前POJO对应表名称：{@value} */
	public static final String TABLE_NAME = "t_sys_parameter";

	/** paramId 属性 对应的列名称：{@value}。 主键 */
	public static final String PARAM_ID = "PARAM_ID";

	/** paramKey 属性 对应的列名称：{@value}。 配置项名称 */
	public static final String PARAM_KEY = "PARAM_KEY";

	/** paramValue 属性 对应的列名称：{@value}。 配置项值 */
	public static final String PARAM_VALUE = "PARAM_VALUE";

	/** defaultValue 属性 对应的列名称：{@value}。 默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。 */
	public static final String DEFAULT_VALUE = "DEFAULT_VALUE";

	/** remark 属性 对应的列名称：{@value}。 描述 */
	public static final String REMARK = "REMARK";

	/** builtin 属性 对应的列名称：{@value}。 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改 */
	public static final String BUILTIN = "BUILTIN";

	/** deleted 属性 对应的列名称：{@value}。 删除标记,1:是，0:否 */
	public static final String DELETED = "DELETED";

	public SysParameter() {
		
	}

	/**
	 * 设置 主键.
	 * @param paramId 主键
	 */
	public void setParamId(Long paramId) {
		this.paramId = paramId ;
	}

	/**
	 * 取得 主键.
	 * @return 主键
	 */
	@Column(name=PARAM_ID, nullable=false, precision=19)
	@Id
	@Authincrement
	public Long getParamId() {
		return this.paramId ;
	}

	/**
	 * 设置 配置项名称.
	 * @param paramKey 配置项名称
	 */
	public void setParamKey(String paramKey) {
		this.paramKey = paramKey ;
	}

	/**
	 * 取得 配置项名称.
	 * @return 配置项名称
	 */
	@Column(name=PARAM_KEY, nullable=false, length=64)
	public String getParamKey() {
		return this.paramKey ;
	}

	/**
	 * 设置 配置项值.
	 * @param paramValue 配置项值
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue ;
	}

	/**
	 * 取得 配置项值.
	 * @return 配置项值
	 */
	@Column(name=PARAM_VALUE, nullable=true, length=255)
	public String getParamValue() {
		return this.paramValue ;
	}

	/**
	 * 设置 默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。.
	 * @param defaultValue 默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue ;
	}

	/**
	 * 取得 默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。.
	 * @return 默认值，通常内置参数才需要此字段。如果用户修改了内置参数，如果要还原成默认设置，可以通过此设置来完成。
	 */
	@Column(name=DEFAULT_VALUE, nullable=true, length=255)
	public String getDefaultValue() {
		return this.defaultValue ;
	}

	/**
	 * 设置 描述.
	 * @param remark 描述
	 */
	public void setRemark(String remark) {
		this.remark = remark ;
	}

	/**
	 * 取得 描述.
	 * @return 描述
	 */
	@Column(name=REMARK, nullable=true, length=255)
	public String getRemark() {
		return this.remark ;
	}

	/**
	 * 设置 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 * @param builtin 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改
	 */
	public void setBuiltin(Integer builtin) {
		this.builtin = builtin ;
	}

	/**
	 * 取得 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 * @return 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改
	 */
	@Column(name=BUILTIN, nullable=false, precision=3)
	public Integer getBuiltin() {
		return this.builtin ;
	}

	/**
	 * 设置 删除标记,1:是，0:否.
	 * @param deleted 删除标记,1:是，0:否
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted ;
	}

	/**
	 * 取得 删除标记,1:是，0:否.
	 * @return 删除标记,1:是，0:否
	 */
	@Column(name=DELETED, nullable=false, precision=3)
	public Integer getDeleted() {
		return this.deleted ;
	}
}