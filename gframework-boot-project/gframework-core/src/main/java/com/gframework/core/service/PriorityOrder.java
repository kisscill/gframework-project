package com.gframework.core.service;


/**
 * 本接口提供由一个可以判断对象优先级的操作方法.<br>
 * 你可以通过此方法进行优先级排序，也可以在使用过程在在进行多次判断。
 * 如果你要进行排序操作，你可以使用 {@link PriorityOrderUtil} 类即可完成。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see PriorityOrderUtil
 */
public interface PriorityOrder {
	/**
	 * 优先级，数字越小优先级越高.
	 * @return 返回一个整数，数字越小优先级越高
	 */
	public int priority() ;
}
