package com.gframework.core.service.spring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 批量DataDictionarySource统一操作存储类.
 * 本类可以将多个DataDictionarySource合并成一个进行操作。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class MultipartDataDictionarySource extends DataDictionarySource {

	private final DataDictionarySource[] sources;
	
	public MultipartDataDictionarySource(String type, DataDictionarySource[] sources) {
		super(type);
		this.sources = sources == null ? new DataDictionarySource[0] : sources;
	}

	public MultipartDataDictionarySource(String type, List<DataDictionarySource> sources) {
		this(type, sources.toArray(new DataDictionarySource[sources.size()]));
	}

	@Override
	public String getData(String code) {
		for (int x = 0; x < sources.length; x ++) {
			String value = sources[x].getData(code);
			if (value != null) return value;
		}
		return null;
	}

	@Override
	public Map<String, String> getDataMap() {
		Map<String, String> map = new HashMap<>(64);
		for (int x = 0; x < sources.length; x ++) {
			map.putAll(sources[x].getDataMap());
		}
		return map;
	}

	@Override
	public Collection<String> getCodes() {
		List<String> codes = new ArrayList<>(64);
		for (int x = 0; x < sources.length; x ++) {
			codes.addAll(sources[x].getCodes());
		}
		return codes;
	}

	@Override
	public Collection<String> getValues() {
		List<String> values = new ArrayList<>(64);
		for (int x = 0; x < sources.length; x ++) {
			values.addAll(sources[x].getValues());
		}
		return values;
	}

}
