package com.gframework.core.service;

/**
 * 这是一个标注可重加载的标识接口，通常用来标记功能类/数据缓存类，可以被统一调度进行数据重加载.
 * <p>重新加载的逻辑需要卸到{@link #reload()}方法中。
 * <p>此接口用于定义一种规范，但是并不提供重载方法调用的具体逻辑，使用者在自己的具体业务中，可以通过获取此接口所有的子类bean来进行重载的触发。
 * @since 2.0.0
 * @author Ghwolf
 */
@FunctionalInterface
public interface Reloadable {
	/**
	 * 重新加载操作
	 */
	public void reload() ;
	
}
