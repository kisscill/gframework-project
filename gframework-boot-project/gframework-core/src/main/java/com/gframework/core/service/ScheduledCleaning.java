package com.gframework.core.service;

/**
 * 定期清理操作根接口.<br>
 * 实现此接口就表明当前类存在一些脏数据需要定期清理。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@FunctionalInterface
public interface ScheduledCleaning {
	/**
	 * 清理操作
	 */
	public void clear() ;
	
}
