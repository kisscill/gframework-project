package com.gframework.core.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.lang.Nullable;

/**
 * 排序操作工具类.
 * <p>
 * 本类可以实现对象集合的排序操作，但是所有对象必须是 {@link PriorityOrder} 接口的子类
 * </p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see PriorityOrder
 */
public class PriorityOrderUtil {
	
	private PriorityOrderUtil(){
	}
	
	/**
	 * 本方法可以对PriorityOrder接口对象数组进行(升序)排序，
	 * 此排序会修改原有集合的顺序。
	 * @param arr 要进行排序的集合，如果为null则不做任何事情
	 */
	public static <T extends PriorityOrder> void sort(@Nullable T[] arr) {
		sort(arr,false);
	}
	/**
	 * 本方法可以对PriorityOrder接口对象数组进行(自定义升降序的)排序，
	 * 此排序会修改原有集合的顺序。
	 * @param arr 要进行排序的集合，如果为null则不做任何事情
	 * @param desc true表示降序排序，false表示升序排序
	 */
	public static <T extends PriorityOrder> void sort(@Nullable T[] arr,boolean desc) {
		if (arr == null || arr.length == 0) return ;
		if (desc) {
			Arrays.sort(arr,(a,b) -> a.priority() - b.priority());
		} else {
			Arrays.sort(arr,(a,b) -> b.priority() - a.priority());
		}
	}
	
	/**
	 * 本方法可以对PriorityOrder接口对象数组进行(升序)排序，
	 * 此排序会修改原有集合的顺序。
	 * @param list 要进行排序的集合，如果为null则不做任何事情
	 */
	public static <T extends PriorityOrder> void sort(@Nullable List<T> list) {
		sort(list,false);
	}
	/**
	 * 本方法可以对PriorityOrder接口对象数组进行(自定义升降序的)排序，
	 * 此排序会修改原有集合的顺序。
	 * @param list 要进行排序的集合，如果为null则不做任何事情
	 * @param desc true表示降序排序，false表示升序排序
	 */
	public static <T extends PriorityOrder> void sort(@Nullable List<T> list,boolean desc) {
		if (list == null || list.size() == 0) return ;
		if (desc) {
			Collections.sort(list,(a,b) -> a.priority() - b.priority());
		} else {
			Collections.sort(list,(a,b) -> b.priority() - a.priority());
		}
	}
	
	

}
