package com.gframework.core.redis;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * gframework框架 多redis 配置
 * 
 * @author Ghwolf
 */
@ConfigurationProperties("gframework.redis")
public class GFrameworkRedisProperties {

	/**
	 * 开关redis配置，默认false
	 */
	private boolean enable = false;
	/**
	 * 多个redis配置，主redis配置仅有一个就是spring.redis.*的配置.
	 * <p>key值将会作为bean名称的一部分，完整名称为："multipart-redis--" + key。类型为RedissonClient
	 */
	private Map<String, RedisProperties> config = new HashMap<>();

	public boolean isEnable() {
		return this.enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public Map<String, RedisProperties> getConfig() {
		return this.config;
	}

	public void setConfig(Map<String, RedisProperties> config) {
		this.config = config;
	}

}
