package com.gframework.core.service.spring;

import java.util.List;

import com.gframework.core.service.PriorityOrder;

/**
 * 数据字段信息加载扩展接口.
 * <p>
 * 你可以通过创建此接口的bean来初始化数据字典信息，数据字典和配置信息不一样，
 * 他不是那么的着急初始化，因此他的初始化会靠后执行，所有请不要再其他bean初始化过程中就去
 * 获取字典信息。
 * </p>
 *
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see DataDictionarySource
 * @see DynamicSource
 * @see DataDictionary
 *
 */
public interface ExtendDataDictionary extends PriorityOrder{

	/**
	 * 加载数据字典信息.
	 * @return 结果封装为DataDictionarySource的List集合返回
	 * @implSpec
	 * 取得的DataDictionarySource默认都认为是可缓存的数据，如果你的字典获取操作是不可缓存的，
	 * 例如每次都需要通过文件或网络获取，那么就让你的DataDictionarySource子类实现{@link DynamicSource}接口。
	 */	
	public List<DataDictionarySource> loadDataDictionary() ;
	
}
