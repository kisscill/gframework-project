package com.gframework.core.middleware;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 公共共用同步httpclient配置信息
 * 
 * @author Ghwolf
 */
@ConfigurationProperties(prefix = "gframework.middleware.httpclient.sync")
public class CommonHttpClientProperties {

	/**
	 * 连接池最大连接数
	 */
	private int maxPoolSize = 300;
	/**
	 * 同一个url的最大请求数
	 */
	private int maxPerRoute = 100;
	/**
	 * 活跃状态监测时间（毫秒），超过这个时间没有使用，那么就要重新验证是否处于活跃状态。
	 */
	private int validateAfterInactivity = 1000 * 5;
	/**
	 * 连接请求超时时间(毫秒)
	 */
	private int defaultTimeout = 2000;

	/**
	 * 取得 活跃状态监测时间（毫秒），超过这个时间没有使用，那么就要重新验证是否处于活跃状态。
	 * 
	 * @return 活跃状态监测时间（毫秒）
	 */
	public int getValidateAfterInactivity() {
		return this.validateAfterInactivity;
	}

	/**
	 * 设置 活跃状态监测时间（毫秒），超过这个时间没有使用，那么就要重新验证是否处于活跃状态。
	 * 
	 * @param validateAfterInactivity 活跃状态监测时间（毫秒）
	 */
	public void setValidateAfterInactivity(int validateAfterInactivity) {
		this.validateAfterInactivity = validateAfterInactivity;
	}

	/**
	 * 取得 连接请求超时时间(毫秒)
	 * 
	 * @return 连接请求超时时间(毫秒)
	 */
	public int getDefaultTimeout() {
		return this.defaultTimeout;
	}

	/**
	 * 设置 连接请求超时时间(毫秒)
	 * 
	 * @param defaultTimeout 连接请求超时时间(毫秒)
	 */
	public void setDefaultTimeout(int defaultTimeout) {
		this.defaultTimeout = defaultTimeout;
	}

	/**
	 * 取得 连接池最大连接数
	 * 
	 * @return 连接池最大连接数
	 */
	public int getMaxPoolSize() {
		return this.maxPoolSize;
	}

	/**
	 * 设置 连接池最大连接数
	 * 
	 * @param maxPoolSize 连接池最大连接数
	 */
	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	/**
	 * 取得 同一个url的最大请求数
	 * 
	 * @return 同一个url的最大请求数
	 */
	public int getMaxPerRoute() {
		return this.maxPerRoute;
	}

	/**
	 * 设置 同一个url的最大请求数
	 * 
	 * @param maxPerRoute 同一个url的最大请求数
	 */
	public void setMaxPerRoute(int maxPerRoute) {
		this.maxPerRoute = maxPerRoute;
	}
}
