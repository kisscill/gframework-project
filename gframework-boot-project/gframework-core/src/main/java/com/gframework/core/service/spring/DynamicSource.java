package com.gframework.core.service.spring;

/**
 * 动态数据标识接口.
 * 本类不具备任何方法，仅作为一种标志，表示当前类的数据是动态的，可能在不停的变化，不能够被缓存的。
 * <p>
 * 例如在使用 {@link DataDictionarySource} 扩展类的时候，就可以通过此接口来表示当前数据源是动态的，
 * 那么 {@link DataDictionary} 就不会将他进行缓存，而是每次都会进行查询。
 * </p>
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see DataDictionarySource
 * @see DataDictionary
 *
 */
public interface DynamicSource {
}
