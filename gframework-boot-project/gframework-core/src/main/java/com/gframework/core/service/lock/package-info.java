/**
 * 锁，分布式锁相关支持库.
 * <p>
 * 如果你需要使用普通的锁，可以使用 
 * synchronized (非公平锁) 或
 * {@link java.util.concurrent.locks.ReentrantLock ReentrantLock} (公平锁)。
 * 如果要涉及到分布式锁，那么可以使用 
 * {@link com.gframework.boot.mvc.service.lock.DistributedLock DistributedLock}
 * </p>
 * @since 1.0.0
 * @author Ghwolf
 */
package com.gframework.core.service.lock;