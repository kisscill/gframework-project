/**
 * 定时器任务相关操作处理核心代码所在包.<br>
 * <p>
 * 定时任务就像是任务队列一样，到了指定时间就把任务添加到队列中去执行，如果有多线程，就多个线程去执行，
 * 这是非常标准的生产者消费者模式。如果是单线程，那么这个模型就和js中的方法队列非常相似。
 * </p>
 * spring定时器简介：
 * <pre>
 * 1、有两种配置方式：
 * <strong>方式1——注解方式</strong>：
 * {@link org.springframework.scheduling.annotation.Scheduled}
 * 这种方式不能指定{@link org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler}
 * 
 * <strong>方式2——接口方式</strong>：
 * 任何一个spring的bean实现{@link org.springframework.scheduling.annotation.SchedulingConfigurer}接口，实现方法即可。
 * 这话总方式可以指定{@link org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler}
 * 
 * 2、spring的定时任务是并行还是串行由{@link org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler}来决定。
 * spring默认会生成一个此类的bean实例，你也可以自己定一个此类bean实例，那么spring将不会采用自己默认生成的。该类可以设置poolSize来决定线程大小，更多配置可以参考此类javadoc。
 * 
 * 3、<strong>关于如何动态刷新任务</strong>
 * ThreadPoolTaskScheduler 类创建定时任务后会取得 {@link java.util.concurrent.ScheduledFuture}接口对象，其表示一个定时任务，可以调用他的
 * {@link java.util.concurrent.ScheduledFuture#cancel(boolean)}方法停止任务，然后重新打开任务就可以了。因此最好通过第二种方式创建任务，或者直接手动创建任务，然后自定义一个新的ThreadPoolTaskScheduler，
 * 自己管理ScheduledFuture类对象。
 * 
 * 4、其中有一点需要注意：
 * fixedDelay指的是任务结束后多久执行下一次，
 * 用js来模拟就类似于：
 * ```javascript
 * function fun(){
 * 	setTimeout(fun,1000);
 * }
 * setTimeout(fun,1000);
 * ```
 * fixedRate指的是定时将任务添加到队列，不管上一次有没有执行完毕。<strong>这可能导致队列溢出</strong>。
 * 用js来模拟就类似于：
 * ```javascript
 * function fun(){
 * }
 * setInterval(fun,1000);
 * ```
 * </pre>
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
package com.gframework.core.service.scheduled;