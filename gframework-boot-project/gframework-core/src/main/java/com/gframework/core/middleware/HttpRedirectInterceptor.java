package com.gframework.core.middleware;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

/**
 * httpClient，重定向拦截器函数式接口.
 *
 * <p>除了默认的重定向请求外，有些时候由于前台是通过html或js主动跳转，没有任何302发生，这个时候
 * 可以拦截并获取response数据，根据页面内容，再次发生重定向
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@FunctionalInterface
public interface HttpRedirectInterceptor {
	
	/**
	 * 是否应当发生重定向.
	 * 
	 * @param request HttpRequest对象  
	 * @param response HttpResponse对象
	 * @param context Http上下文
	 * @param isRedirect 默认策略认为的是否应当重定向
	 * @return 是否应当发生重定向
	 */
	public boolean isRedirect(HttpRequest request, HttpResponse response, HttpContext context, boolean isRedirect) throws ProtocolException;

	/**
	 * 拦截并自定义重定向请求对象.
	 * 
	 * <p>默认不去拦截此操作，如果你希望在即便不发生302或301的时候，根据页面内容也去重定向，那么可以复写此操作
	 * 
	 * @param request HttpRequest对象  
	 * @param response HttpResponse对象
	 * @param context Http上下文
	 * @return 返回要进行重定向请求的Http对象，如果返回null，则执行默认的重定向策略
	 */
	default HttpUriRequest getRedirect(HttpRequest request, HttpResponse response, HttpContext context)
			throws ProtocolException {
		return null;
	}
	
	
	/**
	 * 是否只有在默认策略认为需要重定向的时候才拦截，默认true.
	 * 
	 * <p>true则表示只有真正出现302,301这类状态的时候才回去拦截，否则即便是状态为200也会拦截。
	 * 
	 * @return true表示只有默认策略认为需要重定向才拦截，否则全部拦截。
	 */
	default boolean interceptorIfTrue() {
		return true ;
	}
	
}
