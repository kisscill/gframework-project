package com.gframework.core.service.spring;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 使用Map进行字典信息存储的类.
 * <p>
 * 本类所使用的Map是线程安全的{@link ConcurrentHashMap}，虽然构造方法支持传入Map，但不会直接使用此map，
 * 而是会将该map中的值全部put到本类自己的map中。
 * </p>
 *
 * @since 1.0.0
 * @author Ghwolf
 */
public class MapDataDictionarySource extends DataDictionarySource {

	/**
	 * 存储数据的map，使用的是线程安全的ConcurrentHashMap
	 */
	private final Map<String, String> data;

	/**
	 * 创建一个空的，以Map方式进行存储的数据字典获取对象
	 * 
	 * @param type 数据字典类型
	 */
	public MapDataDictionarySource(String type) {
		this(type, null);
	}

	/**
	 * 创建一个含有初始值的，以Map方式进行存储的数据字典获取对象
	 * 
	 * @param type 数据字典类型
	 * @param data 初始参数，此map不会被直接使用，而是会将他的值复制到本类自己的map中
	 */
	public MapDataDictionarySource(String type, Map<String, String> data) {
		super(type);
		boolean hasData = data != null && !data.isEmpty();
		this.data = new ConcurrentHashMap<>(hasData ? data.size() : 16);
		if (hasData) {
			this.data.putAll(data);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getData(String code) {
		return this.data.get(code);
	}
	
	/**
	 * 设置一个新的数据字典映射关系.
	 * 这可能会覆盖旧的值。
	 * @param code 数据字典的code
	 * @param value 数据字典值
	 */
	public void set(String code,String value) {
		this.data.put(code, value);
	}
	
	/**
	 * 设置多个新的数据字典映射关系.
	 * 这可能会覆盖旧的值。
	 * @param data 数据字典映射关系集合
	 */
	public void set(Map<String,String> data) {
		this.data.putAll(data);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getDataMap() {
		return Collections.unmodifiableMap(this.data);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> getCodes() {
		return this.data.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> getValues() {
		return this.data.values();
	}

}
