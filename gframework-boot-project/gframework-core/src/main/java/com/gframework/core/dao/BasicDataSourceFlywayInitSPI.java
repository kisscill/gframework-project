package com.gframework.core.dao;

import java.util.Collections;
import java.util.List;

import org.flywaydb.core.api.FlywayException;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import com.gframework.boot.flyway.IFlywayExtendSPI;

/**
 * gframework 框架默认数据表flyway创建支持类.
 * <p>本类处理会加载
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@Order(-10000)
public class BasicDataSourceFlywayInitSPI implements IFlywayExtendSPI{

	/**
	 * gframework 框架内置支持库的flyway sql脚本路径
	 */
	private static final String GFRAMEWORK_FLYWAY_SQL_PATH_PREFIX = "classpath:gflyway-base-sql/";
	
	@Override
	public List<String> getLocation(String url, Environment environment) {
		if (StringUtils.isEmpty(url)) {
			return Collections.emptyList();
		}
		boolean enabled = !Boolean.FALSE.equals(environment.getProperty("gframework.flyway.init-gframework-basic-table",Boolean.class));
		
		if (!enabled) {
			return Collections.emptyList();
		}
		
		if (StringUtils.startsWithIgnoreCase(url, "jdbc:mysql") ||
				StringUtils.startsWithIgnoreCase(url, "jdbc:mariadb")) {
			return Collections.singletonList(GFRAMEWORK_FLYWAY_SQL_PATH_PREFIX + "mysql");
		} else {
			throw new FlywayException("\n【==== 警告 ====】\n目前 gframework 框架内置支持库sql仅支持mysql/mariadb，其他类型暂不支持！\n目前url为：" + url + "\n可以通过配置：gframework.flyway.init-gframework-basic-table=false 来关闭基础库创建！");
		}
		
	}

}
