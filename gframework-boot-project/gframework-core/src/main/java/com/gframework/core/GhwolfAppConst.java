package com.gframework.core;

/**
 * 框架内常量
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface GhwolfAppConst {
	/**
	 * 当前版本
	 */
	public String VERSION = "2.0.0";
}
