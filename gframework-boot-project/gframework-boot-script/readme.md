## springboot 启动脚本

此包仅包含springboot的linux启动脚本，并且它仅适用于基于`gframework-boot-starter`打包模式。

`gframework-boot-starter`打包后，会以`tar.gz`包的形式存在，分为

* bin
* lib
* config
* classpath

四个目录，包括项目本身都会以jar包形式存在于`lib`目录下，`lib`目录以及`classpath`目录均为运行时classpath。

在linux中，可以通过启动`restart.sh`来启动/重启系统。
执行restart.sh后，会在sh文件的上级目录创建

* `pid.file`：仅包含启动程序的pid
* `main-class.file`：仅包含启动主类
* `main-jar.file`：仅包含主类所属jar名称

三个文件。

通过执行`stop.sh`可以停止程序。这类似于直接kill进程然后删除pid.file文件。
