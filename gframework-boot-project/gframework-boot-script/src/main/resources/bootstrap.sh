#!/bin/bash

# java opt
JAVA_OPT="-XX:+UseG1GC -Xmx1024m -Xms1024m "
MF_MAIN_CLASS_PREFIX="Boot-Main-Class:"

######### PARAMS #########
cd `dirname $0`
cd ..
PWD=`pwd`

PID_FILE=pid.file
JARFILE_NAME_FILE=main-jar.file

RUNNING=N

# startup jar file name
JARFILE_NAME=
if [ -f $JARFILE_NAME_FILE ]; then
        JARFILE_NAME=`cat $JARFILE_NAME_FILE`
fi


######### GET PID #########

if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        if [ ! -z "$PID" ] && kill -0 $PID 2>/dev/null; then
                RUNNING=Y
        fi
fi

######### FUNCTION #########

init()
{
	# no main-jar file or no main-class file
	if [ -z "$JARFILE_NAME" ] ; then
		rm -f $JARFILE_NAME_FILE;
		
		# the path is used to save temporary files
		MF_DIR=/tmp/bootstart/"$RANDOM";
		mkdir -p $MF_DIR

		# loop to unzip
		for file in "$PWD"/lib/*; do
			unzip -jo $file "META-INF/MANIFEST.MF" -d $MF_DIR > /dev/null
			if [ -f "$MF_DIR"/MANIFEST.MF ]; then
				# compatible windows \r and delete it
				sed -i 's/\r$//' "$MF_DIR"/MANIFEST.MF

				# get main class and handling line breaks
				FIND_FLAG="N"
				MAIN_CLASS=
				while read line; do
					# line is trim
					if [[ $line == "$MF_MAIN_CLASS_PREFIX"* ]]; then
						if [[ $FIND_FLAG == "Y" ]]; then
							break ;
						fi
						FIND_FLAG="Y"
						MAIN_CLASS=$line
						MAIN_CLASS=${MAIN_CLASS#*:};
						# trim string
						MAIN_CLASS=`echo $MAIN_CLASS`;
					elif [[ ! $line == *":"* ]]; then
						if [[ $FIND_FLAG == "Y" ]]; then
							MAIN_CLASS="$MAIN_CLASS$line"
						fi
					else
						if [[ $FIND_FLAG == "Y" ]]; then
							break ;
						fi
					fi
				done < "$MF_DIR"/MANIFEST.MF

				if [ ! -z "$MAIN_CLASS" ]; then
					JARFILE_NAME=${file##*/};
					echo $JARFILE_NAME > $JARFILE_NAME_FILE;
					break ;
				fi
			fi

		done

		rm -rf $MF_DIR
	fi

	if [ -z "$JARFILE_NAME" ] ; then
		echo "====> startup error. can't found any startup jar ! please check whether the startup jar is included in ./lib! "
		exit 0;
	fi
	echo "====> find $JARFILE_NAME main jar to startup!";
}

start()
{
        if [ $RUNNING == "Y" ]; then
                echo "====> Application already started"
        else
		init
		if [ ! -d ./logs  ]; then
			mkdir logs
		fi
		if [ ! -f ./logs/catalina.log  ]; then
			touch ./logs/catalina.log
		fi
		nohup java $JAVA_OPT -Xbootclasspath/a:./conf -jar ./lib/$JARFILE_NAME > /dev/null 2>&1 &
		echo $! > $PID_FILE
		echo "====> Application $JARFILE_NAME starting..."
		if [ -f ./logs/catalina.log ]; then
			tail -f ./logs/catalina.log 
		else
			sleep 5
			if [ -f ./logs/catalina.log ]; then
				tail -f ./logs/catalina.log 
			fi
		fi
        fi
}

stop()
{
        if [ $RUNNING == "Y" ]; then
                kill -9 $PID
                rm -f $PID_FILE
                echo "====> Application stopped"
        else
                rm -f $PID_FILE
                echo "====> Application not running"
        fi
	RUNNING=N
}

restart()
{
        stop
        start
}

######### DOING #########

case "$1" in

        'start')
                start
                ;;

        'stop')
                stop
                ;;

        'restart')
                restart
                ;;

        *)
                echo "====> Usage: $0 {  start | stop | restart  }"
                exit 1
                ;;
esac
exit 0
