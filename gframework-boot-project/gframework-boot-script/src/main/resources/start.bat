@echo off & setlocal enabledelayedexpansion

rem java opt
SET JAVA_OPT=-XX:+UseG1GC -Xmx1024m -Xms1024m 
SET MAIN_CLASS=

rem 避免在其他地方运行导致路径错误
for %%i in (%0%) do (
	cd %%~dpi
)
cd ..
SET PWD=%cd%

cd ..
set "temp2=%cd%"
if "%temp2%"=="%PWD%" (set fileName=%cd:~,1%) else call set fileName=%%PWD:%temp2%\=%%
set JAR_NAME=.\lib\%fileName%.jar

cd %PWD%

if not exist .\logs (
	mkdir logs
)
if not exist .\logs\catalina.log (
	echo=>.\logs\catalina.log
)

java %JAVA_OPT% -Xbootclasspath/a:./conf -jar %JAR_NAME%

:end