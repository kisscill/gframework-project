## gframework 工具类/公共类库

本库集成了常用的第三方工具类库，并提供了大量可直接使用的，提升开发效率的工具类。同时提供了一些标识性注解以减少代码认知复杂度。

### 集成第三方库

1. org.springframework.boot:spring-boot-starter-json
2. org.springframework:spring-core
3. org.slf4j:slf4j-api
4. org.apache.commons:commons-lang3
5. commons-io:commons-io
6. commons-codec:commons-codec

但是，如果你要使用`AppContext`springboot上下文操作工具类，你需要引入springboot相关开发包才可以使用。

### 标识性注解

```java
// 用于标识一个类或一个属性，仅在初始化阶段有效，之后将会被销毁回收。通常用于占用内存较大的对象。
@GcAfterUsed

// 用于标识一个类/方法等操作是会进行逻辑删除处理的，要么删除操作是逻辑删除，要么查询操作会过滤逻辑删除的数据。
@LogicalDelete

// 用于标识一个类、方法的操作是线程安全的，通常用于类上
@ThreadSafe
```

### 中国常用类型代码枚举类

```
// 民族枚举类
public enum NationEnum {}

// 省份枚举类
public enum ProvinceEnum {}
```

### 工具类

> 在文档完善前，其他工具类用法可参考com.gframework.util包目录下的javadoc信息