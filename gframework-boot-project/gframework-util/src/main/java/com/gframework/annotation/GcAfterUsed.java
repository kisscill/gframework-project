package com.gframework.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 使用此注解来表示一个类或属性在被使用完后进行回收.
 * <p>例如属性使用完后设置为null以避免过高的内存占用，类则可以使卸载或bean的销毁等。
 * <p>此注解仅作为标识说明存在，无实际功能逻辑。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Documented
@Retention(RetentionPolicy.CLASS)
@Target(value={FIELD, TYPE})
public @interface GcAfterUsed {
}
