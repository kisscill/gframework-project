package com.gframework.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 使用此注解来表示一个类、方法、属性、对象的操作是支持逻辑删除的.
 * <p>既删除操作会进行逻辑删除，查询操作会进行根据逻辑删除字段进行排除。
 * 
 * <p>目前此注解并没有一个官方的程序对其进行一些逻辑操作，但是保留了Runtime属性以便于后期的扩展和预留操作。
 * <p>你也可以基于此注解的语意特性进行一些扩展处理。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value={FIELD, METHOD, PARAMETER, TYPE})
public @interface LogicalDelete {
	/**
	 * true表示执行逻辑删除拦截，false表示不拦截.
	 * <p>通常在一个标记了LogicalDelete注解的方法中想要调用另一个不进行逻辑删除拦截操作的方法时，可以使用此注解并设置false以阻止拦截处理。
	 * <p>默认true
	 * TODO 目前仅作为预留功能，无实际作用
	 */
	boolean value() default true;
}
