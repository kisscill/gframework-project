package com.gframework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 线程安全标识.
 * 此标识可以标记某一个类、属性、方法是线程安全的。
 * <p>
 * 需要注意的是，这个注解只存在于Class中，因此只能作为开发截断的一种说明使用。
 * </p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Documented
@Retention(RetentionPolicy.CLASS)
@Target(value = { ElementType.FIELD, ElementType.METHOD, ElementType.TYPE })
public @interface ThreadSafe {

}
