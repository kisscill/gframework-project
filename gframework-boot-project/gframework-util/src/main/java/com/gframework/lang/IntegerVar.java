package com.gframework.lang;

/**
 * 可变的int包装类.
 * <p>
 * 为了弥补Integer不可变的特性，有时候有需要进行Integer的大量计算或自增自减操作，那么就可以使用此类。
 * <p>
 * 本类不是线程安全的
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class IntegerVar extends Number implements Comparable<IntegerVar> {

	private static final long serialVersionUID = 4297739071052538676L;
	/**
	 * 保存的int值
	 */
	private int value;

	public IntegerVar(int value) {
		this.value = value;
	}

	public IntegerVar(Number value) {
		if (value == null) throw new NumberFormatException("value is null.");
		this.value = value.intValue();
	}
	
	/**
	 * 自增1并返回新的值
	 * @return 返回新的值
	 */
	public final int add() {
		return ++ this.value;
	}
	/**
	 * 加法运算
	 * @param i 要增加的值 
	 * @return 返回新的值
	 */
	public final int add(int i) {
		return this.value += i ;
	}
	
	/**
	 * 减去1并返回新的值
	 * @return 返回新的值
	 */
	public final int sub() {
		return -- this.value;
	}
	/**
	 * 减法运算
	 * @param i 要减去的值 
	 * @return 返回新的值
	 */
	public final int sub(int i) {
		return this.value -= i ;
	}
	
	/**
	 * 乘法运算
	 * @param i 要乘的值 
	 * @return 返回新的值
	 */
	public final int mul(int i) {
		return this.value *= i ;
	}
	
	/**
	 * 除法运算
	 * @param i 要除的值 
	 * @return 返回新的值
	 */
	public final int div(int i) {
		return this.value /= i ;
	}
	
	/**
	 * 求模运算
	 * @param i 要求模的值 
	 * @return 返回新的值
	 */
	public final int mod(int i) {
		return this.value %= i ;
	}

	/**
	 * 根据整数取得一个IntegerVar类对象
	 * 
	 * @param value 整数
	 * @return 返回本类对象
	 */
	public static IntegerVar valueOf(int value) {
		return new IntegerVar(value);
	}
	
	/**
	 * 根据整数字符串对象取得一个IntegerVar类对象
	 * 
	 * @param value 整数字符串
	 * @return 返回本类对象
	 * @throws NumberFormatException 如果value为null或不是一个数字
	 */
	public static IntegerVar valueOf(String value) {
		return new IntegerVar(Integer.valueOf(value));
	}

	/**
	 * 根据Number对象取得一个IntegerVar类对象
	 * 
	 * @param value Number类对象
	 * @return 返回本类对象
	 * @throws NumberFormatException 如果value为null
	 */
	public static IntegerVar valueOf(Number value) {
		return new IntegerVar(value);
	}

	@Override
	public int compareTo(IntegerVar anotherInteger) {
		return Integer.compare(this.value, anotherInteger.value);
	}

	@Override
	public int intValue() {
		return this.value;
	}

	@Override
	public long longValue() {
		return (long) this.value;
	}

	@Override
	public float floatValue() {
		return (float) this.value;
	}

	@Override
	public double doubleValue() {
		return (double) this.value;
	}

	@Override
	public int hashCode() {
		return Integer.hashCode(this.value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntegerVar) {
			return value == ((IntegerVar) obj).value;
		}
		return false;
	}

}
