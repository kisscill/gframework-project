package com.gframework.lang;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 民族枚举类.
 * <p>
 * 此枚举类的民族code是大多数人的通用代码设置的，包含其他和外国血统。
 * <p>国家和外国血统的定义部分地方表现的不一致，使用时请注意。
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
public enum NationEnum {
	/** 汉族 */
	HAN("01", "汉族"),
	/** 蒙古族 */
	MONGOL("02", "蒙古族"),
	/** 回族 */
	HUI("03", "回族"),
	/** 藏族 */
	TIBETAN("04", "藏族"),
	/** 维吾尔族 */
	UIGHUR("05", "维吾尔族"),
	/** 苗族 */
	MIAO("06", "苗族"),
	/** 彝族 */
	YI("07", "彝族"),
	/** 壮族 */
	BAI_ZHUANG("08", "壮族"),
	/** 布依族 */
	BUYI("09", "布依族"),
	/** 朝鲜族 */
	KOREAN("10", "朝鲜族"),
	/** 满族 */
	MANCHU("11", "满族"),
	/** 侗族 */
	DONG("12", "侗族"),
	/** 瑶族 */
	YAO("13", "瑶族"),
	/** 白族 */
	BAI("14", "白族"),
	/** 土家族 */
	TUJIA("15", "土家族"),
	/** 哈尼族 */
	HANI("16", "哈尼族"),
	/** 哈萨克族 */
	KAZAKH("17", "哈萨克族"),
	/** 傣族 */
	DAI("18", "傣族"),
	/** 黎族 */
	LI("19", "黎族"),
	/** 僳僳族 */
	LISU("20", "僳僳族"),
	/** 佤族 */
	WA("21", "佤族"),
	/** 畲族 */
	SHE("22", "畲族"),
	/** 高山族 */
	GAOSHAN("23", "高山族"),
	/** 拉祜族 */
	LAHU("24", "拉祜族"),
	/** 水族 */
	SHUI("25", "水族"),
	/** 东乡族 */
	DONGXIANG("26", "东乡族"),
	/** 纳西族 */
	NAXI("27", "纳西族"),
	/** 景颇族 */
	JINGPO("28", "景颇族"),
	/** 柯尔克孜族 */
	KIRGHIZ("29", "柯尔克孜族"),
	/** 土族 */
	DU("30", "土族"),
	/** 达斡尔族 */
	DAUR("31", "达斡尔族"),
	/** 仫佬族 */
	MULAM("32", "仫佬族"),
	/** 羌族 */
	QIANG("33", "羌族"),
	/** 布朗族 */
	BLANG("34", "布朗族"),
	/** 撒拉族 */
	SALAR("35", "撒拉族"),
	/** 毛南族 */
	MAONAN("36", "毛南族"),
	/** 仡佬族 */
	GELAO("37", "仡佬族"),
	/** 锡伯族 */
	XIBE("38", "锡伯族"),
	/** 阿昌族 */
	ACHANG("39", "阿昌族"),
	/** 普米族 */
	PUMI("40", "普米族"),
	/** 塔吉克族 */
	TAJIK("41", "塔吉克族"),
	/** 怒族 */
	NU("42", "怒族"),
	/** 乌孜别克族 */
	UZBEK("43", "乌孜别克族"),
	/** 俄罗斯族 */
	RUSSIAN("44", "俄罗斯族"),
	/** 鄂温克族 */
	EVENKI("45", "鄂温克族"),
	/** 德昂族 */
	DEANG("46", "德昂族"),
	/** 保安族 */
	BONAN("47", "保安族"),
	/** 裕固族 */
	YUGUR("48", "裕固族"),
	/** 京族 */
	GIN("49", "京族"),
	/** 塔塔尔族 */
	TATAR("50", "塔塔尔族"),
	/** 独龙族 */
	DRUNG("51", "独龙族"),
	/** 鄂伦春族 */
	OROQIN("52", "鄂伦春族"),
	/** 赫哲族 */
	HEZHEN("53", "赫哲族"),
	/** 门巴族 */
	MENBA("54", "门巴族"),
	/** 珞巴族 */
	LHOBA("55", "珞巴族"),
	/** 基诺族 */
	JINO("56", "基诺族"),
	/** 其他 */
	QITA("57", "其他"),
	/** 外国血统 */
	WAIGUO("58", "外国血统"),

	;
	/**
	 * 民族code
	 */
	private final String nationCode;
	/**
	 * 民族名称
	 */
	private final String nationName;
	/**
	 * map快速查询缓存.
	 * <p>不可改
	 */
	private static final Map<String,String> NATION_MAP ;
	
	static {
		Map<String,String> map = new HashMap<>(NationEnum.values().length);
		for (NationEnum e : NationEnum.values()) {
			map.put(e.nationCode, e.nationName);
		}
		NATION_MAP = Collections.unmodifiableMap(map);
	}

	private NationEnum(String nationCode, String nationName) {
		this.nationCode = nationCode;
		this.nationName = nationName;
	}
	
	/**
	 * 根据一个民族code获取民族名称
	 * @param nationCode 民族code
	 * @return 返回民族名称
	 */
	public static String getNationName(String nationCode){
		return NATION_MAP.get(nationCode);
	}
	
	/**
	 * 获取所有的民族信息集合
	 * @return 返回一个Map集合，key是code，value是名称
	 */
	public static Map<String,String> getAllNation(){
		return NATION_MAP ;
	}
	
	/**
	 * 获取民族code
	 * @return 民族code
	 */
	public String getCode(){
		return this.nationCode;
	}
	
	/**
	 * 获取民族名称
	 * @return 民族名称
	 */
	public String getName(){
		return this.nationName;
	}
}
