package com.gframework.lang;

/**
 * 用于存储一个<strong>任意类型对象</strong>范围的类.
 * <p>
 * 本类的对象不能存在null值
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class ObjectRange<T> {

	private final T start;
	private final T end;

	/**
	 * 创建一个范围对象，对象本身可以是任意类型，如日期，字符串，数字等。
	 *
	 * @param start 开启值.
	 * @param end 结束值.
	 */
	public ObjectRange(T start, T end) {
		if (start == null || end == null) {
			throw new IllegalArgumentException("ObjectRange范围对象的开始值和结束值不能为null");
		}
		this.start = start;
		this.end = end;
	}

	/**
	 * 返回开始值.
	 */
	public T getStart() {
		return start;
	}

	/**
	 * 返回结束值.
	 */
	public T getEnd() {
		return end;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ObjectRange<?> other = (ObjectRange<?>) obj;
		if (end == null) {
			if (other.end != null) {
				return false;
			}
		} else if (!end.equals(other.end)) {
			return false;
		}
		if (start == null) {
			if (other.start != null) {
				return false;
			}
		} else if (!start.equals(other.start)) {
			return false;
		}
		return true;
	}

}
