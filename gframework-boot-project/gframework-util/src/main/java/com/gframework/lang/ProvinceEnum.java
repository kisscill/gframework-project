package com.gframework.lang;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 省份枚举类.
 * <p>
 * 此枚举类的省份code是按照国标代码进行设置的。
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
public enum ProvinceEnum {

	/** 北京市 */
	BEIJING("11", "北京市"),
	/** 天津市 */
	TIANJING("12", "天津市"),
	/** 河北省 */
	HEBEI("13", "河北省"),
	/** 山西省 */
	SHANXI("14", "山西省"),
	/** 内蒙古自治区 */
	INNER_MONGORIA("15", "内蒙古自治区"),
	/** 辽宁省 */
	LIAONING("21", "辽宁省"),
	/** 吉林省 */
	JILIN("22", "吉林省"),
	/** 黑龙江省 */
	HEILONGJIANG("23", "黑龙江省"),
	/** 上海市 */
	SHANGHAI("31", "上海市"),
	/** 江苏省 */
	JIANGSU("32", "江苏省"),
	/** 安徽省 */
	ANHUI("34", "安徽省"),
	/** 浙江省 */
	ZHEJIANG("33", "浙江省"),
	/** 福建省 */
	FUJIAN("35", "福建省"),
	/** 江西省 */
	JIANGXI("36", "江西省"),
	/** 山东省 */
	SHANDONG("37", "山东省"),
	/** 河南省 */
	HENAN("41", "河南省"),
	/** 湖北省 */
	HUBEI("42", "湖北省"),
	/** 湖南省 */
	HUNAN("43", "湖南省"),
	/** 广东省 */
	GUANGDONG("44", "广东省"),
	/** 广西壮族自治区  */
	GUANGXI("45", "广西壮族自治区"),
	/** 海南省 */
	HAINAN("46", "海南省"),
	/** 重庆市 */
	CHONGQING("50", "重庆市"),
	/** 四川省 */
	SICHUAN("51", "四川省"),
	/** 贵州省 */
	GUIZHOU("52", "贵州省"),
	/** 云南省 */
	YUNNAN("53", "云南省"),
	/** 西藏自治区 */
	TIBET("54", "西藏自治区"),
	/** 陕西省 */
	SHAANXI("61", "陕西省"),
	/** 甘肃省 */
	GANSU("62", "甘肃省"),
	/** 青海省 */
	QINGHAI("63", "青海省"),
	/** 宁夏回族自治区 */
	NINGXIA("64", "宁夏回族自治区"),
	/** 新疆维吾尔自治区 */
	XINJIANG("65", "新疆维吾尔自治区"),
	/** 台湾省 */
	TAIWAN("71", "台湾省"),
	/** 香港特别行政区 */
	HONG_KONG("81", "香港特别行政区"),
	/** 澳门特别行政区 */
	MACAO("82", "澳门特别行政区"),
	;
	/**
	 * 省份code
	 */
	private final String provinceCode;
	/**
	 * 省份名称
	 */
	private final String provinceName;
	/**
	 * map快速查询缓存.
	 * <p>不可改
	 */
	private static final Map<String,String> PROVINCE_MAP ;
	
	static {
		Map<String,String> map = new HashMap<>(ProvinceEnum.values().length);
		for (ProvinceEnum e : ProvinceEnum.values()) {
			map.put(e.provinceCode, e.provinceName);
		}
		PROVINCE_MAP = Collections.unmodifiableMap(map);
	}

	private ProvinceEnum(String provinceCode, String provinceName) {
		this.provinceCode = provinceCode;
		this.provinceName = provinceName;
	}
	
	/**
	 * 根据一个省份code获取省份名称
	 * @param provinceCode 省份code
	 * @return 返回省份名称
	 */
	public static String getProvinceName(String provinceCode){
		return PROVINCE_MAP.get(provinceCode);
	}
	
	/**
	 * 获取所有的省份信息集合
	 * @return 返回一个Map集合，key是code，value是名称
	 */
	public static Map<String,String> getAllProvince(){
		return PROVINCE_MAP ;
	}
	
	/**
	 * 获取省份code
	 * @return 省份code
	 */
	public String getCode(){
		return this.provinceCode;
	}
	
	/**
	 * 获取省份名称
	 * @return 省份名称
	 */
	public String getName(){
		return this.provinceName;
	}
}
