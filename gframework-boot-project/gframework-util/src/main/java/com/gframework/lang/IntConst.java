package com.gframework.lang;

/**
 * int包装类常量
 * @author Ghwolf
 */
public interface IntConst {
	/**
	 * int=0的包装类对象
	 */
	public static final Integer ZERO = Integer.valueOf(0);
	/**
	 * int=1的包装类对象
	 */
	public static final Integer ONE = Integer.valueOf(1);
	/**
	 * int=2的包装类对象
	 */
	public static final Integer TWO = Integer.valueOf(2);
}
