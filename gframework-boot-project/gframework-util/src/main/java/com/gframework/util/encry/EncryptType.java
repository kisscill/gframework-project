package com.gframework.util.encry;

/**
 * 加密算法枚举类.
 * <p>主要是列举基于EnctypeUtils的对称加密算法
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see EncryptUtils
 */
public enum EncryptType {
	/**
	 * des算法
	 */
	DES(EncryptionMode.ECB,FillMode.pkcs5padding,EncryptType.DES_BLOCK_56,EncryptType.DES_ZERO_PADDING),
	/**
	 * aes算法
	 */
	AES(EncryptionMode.ECB,FillMode.pkcs5padding,EncryptType.AES_BLOCK_128,EncryptType.AES_ZERO_PADDING),
	;
	/**
	 * AES加密算法，密钥长度——128位
	 */
	public static final int AES_BLOCK_128 = 128 ;
	/**
	 * AES加密算法，密钥长度——192位
	 */
	public static final int AES_BLOCK_192 = 192 ;
	/**
	 * AES加密算法，密钥长度——256位
	 */
	public static final int AES_BLOCK_256 = 256 ;
	/**
	 * DES加密算法，密钥长度——56位
	 */
	public static final int DES_BLOCK_56 = 56 ;
	
	
	/**
	 * AES算法偏移位16位0填充字符串 
	 */
	public static final String AES_ZERO_PADDING = "0000000000000000";
	/**
	 * DES算法偏移位8位0填充字符串 
	 */
	public static final String DES_ZERO_PADDING = "00000000";


	private final EncryptionMode defaultEncryptionMode; 
	private final FillMode defaultFillMode; 
	private final int defaultBlockSize; 
	private final String defaultOffset; 
	
	private EncryptType(EncryptionMode defaultEncryptionMode,FillMode defaultFillMode,int defaultBlockSize,String defaultOffset){
		this.defaultEncryptionMode = defaultEncryptionMode;
		this.defaultFillMode = defaultFillMode;
		this.defaultBlockSize = defaultBlockSize;
		this.defaultOffset = defaultOffset;
	}
	
	/**
	 * 获取当前加密算法默认的加密模式
	 * @return EncryptionMode枚举对象
	 */
	public EncryptionMode getDefaultEncryptionMode() {
		return this.defaultEncryptionMode;
	}
	/**
	 * 获取当前加密算法默认的填充方式
	 * @return FillMode枚举对象
	 */
	public FillMode getDefaultFillMode() {
		return this.defaultFillMode;
	}
	/**
	 * 获取当前加密算法默认的数据块大小
	 * @return 返回默认数据块大小
	 */
	public int getDefaultBlockSize() {
		return this.defaultBlockSize;
	}

	/**
	 * 获取当前加密算法默认的偏移位字符串
	 * @return 0填充的偏移位字符串
	 */
	public String getDefaultOffset() {
		return this.defaultOffset;
	}
	
	
}
