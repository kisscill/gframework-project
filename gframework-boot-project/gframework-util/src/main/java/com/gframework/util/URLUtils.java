package com.gframework.util;

import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

/**
 * url相关操作工具类
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public final class URLUtils {
	private URLUtils(){}
	
	/**
	 * 获取一个url上的一个指定参数
	 * 
	 * @param url 要获取的url，可以为空
	 * @param paramName 要获取的参数名称，可以为空
	 * @return 返回值一定不是空，没有则返回空字符串
	 */
	public static String getUrlParam(@Nullable String url,@Nullable String paramName) {
		if (StringUtils.isEmpty(url) || StringUtils.isEmpty(paramName)) {
			return "";
		}
		int i = url.indexOf('?');
		if (i == -1) return "";
		
		return getQueryStringParam0(url.substring(i + 1), paramName);
	}
	
	/**
	 * 获取一个url的queryString上的一个指定参数
	 * 
	 * @param queryString 要获取的url的queryString，可以为空
	 * @param paramName 要获取的参数名称，可以为空
	 * @return 返回值一定不是空，没有则返回空字符串
	 */
	public static String getQueryStringParam(@Nullable String queryString,@Nullable String paramName) {
		if (StringUtils.isEmpty(queryString) || StringUtils.isEmpty(paramName)) {
			return "";
		}
		return getQueryStringParam0(queryString,paramName);
	}
	private static String getQueryStringParam0(String queryString,String paramName) {
		String keyPrefix = paramName + "=";
		for (String pm : queryString.split("&")) {
			if (pm.startsWith(keyPrefix)) {
				return pm.substring(keyPrefix.length());
			}
		}
		return "";
	}
	
	
	/**
	 * 拼接url，并自动处理结尾和开头的"/"的兼容性问题
	 * 
	 * @param urls 凭借的url
	 * @return 返回拼接后的url，返回值一定不是空，至少也是""
	 */
	public static String splicingUrl(@Nullable String ... urls) {
		if (urls == null || urls.length == 0) {
			return "";
		}
		if (urls.length == 1) {
			return urls[0];
		}
		String url = splicingUrl(urls[0],urls[1]);
		for (int x = 2 ; x < urls.length ; x ++) {
			url = splicingUrl(url,urls[x]);
		}
		return url ;
	}
	/**
	 * 拼接url1和url2，并自动处理结尾的"/"，既url1可以以/结尾也可以不以"/"结尾。
	 * 并且url2可以以"/"开头也可以不以"/"开头。
	 * @param url1 前面的url
	 * @param url2 后面的url
	 * @return 返回拼接后的url，如果url1和url2都是null，返回值一定不是空，至少也是""
	 */
	public static String splicingUrl(@Nullable String url1,@Nullable String url2) {
		if (StringUtils.isEmpty(url1)) {
			if (StringUtils.isEmpty(url2)) {
				return "" ;
			} else {
				return url2 ;
			}
		} else {
			if (StringUtils.isEmpty(url2)) {
				return url1 ;
			} else {
				int url1End = -1;
				int url2Start = -1;
				for (int x = url1.length() -1 ; x >= 0 ; x --) {
					if (url1.charAt(x) == '/') {
						url1End = x ;
					} else {
						break ;
					}
				}
				for (int x = 0 ; x < url2.length() ; x ++) {
					if (url2.charAt(x) == '/') {
						url2Start = x ;
					} else {
						break ;
					}
				}
				if (url1End == -1) {
					if (url2Start == -1) {
						return url1 + "/" + url2 ;
					} else if (url2Start == 0) {
						return url1 + url2 ;
					} else {
						return url1 + substring(url2,url2Start);
					}
				} else if (url1End == url1.length() - 1) {
					if (url2Start == -1) {
						return url1 + url2 ;
					} else {
						return url1 + substring(url2,url2Start + 1);
					}
				} else {
					if (url2Start == -1) {
						return substring(url1,0,url1End + 1) + url2;
					} else {
						return substring(url1,0,url1End + 1) + substring(url2,url2Start + 1);
					}
				}
			}
		}
	}
	
	private static String substring(String str,int start){
		if (start < str.length()) {
			return str.substring(start);
		} else {
			return "";
		}
	}
	private static String substring(String str,int start,int end){
		if (start < str.length()) {
			return str.substring(start,end);
		} else {
			return "";
		}
	}
	

}
