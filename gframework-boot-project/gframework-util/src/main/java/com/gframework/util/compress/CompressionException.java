
package com.gframework.util.compress;


/**
 * 压缩异常
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@SuppressWarnings("serial")
public class CompressionException extends RuntimeException {
	  /**
     * Creates a new instance.
     */
    public CompressionException() {
    }

    /**
     * Creates a new instance.
     */
    public CompressionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new instance.
     */
    public CompressionException(String message) {
        super(message);
    }

    /**
     * Creates a new instance.
     */
    public CompressionException(Throwable cause) {
        super(cause);
    }
}
