package com.gframework.util.encry;

import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

/**
 * 加密模式.
 * <p>用来描述加密算法（此处特指分组密码，不包括流密码，）在加密时对明文分组的模式，它代表了不同的分组方式
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see FillMode
 */
public enum EncryptionMode {
	/**
	 * 电子密码本模式，通常作为默认模式，<strong>并且此模式不需要进行偏移量设置</strong>
	 */
	ECB,
	/**
	 * 密码分组连接模式
	 */
	CBC,
	/**
	 * 计数器模式，<strong>此模式必须使用NoPadding填充模式</strong>
	 */
	CTR,
	/**
	 * 输出反馈模式
	 */
	OFB,
	/**
	 * 密文反馈模式
	 */
	CFB,
	;
	
	/**
	 * 取得默认的加密模式
	 */
	public static EncryptionMode getDefault() {
		return ECB ;
	}
	
	/**
	 * 根据模式名称获取模式，不区分大小写
	 * @param name 模式名称，不区分大小写，如果为空则返回null
	 * @return 如果存在则返回对应模式，否则返回null
	 */
	@Nullable
	public static EncryptionMode getByCode(@Nullable String name) {
		if (StringUtils.isEmpty(name)) {
			return null ;
		}
		name = name.toUpperCase();
		for (EncryptionMode e : EncryptionMode.values()) {
			if (e.name().equals(name)) {
				return e;
			}
		}
		return null ;
	}
}
