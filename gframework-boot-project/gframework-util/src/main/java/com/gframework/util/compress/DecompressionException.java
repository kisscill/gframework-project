package com.gframework.util.compress;


/**
 * 解压缩异常
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class DecompressionException extends RuntimeException {

    /**
     * Creates a new instance.
     */
    public DecompressionException() {
    }

    /**
     * Creates a new instance.
     */
    public DecompressionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new instance.
     */
    public DecompressionException(String message) {
        super(message);
    }

    /**
     * Creates a new instance.
     */
    public DecompressionException(Throwable cause) {
        super(cause);
    }
}