package com.gframework.util.sql.mysql;

import org.springframework.lang.Nullable;

/**
 * 基于mysql类型的数据库一些辅助操作工具类.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public final class MySqlUtils {
	private MySqlUtils(){}
	
	/**
	 * 对模糊查询条件参数中的特殊字符做转义.
	 * <p>将 "%" 和 "_" 转换为"\%"和"\_"形式。
	 * @param param 要进行转换的参数，如果为null，则返回null
	 * @return 返回转换后的字符串，如果为null，则返回""
	 */
	public static String escapeFuzzyQuery(@Nullable String param) {
		if (param == null) return "" ;
		StringBuilder sb = new StringBuilder(param.length() + 16);
		final int length = param.length();
		for (int x = 0 ; x < length ; x ++) {
			char c = param.charAt(x);
			if (c == '%' || c == '_') {
				sb.append('\\');
			}
			sb.append(c);
		}
		return sb.toString();
	}

}
