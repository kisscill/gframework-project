/**
 * 各种加密解密算法请使用apache common库中的{@link org.apache.commons.codec.digest.DigestUtils}类.
 * <p>byte与16进制字符串转换其使用{@link org.apache.commons.codec.binary.Hex}类
 * 
 * @author Ghwolf
 *
 */
package com.gframework.util.encry;