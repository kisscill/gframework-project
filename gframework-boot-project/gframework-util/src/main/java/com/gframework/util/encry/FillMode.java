package com.gframework.util.encry;

import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

/**
 * 填充方式.
 * <p>填充方式是在分组密码中，当明文长度不是分组长度的整数倍时，需要在最后一个分组中填充一些数据使其凑满一个分组的长度。
 * <p>XXX 目前还有大量模式经测试发现不支持，暂不明确是名称未写正确还是其他原因导致。
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see EncryptionMode
 */
public enum FillMode {
	pkcs5padding,
	/**
	 * java不支持zeropadding模式，只有NoPadding不填充模式.
	 * <p>原本zeropadding采用的就是0填充方式。所以此框架对此模式做了一个兼容处理，会手动填充0
	 */
	zeropadding,
	/**
	 * 不填充模式
	 */
	NoPadding,
	iso10126Padding,
	;
	/**
	 * 取得默认的填充方式
	 */
	public static FillMode getDefault() {
		return pkcs5padding ;
	}
	
	/**
	 * 根据填充方式名称获取填充方式，不区分大小写
	 * @param name 填充方式名称，不区分大小写，如果为空则返回null
	 * @return 如果存在则返回对应填充方式，否则返回null
	 */
	@Nullable
	public static FillMode getByCode(@Nullable String name) {
		if (StringUtils.isEmpty(name)) {
			return null ;
		}
		name = name.toUpperCase();
		for (FillMode e : FillMode.values()) {
			if (e.name().equals(name)) {
				return e;
			}
		}
		return null ;
	}
}
