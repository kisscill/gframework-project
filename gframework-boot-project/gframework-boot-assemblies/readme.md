## gframework项目打包预设assembly配置

如果你集成了`gframework-boot-starter`父pom，那么会默认采用assembly插件进行打包。
通常你无需进行任何配置，框架已经内置了一套assembly.xml配置。但你也可以自定义自己的结构化打包配置。

#### 有assembly.xml示例

```xml
<assembly xmlns="http://maven.apache.org/ASSEMBLY/2.1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/ASSEMBLY/2.1.0 http://maven.apache.org/xsd/assembly-2.1.0.xsd">
    <id>to-tar-assembly</id>
    <formats>
        <format>tar.gz</format>
    </formats>
    <includeBaseDirectory>true</includeBaseDirectory>
    <fileSets>
        <fileSet>
            <directory>src/main/prod-conf</directory>
            <outputDirectory>conf</outputDirectory>
        </fileSet>
    </fileSets>
    <dependencySets>
        <dependencySet>
            <outputDirectory>/lib</outputDirectory>
            <excludes>
            	<exclude>com.gframework.boot:gframework-boot-script:*</exclude>
            </excludes>
        </dependencySet>
        <dependencySet>
            <outputDirectory>/bin</outputDirectory>
            <fileMode>0755</fileMode>
            <includes>
            	<include>com.gframework.boot:gframework-boot-script:*</include>
            </includes>
            <unpack>true</unpack>
            <unpackOptions>
				<includes>
            		<include>*.sh</include>
            	</includes>
            	<lineEnding>unix</lineEnding>
            	<encoding>UTF-8</encoding>
            </unpackOptions>
        </dependencySet>
        <dependencySet>
            <outputDirectory>/bin</outputDirectory>
            <fileMode>0755</fileMode>
            <includes>
            	<include>com.gframework.boot:gframework-boot-script:*</include>
            </includes>
            <unpack>true</unpack>
            <unpackOptions>
				<includes>
            		<include>*.bat</include>
            	</includes>
            	<lineEnding>crlf</lineEnding>
            	<encoding>UTF-8</encoding>
            </unpackOptions>
        </dependencySet>
    </dependencySets>
</assembly>
```
