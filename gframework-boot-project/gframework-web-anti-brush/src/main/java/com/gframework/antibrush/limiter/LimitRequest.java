package com.gframework.antibrush.limiter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 限流注解.
 * <p>用于controller，可以对一个方法或整个类进行限流操作，在指定时间内最多出现指定个请求。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
public @interface LimitRequest {
	/**
	 * 在指定时间内最多多少个请求，默认1000
	 */
	int maxRequest() default 1000;
	
	/**
	 * 多长时间内（单位由{@link #timeUnit()}决定）默认60
	 */
	int time() default 60 ;
	
	/**
	 * 时间单位，默认秒
	 */
	TimeUnit timeUnit() default TimeUnit.SECONDS ;
	
	/**
	 * 限流模式，默认是方法级限流模式
	 */
	Class<? extends CurrentLimitingMode> limitingMode() default MethodCurrentLimitingMode.class;
	
}
