package com.gframework.antibrush.exec;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.HttpStatus;

/**
 * 默认的接口请求到上限被拒绝的返回结果处理类.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@ConditionalOnMissingBean(LimiterNoPassHandler.class)
public class DefaultLimiterNoPassHandler implements LimiterNoPassHandler{
	
	private static final Logger logger = LoggerFactory.getLogger(DefaultLimiterNoPassHandler.class);

	@Override
	public Object returnValue(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.sendError(HttpStatus.TOO_MANY_REQUESTS.value(), HttpStatus.TOO_MANY_REQUESTS.getReasonPhrase());
		} catch (IOException e) {
			logger.error("【默认接口防刷返回结果处理器】接口请求次数过多，拒绝请求返回信息是出现异常！",e);
		}
		return null ;
	}

}
