package com.gframework.antibrush.limiter;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

/**
 * 限流模式.
 * <p>限流模式应用与{@link LimitRequest}注解，可以指定某个方法的限流模式。你也可以自己实现接口子类
 * 去自定义限流模式。唯一需要注意的是，此接口子类是一次性的，每次执行都会重新new一个新的对象，不会重复利用。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see LimitRequest
 */
public interface CurrentLimitingMode {
	/**
	 * 获取一个唯一标识符，这个标识符用来确定当前操作关联的限流桶.
	 * <p>例如，如果是基于ip限流，那么这个key就要能够对不同的ip访问不同的方法都进行区分。
	 * @param anno 限流注解对象，(不是null)
	 * @param method 当前执行的方法对象，(不是null)
	 * @param request httpServletRequest 请求对象，(不是null)
	 * @return 返回唯一标识符key，不能为null
	 */
	public String getKey(LimitRequest anno,Method method,HttpServletRequest request);

}
