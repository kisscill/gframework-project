package com.gframework.antibrush.exec;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.lang.Nullable;

/**
 * 限流器禁止通行后的处理器.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface LimiterNoPassHandler{
	/**
	 * 当前因限流原因禁止访问时的返回结果获取方法.
	 * <p>返回结果有两种方式：
	 * <ul>
	 * <li>第一种是直接将返回结果return，
	 * 但是这样可能未必会奏效，例如在{@link LimitRequestAOP}中返回了一个和方法返回值类型不一样的对象，那么就会出现错误。</li>
	 * <li>第二种是将结果直接使用response进行输出</li>
	 * </ul>
	 * 
	 * @param request Http请求对象
	 * @param response Http响应对象
	 * @return 返回给客户端的内容，可以为null，可以不用返回值，直接使用response返回。 
	 */
	@Nullable
	public Object returnValue(HttpServletRequest request,HttpServletResponse response) ;

}
