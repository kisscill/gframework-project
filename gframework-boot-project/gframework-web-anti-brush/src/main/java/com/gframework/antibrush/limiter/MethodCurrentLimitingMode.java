package com.gframework.antibrush.limiter;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

/**
 * 基于方法的限流模式
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class MethodCurrentLimitingMode implements CurrentLimitingMode{

	@Override
	public String getKey(LimitRequest anno, Method method, HttpServletRequest request) {
		return method.getDeclaringClass().getName() + ":" + method.getName();
	}
	
}
