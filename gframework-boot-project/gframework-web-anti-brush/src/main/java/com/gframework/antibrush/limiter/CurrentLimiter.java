package com.gframework.antibrush.limiter;

/**
 * 限流器操作接口
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@FunctionalInterface
public interface CurrentLimiter {
	/**
	 * 请求通行——速率算发
	 * @param anno 限流注解对象
	 * @param key 当前操作标识符，参考{@link CurrentLimitingMode}
	 * @return 如果允许通行返回true，否则返回false
	 */
	public boolean rate(LimitRequest anno,String key) ;

}
