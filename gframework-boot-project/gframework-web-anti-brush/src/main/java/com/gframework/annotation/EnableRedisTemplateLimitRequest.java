package com.gframework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.gframework.antibrush.exec.LimitRequestAOP;
import com.gframework.antibrush.limiter.CurrentLimiterRedisTemplate;


/**
 * 启用基于RedisTemplate操作的limitRequet 接口防刷器
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({LimitRequestAOP.class,CurrentLimiterRedisTemplate.class})
public @interface EnableRedisTemplateLimitRequest {

}
