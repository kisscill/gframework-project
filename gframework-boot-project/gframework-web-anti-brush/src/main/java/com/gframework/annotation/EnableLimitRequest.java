package com.gframework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.gframework.antibrush.exec.LimitRequestAOP;
import com.gframework.antibrush.limiter.CurrentLimiterLocal;


/**
 * 启用基于内存操作的基本limitRequet 接口防刷器.
 * <p>
 * 这不适用于分布式的情况，如果是分布式接口防刷，可以采用 {@link EnableRedissonLimitRequest} 或
 * {@link EnableRedisTemplateLimitRequest}
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see EnableRedissonLimitRequest
 * @see EnableRedisTemplateLimitRequest
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({LimitRequestAOP.class,CurrentLimiterLocal.class})
public @interface EnableLimitRequest {

}
