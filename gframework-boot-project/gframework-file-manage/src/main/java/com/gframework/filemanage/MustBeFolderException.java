package com.gframework.filemanage;

/**
 * 当需要一个目录信息，但却是一个文件时，抛出此异常
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class MustBeFolderException extends RuntimeException {

	public MustBeFolderException(String message, Throwable cause) {
		super(message, cause);
	}

	public MustBeFolderException(String message) {
		super(message);
	}

}
