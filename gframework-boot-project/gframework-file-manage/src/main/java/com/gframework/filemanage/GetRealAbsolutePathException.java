package com.gframework.filemanage;

/**
 * 获取文件真实绝对路径失败后，抛出此异常，通常是因为路径格式不正确。
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class GetRealAbsolutePathException extends RuntimeException {

	public GetRealAbsolutePathException(String message, Throwable cause) {
		super(message, cause);
	}

	public GetRealAbsolutePathException(String message) {
		super(message);
	}

}
