package com.gframework.filemanage;

/**
 * 文件不存在时，抛出此异常
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class FileNotFoundRuntimeException extends RuntimeException {

	public FileNotFoundRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileNotFoundRuntimeException(String message) {
		super(message);
	}

}
