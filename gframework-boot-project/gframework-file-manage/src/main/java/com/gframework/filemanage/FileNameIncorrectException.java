package com.gframework.filemanage;

/**
 * 文件名称不正确时，抛出此异常
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class FileNameIncorrectException extends RuntimeException {

	public FileNameIncorrectException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileNameIncorrectException(String message) {
		super(message);
	}

}
