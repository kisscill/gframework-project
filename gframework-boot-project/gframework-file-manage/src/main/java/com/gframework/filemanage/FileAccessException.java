package com.gframework.filemanage;

/**
 * 文件无权限操作时，抛出此异常
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class FileAccessException extends RuntimeException {

	public FileAccessException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileAccessException(String message) {
		super(message);
	}

}
