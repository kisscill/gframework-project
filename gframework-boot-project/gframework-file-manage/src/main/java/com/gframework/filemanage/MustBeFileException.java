package com.gframework.filemanage;

/**
 * 当需要一个文件信息，但却是一个文件夹时，抛出此异常
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class MustBeFileException extends RuntimeException {

	public MustBeFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public MustBeFileException(String message) {
		super(message);
	}

}
