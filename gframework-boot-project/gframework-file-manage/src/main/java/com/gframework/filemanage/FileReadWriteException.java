package com.gframework.filemanage;

/**
 * 文件/文件夹读写失败时，抛出此异常
 * 
 * @since 2.1.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class FileReadWriteException extends RuntimeException {

	public FileReadWriteException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileReadWriteException(String message) {
		super(message);
	}

}
