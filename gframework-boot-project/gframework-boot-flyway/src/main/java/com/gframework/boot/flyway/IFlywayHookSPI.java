package com.gframework.boot.flyway;

import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

/**
 * flyway执行钩子程序spi接口，采用spring SPI(spring.factories)接入方式.
 * <p>接入此SPI的接口子类将会在扩展flyway操作执行前后被调用。
 * <p>重要的是，你可以得到{@link Environment}和{@link FlywayProperties}对象，其中后者可以得到数据库连接信息。
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
public interface IFlywayHookSPI {
	
	/**
	 * 在flyway类创建以及配置信息设置前拦截.
	 * <p>此时对pro进行修改是会生效的，例如数据库连接信息等。
	 * 
	 * @param pro flyway配置信息，包括数据库连接信息
	 * @param env spring环境配置信息
	 */
	default void beforeConfig(FlywayProperties pro,ConfigurableEnvironment env) {}
	/**
	 * 在flyway的migrate指令执行前操作.
	 * 
	 * @param pro flyway配置信息，包括数据库连接信息
	 * @param env spring环境配置信息
	 */
	default void beforeMigrate(FlywayProperties pro,ConfigurableEnvironment env) {}
	/**
	 * 在flyway的migrate指令执行后操作.
	 * 
	 * @param pro flyway配置信息，包括数据库连接信息
	 * @param env spring环境配置信息
	 */
	default void afterMigrate(FlywayProperties pro,ConfigurableEnvironment env) {}
}
