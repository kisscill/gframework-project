package com.gframework.annotation.flyway;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

/**
 * 开启flyway扩展操作，同时关闭flyway默认的装配操作.
 * <p>仅在【启用此注解】 并且 【spring.flyway.enabled=true】的情况下才会执行相关操作。
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(ExtendDefaultFlyway.class)
public @interface EnableFlywayExtend {

}
