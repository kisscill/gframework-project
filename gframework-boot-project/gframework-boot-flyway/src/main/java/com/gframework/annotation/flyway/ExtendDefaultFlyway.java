package com.gframework.annotation.flyway;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;

/**
 * 本类的功能仅仅时去掉默认的flyway装配类，
 * 因为{@link EnableAutoConfiguration}用在注解上是无效的。
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@EnableAutoConfiguration(exclude = FlywayAutoConfiguration.class)
class ExtendDefaultFlyway {
}
