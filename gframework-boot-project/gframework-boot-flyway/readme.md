## springboot flyway 扩展包

此扩展包是为了解决flyway执行过晚导致部分依赖数据库的代码逻辑无法在第一次正常运行的问题。
**所以此扩展包仅仅是提前了flyway的执行时间，但是对flyway本身的逻辑和配置几乎不做修改**。

**例如：** 你需要在系统初始化的时候从数据库加载部分信息，而这部分逻辑又要优先于`PropertySourcesPlaceholderConfigurer`执行。
那么，你完全可以使用此扩展包解决此问题。  

此扩展包的特点是将flyway的执行顺序提前到springboot处于`environmentPrepared`状态的时候执行，所以此扩展包依赖springboot。同时也是专门为springboot量身定做的。
因为扩展包内的扩展机制均采用spring的SPI方式实现的。

### pom引入

```xml
<dependency>
	<groupId>com.gframework.boot</groupId>
	<artifactId>gframework-boot-flyway</artifactId>
	<version>2.0.1-beta</version>
</dependency>
```

此扩展包包会自动依赖springboot开发包以及flyway开发包。
同时如果仅仅做依赖，几乎不会对您的系统产生任何影响。  

### 配置

**你还需要做以下配置才能生效：**

**1、在任意启动类上增加`@EnableFlywayExtend`注解**
```java
@EnableFlywayExtend
public class SpringBootStart {}
```
**2、启用flyway**
```
# application.yaml
spring:
  flyway:
    enabled: true

# application.properties
spring.flyway.enabled=true
```


当你加上`@EnableFlywayExtend`注解后，会发生以下几个事情：

1. `FlywayAutoConfiguration`自动装配类将会被排除，既默认的flyway操作将不会执行
2. 在springboot启动阶段到达`environmentPrepared`时，会执行flyway（此时控制台还没有输出springboot的logo）
3. spring.factories文件中的`IFlywayExtendSPI`和`IFlywayHookSPI`接口SPI类将会被加载使用。

### 执行过程

flyway的执行会通过加载`spring.flyway.*`的配置，生成`FlywayProperties`类对象，最后生成`Flyway`类对象并执行`migrate`方法。

但是对于url、username、password、locations四个主要参数的读取，提供了较为灵活的配置和扩展方式。
1. url、username、password三个配置参数会首先读取spring.flyway.\*的数据库配置，如果没有，则会读取spring.datasource.\*的数据库配置
2. 通过SPI方式来设置url、username、password将会**永久**改变flyway执行的数据库配置。
3. 通过SPI方式也可以扩展locations配置。


### SPI

gframework-boot-flyway提供了两个SPI接口，通过spring.factories配置文件进行扩展。

#### flyway执行配置拦截器 `IFlywayExtendSPI`

这是一个可以用于修改flyway执行配置的拦截器，他会在flyway配置对象`FlywayProperties`初始化完毕之后执行。
你可以修改url、username、password，扩展locations，甚至可以修改其中的任意参数都是可以的。

通过`@Order`注解可以控制拦截器的执行顺序

#### flyway执行拦截器 `IFlywayHookSPI`

这是一个flyway执行的拦截器，它会在三个阶段执行：

* **beforeConfig**：在flyway对象创建前执行，可以修改flyway配置
* **beforeMigrate**：执行flyway的migrate命令前执行。此时修改配置将不会生效
* **afterMigrate**：执行flyway的migrate命令后执行。此时修改配置将不会生效


### 开发阶段执行flyway

此扩展包还提供了另一个功能，可以帮助你在开发阶段，不用启动springboot、也不需要配置flyway插件或maven插件，就可以按照springboot的运行方式去执行flyway。

**使用方法：**
```java
public class Demo {
	public static void main(String args[]) {
		FlywayDevRunner.migrate();
	}
}
```
