package com.gframework.mybatis.util.generator.core.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * mybatis的xml中生成XmlElement拦截器.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public interface XmlElementInterceptor {
	/**
	 * 创建XmlElement拦截.
	 * 
	 * @param table 当前操作表
	 * @param element 当前生成的Element
	 * @param configuration 全局配置对象
	 * @return 返回处理完毕的XmlELement，如果返回null则表示不生成
	 */
	public XmlElement createXmlElement(MetaData table, XmlElement element, Configuration configuration) ;
	
	/**
	 * 创建xml root根节点对象时拦截.
	 * 
	 * @param table 当前操作表
	 * @param root 当前生成的Element
	 * @param configuration 全局配置对象
	 * @return 返回处理完毕的XmlELement，如果返回null则表示不生成
	 */
	public XmlElement createRootElement(MetaData table,XmlElement root,Configuration configuration) ;
}
