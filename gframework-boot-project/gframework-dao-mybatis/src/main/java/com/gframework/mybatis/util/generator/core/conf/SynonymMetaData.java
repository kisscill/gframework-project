package com.gframework.mybatis.util.generator.core.conf;

/**
 * 本类表示为一个同义词类型的元对象.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
class SynonymMetaData extends TableMetaData {

	@Override
	public String getTableType() {
		return "SYNONYM";
	}

	@Override
	public boolean isReadonly() {
		return false;
	}

}
