package com.gframework.mybatis.util.generator.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Interface;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;
import com.gframework.mybatis.util.generator.core.interceptor.JavaDaoInterceptor;

/**
 * dao生成拦截器.
 * <p>
 * 去掉多余的方法，加上适当的接口
 * </p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class DaoInterceptor implements JavaDaoInterceptor {

	@Override
	public Interface createInterface(MetaData table, Interface inter, Configuration conf) {
		if ("VIEW".equalsIgnoreCase(table.getTableType())) {
			inter.addImportedType(new FullyQualifiedJavaType("com.gframework.mybatis.dao.IViewDAO"));
			inter.addImportedType(new FullyQualifiedJavaType(conf.getBasePackage() + "." + table.getModuleName()
					+ ".entity.pojo." + table.getFormatTableName()));
			FullyQualifiedJavaType viewDao = new FullyQualifiedJavaType("IViewDAO");
			viewDao.addTypeArgument(new FullyQualifiedJavaType(table.getFormatTableName()));
			inter.addSuperInterface(viewDao);
		} else if ("TABLE".equalsIgnoreCase(table.getTableType()) || "SYNONYM".equalsIgnoreCase(table.getTableType())) {
			inter.addImportedType(new FullyQualifiedJavaType("com.gframework.mybatis.dao.ITableBasicDAO"));
			inter.addImportedType(new FullyQualifiedJavaType(conf.getBasePackage() + "." + table.getModuleName()
			+ ".entity.pojo." + table.getFormatTableName()));
			
			FullyQualifiedJavaType tableDao = new FullyQualifiedJavaType("ITableBasicDAO");
			FullyQualifiedJavaType entityType = new FullyQualifiedJavaType(table.getFormatTableName());
			tableDao.addTypeArgument(entityType);
			inter.addSuperInterface(tableDao);
			
			Column pkCol = table.getPrimaryKey();
			if (pkCol != null) {
				inter.addImportedType(new FullyQualifiedJavaType("com.gframework.mybatis.dao.IPrimaryDAO"));
				FullyQualifiedJavaType pkDao = new FullyQualifiedJavaType("IPrimaryDAO");
				pkDao.addTypeArgument(new FullyQualifiedJavaType(pkCol.getJavaType().getSimpleName()));
				pkDao.addTypeArgument(entityType);
				
				String pacName = pkCol.getJavaType().getPackage().getName();
				if (!pacName.equals("java.lang")) {
					inter.addImportedType(new FullyQualifiedJavaType(pkCol.getJavaType().getName()));
				}
				
				inter.addSuperInterface(pkDao);
			}
			
		} else {
		}

		return inter;
	}

	@Override
	public Method createMethod(MetaData table, Interface inter, Method method, Configuration configuration) {
		return null;
	}

}
