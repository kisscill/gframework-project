package com.gframework.mybatis.util.generator.core.conf;

import java.util.ArrayList;
import java.util.List;

import com.gframework.mybatis.util.generator.core.util.JavaBeansUtil;

/**
 * 数据表中列信息封装类.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class Column {

	/**
	 * 列所属catalog（如果数据库不支持则为null）
	 */
	private String catalog;
	/**
	 * 列所属schema（如果数据库不支持则为null）
	 */
	private String schema;
	/**
	 * 取得列所属表名称
	 */
	private String tableName;
	/**
	 * 取得列名称
	 */
	private String columnName;
	/**
	 * 取得转换为驼峰命名法的列名称
	 */
	private String formatColumnName ;
	/**
	 * 取得列在数据库中的类型名称
	 */
	private String dataTypeName;
	/**
	 * 取得列类型对应的java类型
	 */
	private Class<?> javaType;
	/**
	 * 字段长度
	 */
	private int columnSize;
	/**
	 * 数字型小数位精度（所占长度）
	 */
	private int decimalDigits;
	/**
	 * 是否允许为null
	 */
	private boolean nullable;
	/**
	 * 列注释
	 */
	private String columnComment;
	/**
	 * 是否自动增长列
	 */
	private boolean autoincrement;
	/**
	 * 是否主键
	 */
	private boolean primaryKey;
	/**
	 * 关联此列的所有外键信息
	 */
	private List<String> foreignRelevance = new ArrayList<>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
		result = prime * result + ((javaType == null) ? 0 : javaType.hashCode());
		return result;
	}

	void addForeignRelevance(String keyFullName) {
		this.foreignRelevance.add(keyFullName);
	}
	
	public String getCatalog() {
		return this.catalog;
	}

	public String getSchema() {
		return this.schema;
	}

	public String getTableName() {
		return this.tableName;
	}

	public String getColumnName() {
		return this.columnName;
	}
	
	public String getFormatColumnName() {
		return this.formatColumnName;
	}

	public String getDataTypeName() {
		return this.dataTypeName;
	}

	public Class<?> getJavaType() {
		return this.javaType;
	}

	public int getColumnSize() {
		return this.columnSize;
	}

	public int getDecimalDigits() {
		return this.decimalDigits;
	}

	public boolean isNullable() {
		return this.nullable;
	}

	public String getColumnComment() {
		return this.columnComment;
	}

	public boolean isAutoincrement() {
		return this.autoincrement;
	}

	public boolean isPrimaryKey() {
		return this.primaryKey;
	}

	public List<String> getForeignRelevance() {
		return this.foreignRelevance;
	}

	void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	void setSchema(String schema) {
		this.schema = schema;
	}

	void setTableName(String tableName) {
		this.tableName = tableName;
	}

	void setColumnName(String columnName) {
		this.columnName = columnName;
		this.formatColumnName = JavaBeansUtil.getCamelCaseString(columnName, false);
	}

	void setDataTypeName(String dataTypeName) {
		this.dataTypeName = dataTypeName;
	}

	void setJavaType(Class<?> javaType) {
		this.javaType = javaType;
	}

	void setColumnSize(int columnSize) {
		this.columnSize = columnSize;
	}

	void setDecimalDigits(int decimalDigits) {
		this.decimalDigits = decimalDigits;
	}

	void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}

	void setAutoincrement(boolean autoincrement) {
		this.autoincrement = autoincrement;
	}

	void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	void setForeignRelevance(List<String> foreignRelevance) {
		this.foreignRelevance = foreignRelevance;
	}

}
