package com.gframework.mybatis.dao.mybatis.provider;

import org.springframework.core.NestedRuntimeException;

/**
 * POJO类解析异常
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class PojoAnalysisException extends NestedRuntimeException {

	public PojoAnalysisException(String msg) {
		super(msg);
	}

	public PojoAnalysisException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
