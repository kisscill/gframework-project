package com.gframework.mybatis.dao.datasource;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Connection包装类，可以将一个数据库连接对象进行包装，从而扩充自己的方法.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class ConnectionWrapper implements Connection {

	/**
	 * 被包装的Connection接口对象
	 */
	private Connection conn;

	public ConnectionWrapper(Connection conn) {
		this.conn = conn;
	}

	/**
	 * 调用此方法可以取得主要的数据库连接对象.
	 * 本类所有默认复写的方法都是通过此方法来取得数据库连接对象的，你可以复写此方法来修改数据库连接获取逻辑，从而达到更灵活的功能开发。
	 * 
	 * @return 返回数据库连接对象
	 */
	public Connection getConn() {
		return this.conn;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return this.getConn().unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return this.getConn().isWrapperFor(iface);
	}

	@Override
	public Statement createStatement() throws SQLException {
		return this.getConn().createStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return this.getConn().prepareStatement(sql);
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		return this.getConn().prepareCall(sql);
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
		return this.getConn().nativeSQL(sql);
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		this.getConn().setAutoCommit(autoCommit);
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		return this.getConn().getAutoCommit();
	}

	@Override
	public void commit() throws SQLException {
		this.getConn().commit();
	}

	@Override
	public void rollback() throws SQLException {
		this.getConn().rollback();
	}

	@Override
	public void close() throws SQLException {
		this.getConn().close();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return this.getConn().isClosed();
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		return this.getConn().getMetaData();
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		this.getConn().setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return this.getConn().isReadOnly();
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		this.getConn().setCatalog(catalog);
	}

	@Override
	public String getCatalog() throws SQLException {
		return this.getConn().getCatalog();
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		this.getConn().setTransactionIsolation(level);
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		return this.getConn().getTransactionIsolation();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return this.getConn().getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		this.getConn().clearWarnings();
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		return this.getConn().createStatement(resultSetType, resultSetConcurrency);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
			throws SQLException {
		return this.getConn().prepareStatement(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		return this.getConn().prepareCall(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return this.getConn().getTypeMap();
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		this.getConn().setTypeMap(map);
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		this.getConn().setHoldability(holdability);
	}

	@Override
	public int getHoldability() throws SQLException {
		return this.getConn().getHoldability();
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		return this.getConn().setSavepoint();
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		return this.getConn().setSavepoint(name);
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		this.getConn().rollback(savepoint);
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		this.getConn().releaseSavepoint(savepoint);
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		return this.getConn().createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return this.getConn().prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return this.getConn().prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		return this.getConn().prepareStatement(sql, autoGeneratedKeys);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		return this.getConn().prepareStatement(sql, columnIndexes);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		return this.getConn().prepareStatement(sql, columnNames);
	}

	@Override
	public Clob createClob() throws SQLException {
		return this.getConn().createClob();
	}

	@Override
	public Blob createBlob() throws SQLException {
		return this.getConn().createBlob();
	}

	@Override
	public NClob createNClob() throws SQLException {
		return this.getConn().createNClob();
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		return this.getConn().createSQLXML();
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
		return this.getConn().isValid(timeout);
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
		this.getConn().setClientInfo(name, value);
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
		this.getConn().setClientInfo(properties);
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
		return this.getConn().getClientInfo(name);
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		return this.getConn().getClientInfo();
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
		return this.getConn().createArrayOf(typeName, elements);
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
		return this.getConn().createStruct(typeName, attributes);
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		this.getConn().setSchema(schema);
	}

	@Override
	public String getSchema() throws SQLException {
		return this.getConn().getSchema();
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		this.getConn().abort(executor);
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		this.setNetworkTimeout(executor, milliseconds);
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		return this.getConn().getNetworkTimeout();
	}

}
