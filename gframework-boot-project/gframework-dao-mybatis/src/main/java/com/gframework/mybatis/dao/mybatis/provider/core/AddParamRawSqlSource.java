package com.gframework.mybatis.dao.mybatis.provider.core;

import java.util.Map;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.scripting.defaults.RawSqlSource;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.session.Configuration;
import org.springframework.lang.Nullable;

/**
 * 可以自定义附加参数的 RawSqlSource 扩展操作类
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class AddParamRawSqlSource extends RawSqlSource {

	/**
	 * 附加参数
	 */
	private Map<String, Object> additionalParameters;

	public AddParamRawSqlSource(Configuration configuration, SqlNode rootSqlNode, Class<?> parameterType,
			@Nullable Map<String, Object> additionalParameters) {
		super(configuration, rootSqlNode, parameterType);
		this.additionalParameters = additionalParameters;
	}

	public AddParamRawSqlSource(Configuration configuration, String sql, Class<?> parameterType,
			@Nullable Map<String, Object> additionalParameters) {
		super(configuration, sql, parameterType);
		this.additionalParameters = additionalParameters;
	}

	@Override
	public BoundSql getBoundSql(Object parameterObject) {
		BoundSql sql = super.getBoundSql(parameterObject);
		if (additionalParameters != null) {
			for (Map.Entry<String, Object> entry : additionalParameters.entrySet()) {
				sql.setAdditionalParameter(entry.getKey(), entry.getValue());
			}
		}
		return sql;
	}

}
