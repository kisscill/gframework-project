package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.NeedPrimaryKey;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Parameter;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;
import com.gframework.mybatis.util.generator.core.exception.NoPrimaryKeyException;

/**
 * 根据主键查询多条数据.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class FindByIds implements SqlOperatorGenerator,NeedCanUpdate,NeedPrimaryKey {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		Column pk = table.getPrimaryKey();
		if (pk == null) {
			throw new NoPrimaryKeyException(table.getTableName() + " 表没有主键！");
		}
		XmlElement xml = new XmlElement("select");
		xml.addAttribute(new Attribute("id", "findByIds"));
		xml.addAttribute(new Attribute("parameterType", "collection"));
		xml.addAttribute(new Attribute("resultMap", "basicResultMap"));
		xml.addAttribute(new Attribute("useCache", "true"));
		
		xml.addComment("根据主键查询多条数据");

		xml.addElement(new TextElement("SELECT "));
		int foot = 0 ;
		int maxFoot = table.getColumns().size() - 1;
		for (Column col : table.getColumns()) {
			if (foot ++ == maxFoot) {
				xml.addElement(new TextElement("\t" + col.getColumnName()));
			} else {
				xml.addElement(new TextElement("\t" + col.getColumnName() + ","));
			}
		}
		xml.addElement(new TextElement("FROM "));
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		
		XmlElement where = new XmlElement("where");
		where.addElement(new TextElement(pk.getColumnName() + " IN "));
		
		XmlElement foreach = new XmlElement("foreach");
		foreach.addAttribute(new Attribute("collection","collection"));
		foreach.addAttribute(new Attribute("item","_item"));
		foreach.addAttribute(new Attribute("open","("));
		foreach.addAttribute(new Attribute("close",")"));
		foreach.addAttribute(new Attribute("separator",","));
		foreach.addElement(new TextElement("#{_item}"));
		where.addElement(foreach);
		
		xml.addElement(where);
		
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("findByIds");
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		FullyQualifiedJavaType retType = new FullyQualifiedJavaType("java.util.List");
		retType.addTypeArgument(new FullyQualifiedJavaType(pojoName));
		
		method.setReturnType(retType);
		method.setVisibility(JavaVisibility.PUBLIC);
		
		FullyQualifiedJavaType pType = new FullyQualifiedJavaType("java.util.Collection");
		pType.addTypeArgument(new FullyQualifiedJavaType(table.getPrimaryKey().getJavaType().getName()));
		method.addParameter(new Parameter(pType,"ids"));
		// XXX 后期有时间了添加注释
		return method;
	}

}
