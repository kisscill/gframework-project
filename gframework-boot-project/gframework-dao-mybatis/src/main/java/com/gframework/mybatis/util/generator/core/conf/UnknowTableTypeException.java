package com.gframework.mybatis.util.generator.core.conf;

import com.gframework.mybatis.util.generator.core.exception.GeneratorException;

/**
 * 当出现一个未知的TableType时，抛出此异常.<br>
 * 此异常通常可以通过版本的升级来解决，如果你所使用的数据库是新的，但是对应的相关代码并未升级，那么可能会出现此错误。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class UnknowTableTypeException extends GeneratorException {

	private static final long serialVersionUID = 3211666647968471462L;

	public UnknowTableTypeException() {
		super(EVERY_EXCEPTION_PREFIX + "未知的table类型。");
	}

	public UnknowTableTypeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(EVERY_EXCEPTION_PREFIX + message, cause, enableSuppression, writableStackTrace);
	}

	public UnknowTableTypeException(String message, Throwable cause) {
		super(EVERY_EXCEPTION_PREFIX + message, cause);
	}

	public UnknowTableTypeException(String message) {
		super(EVERY_EXCEPTION_PREFIX + message);
	}

	public UnknowTableTypeException(Throwable cause) {
		super(EVERY_EXCEPTION_PREFIX + "未知的table类型。", cause);
	}

}
