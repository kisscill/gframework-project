package com.gframework.mybatis.util.generator.core.conf;

/**
 * 本类表示为一个视图类型的元对象.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
class ViewMetaData extends TableMetaData {

	@Override
	public String getTableType() {
		return "VIEW";
	}

	@Override
	public boolean isReadonly() {
		return true;
	}

}
