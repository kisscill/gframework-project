package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.NeedPrimaryKey;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Parameter;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 根据主键更新非null属性字段.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class UpdateById implements SqlOperatorGenerator,NeedCanUpdate,NeedPrimaryKey {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("update");
		xml.addAttribute(new Attribute("id", "updateById"));
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		xml.addAttribute(new Attribute("parameterType", pojoName));
		xml.addComment("根据主键更新非null属性字段");

		xml.addElement(new TextElement("UPDATE"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);

		XmlElement set = new XmlElement("set");
		
		for (Column col : table.getColumns()) {
			if (col.isPrimaryKey()) {
				continue ;
			}
			XmlElement ife1 = new XmlElement("if");
			Attribute test = new Attribute("test",col.getFormatColumnName() + " != null");
			ife1.addAttribute(test);
			ife1.addElement(new TextElement(col.getColumnName() + " = #{" + col.getFormatColumnName() + "},"));
			set.addElement(ife1);
		}
		
		xml.addElement(set);
		
		Column pk = table.getPrimaryKey();
		XmlElement where = new XmlElement("where");
		where.addElement(new TextElement(pk.getColumnName() + " = #{" + pk.getFormatColumnName() + "}"));
		xml.addElement(where);

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("updateById");
		method.setReturnType(FullyQualifiedJavaType.getIntInstance());
		method.setVisibility(JavaVisibility.PUBLIC);
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		method.addParameter(new Parameter(new FullyQualifiedJavaType(pojoName),"pojo"));
		// XXX 后期有时间了添加注释
		return method;
	}

}
