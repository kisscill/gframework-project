package com.gframework.mybatis.util.generator.core.codegen;

import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * sql操作生成接口.<br>
 * <p>无论是扩充还是本地默认的生成操作类，都必须实现此接口，每个此接口子类都代表一种生成方法。对应着有dao方法的生成和xml方法的生成。</p>
 * <p>此接口子类必须提供无参构造方法！</p>
 * <p>可以与本接口一起使用的其他接口有：{@link NeedCanUpdate},{@link NeedPrimaryKey}</p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see NeedCanUpdate
 * @see NeedPrimaryKey
 */
public interface SqlOperatorGenerator {

	/**
	 * 生成一个mybatis的sql操作方法.
	 * 
	 * @param table 当前操作的表
	 * @param config 全局配置信息
	 * @return 返回操作方法，如果为null则表示不生成
	 */
	public XmlElement createSqlXml(MetaData table, Configuration config);

	/**
	 * 生成一个mybatis的sql操作方法对应的DAO接口方法.
	 * 
	 * @param table 当前操作的表
	 * @param config 全局配置信息
	 * @return 返回DAO方法，如果为null则表示不生成
	 */
	public Method createMethod(MetaData table, Configuration config);
}
