package com.gframework.mybatis.util.generator.core.codegen;

/**
 * 此接口表示当前操作需要表有能写才能使用，像是视图是只读的.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface NeedCanUpdate {
}
