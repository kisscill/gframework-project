package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Parameter;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 保存单条数据（不包含null字段，null属性会按照默认值进行添加）(自动增长列除外).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class Save implements SqlOperatorGenerator,NeedCanUpdate {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("insert");
		xml.addAttribute(new Attribute("id", "save"));
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		xml.addAttribute(new Attribute("parameterType", pojoName));
		Column pk = table.getPrimaryKey();
		if (pk != null) {
			xml.addAttribute(new Attribute("useGeneratedKeys","true"));
			xml.addAttribute(new Attribute("keyProperty",pk.getFormatColumnName()));
		}
		xml.addComment("保存一条数据，只会添加非null属性内容(自动增长列除外)");

		xml.addElement(new TextElement("INSERT INTO"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		xml.addElement(new TextElement("("));
		
		XmlElement trimFields = new XmlElement("trim");
		XmlElement trimValues = new XmlElement("trim");
		Attribute suff = new Attribute("suffixOverrides", ",");
		trimFields.addAttribute(suff);
		trimValues.addAttribute(suff);
		for (Column col : table.getColumns()) {
			if (col.isAutoincrement()) {
				continue;
			}
			XmlElement ife1 = new XmlElement("if");
			XmlElement ife2 = new XmlElement("if");
			Attribute test = new Attribute("test",col.getFormatColumnName() + " != null");
			ife1.addAttribute(test);
			ife2.addAttribute(test);
			ife1.addElement(new TextElement(col.getColumnName() + ","));
			ife2.addElement(new TextElement("#{" + col.getFormatColumnName() + "},"));
			trimFields.addElement(ife1);
			trimValues.addElement(ife2);
		}
		xml.addElement(trimFields);
		xml.addElement(new TextElement(") VALUES ("));
		xml.addElement(trimValues);
		xml.addElement(new TextElement(")"));

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("save");
		method.setReturnType(FullyQualifiedJavaType.getIntInstance());
		method.setVisibility(JavaVisibility.PUBLIC);
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		method.addParameter(new Parameter(new FullyQualifiedJavaType(pojoName),"pojo"));
		// XXX 后期有时间了添加注释
		return method;
	}

}
