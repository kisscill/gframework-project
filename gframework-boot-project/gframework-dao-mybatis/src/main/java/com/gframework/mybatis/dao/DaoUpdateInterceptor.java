package com.gframework.mybatis.dao;

/**
 * dao更新方法拦截器.
 * <p>实现此接口子类bean，以实现对默认提供的dao更新方法进行拦截处理。
 * <p>通常此接口用于对对象的公共属性值进行统一赋值处理操作。
 * @since 2.0.0
 * @author Ghwolf
 */
public interface DaoUpdateInterceptor {
	
	/**
	 * 拦截操作，可以和获取到准备进行修改的对象，然后进行额外处理操作.
	 * <p>仅当{@link #canInterceptUpdate(Class)}返回true时才会执行此方法。
	 * @param pojoType 当前准备进行修改的pojo类型
	 * @param obj 准备进行修改的对象，一定不是null
	 */
	public void interceptUpdate(Class<?> pojoType,Object obj);

	/**
	 * 判断一个要进行修改的pojo类型是否是支持拦截处理的类型.
	 * <p>如果是，则应当返回true，此时才会去调用intercept方法，并且对应的参数对象类型就是pojoType类型
	 * @param pojoType 当前准备进行修改的pojo类型
	 * @return 如果支持拦截处理，则返回true，否则返回false
	 */
	public boolean canInterceptUpdate(Class<?> pojoType) ;
	

}
