package com.gframework.mybatis.service;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.lang.Nullable;

import com.gframework.mybatis.dao.ITableBasicDAO;
import com.gframework.mybatis.service.impl.ServiceImpl;
import com.gframework.mybatis.service.impl.TableServiceImpl;
import com.gframework.mybatis.service.impl.ViewServiceImpl;
import com.gframework.sqlparam.BeanSqlParam;
import com.gframework.sqlparam.SqlParam;

/**
 * 基于本套mybatis框架的service通用操作父接口.
 * <p>此接口定义了dao类型必须是{@link ITableBasicDAO}接口子类/子接口。
 * 
 * @since 2.0.0
 * @author Ghwolf
 *
 * @see IViewService
 * @see IService
 * @see TableServiceImpl
 * @see ViewServiceImpl
 * @see ServiceImpl
 *
 * @param <M> 当前操作对应的dao接口类型
 * @param <T> 实体类类型
 */
public interface ITableService<M extends ITableBasicDAO<T>,T extends Serializable> extends IViewService<M, T>{
	
	/**
	 * 保存一条新的数据，并返回主键.
	 * 
	 * <p>如果保存出现错误或失败，则会抛出相应的异常。
	 * 
	 * @param pojo 要保存的POJO实体类
	 * @return 返回主键，通常来说不会是null，除非出现数据库未指定主键且代码中未设置主键的情况。
	 * @throws IllegalArgumentException 如果pojo对象为null
	 */
	@Nullable
	public Serializable save(T pojo) ;
	
	/**
	 * 保存多条新的数据，并返回主键.
	 * 
	 * <p>如果保存出现错误或失败，则会抛出相应的异常。
	 * 
	 * @param pojos 存储了要保存的pojo类集合对象
	 * @return 返回数据更新条数，既保存的新数据个数
	 * @throws IllegalArgumentException 如果pojos对象为null
	 */
	public int saveBatch(Collection<T> pojos) ;
	
	/**
	 * 保存多条新的数据，并返回主键.
	 * <p>此方法支持设置一个最大保存限度，来进行分批保存，以避免一次存储的sql过长
	 * 
	 * <p>如果保存出现错误或失败，则会抛出相应的异常。
	 * 
	 * @param pojos 存储了要保存的pojo类集合对象
	 * @param maximum 单次保存最大保存的数据个数，>=1
	 * @return 返回数据更新条数，既保存的新数据个数
	 * @throws IllegalArgumentException 如果pojos对象为null或maximum小于1
	 */
	public int saveBatch(Collection<T> pojos,int maximum) ;
	
	/**
	 * 根据自定义条件更新数据
	 * @param beanSqlParam 要更新的数据和查询条件封装类，如果为null则不进行更新
	 * @return 返回更新的数据行数，如果没有数据被更新，则返回0
	 */
	public int update(@Nullable BeanSqlParam<T> beanSqlParam) ;
	
	/**
	 * 根据自定义条件删除数据
	 * @param sqlParam 条件封装类，如果为null则不进行查询
	 * @return 返回删除的数据行数，如果没有数据被删除，则返回0
	 */
	public int delete(@Nullable SqlParam sqlParam) ;
	
}
