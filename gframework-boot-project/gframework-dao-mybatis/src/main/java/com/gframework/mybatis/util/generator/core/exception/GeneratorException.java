package com.gframework.mybatis.util.generator.core.exception;

/**
 * 自动生成模块所有异常的公共父异常。所有异常均为运行时异常.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class GeneratorException extends RuntimeException {

	private static final long serialVersionUID = -5901449230917011882L;
	private static final String EXCEPTION_MESSAGE_PREFIX = "【自动生成框架异常】" ;
	protected static final String EVERY_EXCEPTION_PREFIX = " -> 子异常：" ;
	public GeneratorException() {
		super(EXCEPTION_MESSAGE_PREFIX);
	}

	public GeneratorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(EXCEPTION_MESSAGE_PREFIX + message, cause, enableSuppression, writableStackTrace);
	}

	public GeneratorException(String message, Throwable cause) {
		super(EXCEPTION_MESSAGE_PREFIX + message, cause);
	}

	public GeneratorException(String message) {
		super(EXCEPTION_MESSAGE_PREFIX + message);
	}

	public GeneratorException(Throwable cause) {
		super(EXCEPTION_MESSAGE_PREFIX,cause);
	}

}
