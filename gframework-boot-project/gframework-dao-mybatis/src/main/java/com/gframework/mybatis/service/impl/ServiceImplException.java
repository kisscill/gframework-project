package com.gframework.mybatis.service.impl;

/**
 * ServiceImpl类执行异常
 * @since 2.0.0
 * @author Ghwolf
 *
 */
@SuppressWarnings("serial")
public class ServiceImplException extends RuntimeException {

	public ServiceImplException() {
		super();
	}

	public ServiceImplException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ServiceImplException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceImplException(String message) {
		super(message);
	}

	public ServiceImplException(Throwable cause) {
		super(cause);
	}

}
