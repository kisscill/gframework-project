package com.gframework.mybatis.entity.pojo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 当一个列【被当作某个表的外键参考列】关联的时候，可以使用此注解注释.<br>
 * 此注解无实际用处，作为预留功能，可以仅作为注释信息，也可以在任何你想用到的地方取使用。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
public @interface ForeignWith {
	/**
	 * 关联他的外键信息.<br>
	 * 可以有多个，格式为：[catalog].[shcema].table.column
	 */
	String[] fkcolumn();
}
