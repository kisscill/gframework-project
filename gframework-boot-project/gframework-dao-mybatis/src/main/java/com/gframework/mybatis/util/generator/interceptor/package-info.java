/**
 * 所有Generator模块的扩展拦截器都在此包中.
 * 
 * @author Ghwolf
 *
 */
package com.gframework.mybatis.util.generator.interceptor;