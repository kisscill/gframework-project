package com.gframework.mybatis.dao.mybatis.provider.per;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.builder.annotation.ProviderContext;
import org.apache.ibatis.builder.annotation.ProviderMethodResolver;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.lang.Nullable;

import com.gframework.mybatis.dao.mybatis.provider.MybatisDaoInfo;
import com.gframework.mybatis.dao.mybatis.provider.ProviderUtils;
import com.gframework.sqlparam.SqlParam;


/**
 * sql提供器公共操作辅助父接口.
 * <p>
 * 由于mybatis没有对provider进行单例处理，因此所有方法采用static会更加高性能，
 * 因此父类或夫接口也只能通过static方法对子类的一些公共操作提出来。
 * </p>
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 * @see ProviderContext
 * @see ProviderMethodResolver
 * @see SqlParam
 * @see MybatisDaoInfo
 */
public abstract class AbstractProvider implements ProviderMethodResolver{
	
	/**
	 * 取得MybatisDaoInfo类对象.
	 */
	protected static MybatisDaoInfo getMybatisDaoInfo(ProviderContext context) {
		return ProviderUtils.getMybatisDaoInfo(context.getMapperType());
	}
	/**
	 * 设置sqlParam参数并取得map，如果没有需要生成的where语句，则返回null
	 * @param param SqlParam参数，可以为null
	 * @param sql SQL类对象
	 * @return param为null，则返回null，如果生成了sql，则返回参数集合，否则集合长度为0，如果没有要设置的参数，集合长度也是0
	 */
	protected static Map<String,Object> setSqlParam(@Nullable SqlParam param,SQL sql) {
		if (param != null) {
			Map<String,Object> map = new HashMap<>();
			String whereSql = param.getSql(map);
			if (whereSql != null) {
				sql.WHERE(whereSql);
			}
			return map ;
		} else {
			return null ;
		}
	}
	/**
	 * 设置sqlParam参数并取得map，如果没有需要生成的wh	eere语句
	 * @param param SqlParam参数，可以为null
	 * @param sql SQL类对象
	 * @param map 要设置参数的map，可以为null
	 */
	protected static void setSqlParam(@Nullable SqlParam param,SQL sql,@Nullable Map<String,Object> map) {
		if (param != null) {
			String whereSql = param.getSql(map);
			if (whereSql != null) {
				sql.WHERE(whereSql);
			}
		}
	}
	
}
