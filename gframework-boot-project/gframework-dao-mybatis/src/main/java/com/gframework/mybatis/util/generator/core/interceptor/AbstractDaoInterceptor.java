package com.gframework.mybatis.util.generator.core.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.java.Interface;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * javaDao创建拦截器(抽象类).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public abstract class AbstractDaoInterceptor implements JavaDaoInterceptor {

	/**
	 * 创建接口拦截，
	 * 当创建好一个接口的时候，进行拦截，返回值为最终生成的接口结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的接口
	 * @param inter 当前已经构造好的接口对象
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public Interface createTable(MetaData table, Interface inter, Configuration configuration) {
		return inter;
	}

	/**
	 * 方法创建拦截，
	 * 当创建好一个方法之后，进行拦截，返回值为最终生成的方法结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的表
	 * @param inter 当前已经构造好的方法对象
	 * @param method 方法
	 * @param column 当前操作的列
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public Method createMethod(MetaData table, Interface inter, Method method, Column column,
			Configuration configuration) {
		return method;
	}
}
