package com.gframework.mybatis.dao.mybatis.plugins;

import org.apache.ibatis.mapping.SqlSource;

/**
 * 封装SqlSource和查询参数的类
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class SqlSourceParamVo {

	private SqlSource sqlSource;
	private Object param;

	public SqlSourceParamVo() {
	}

	public SqlSourceParamVo(SqlSource sqlSource, Object param) {
		this.sqlSource = sqlSource;
		this.param = param;
	}

	public SqlSource getSqlSource() {
		return this.sqlSource;
	}

	public void setSqlSource(SqlSource sqlSource) {
		this.sqlSource = sqlSource;
	}

	public Object getParam() {
		return this.param;
	}

	public void setParam(Object param) {
		this.param = param;
	}

}
