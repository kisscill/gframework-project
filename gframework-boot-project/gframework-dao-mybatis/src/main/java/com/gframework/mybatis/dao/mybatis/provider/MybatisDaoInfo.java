package com.gframework.mybatis.dao.mybatis.provider;

import java.io.Serializable;

import org.springframework.lang.Nullable;


/**
 * 基于mybatis的dao操作接口信息解析存储类.
 *
 * @since 1.0.0
 * @author Ghwolf
 */
public class MybatisDaoInfo {

	/**
	 * 是否只读，用来区分一个数据库实体是否是只读的，通常来说视图是只读的，表是可读可写的。
	 */
	private boolean readOnly;
	/**
	 * 主键类型
	 */
	@Nullable
	private Class<? extends Serializable> primaryKeyType;
	/**
	 * POJO类型
	 */
	private Class<? extends Serializable> pojoType;
	/**
	 * 对应POJO实体类的解析信息
	 */
	private PojoInfo pojoInfo;

	@Nullable
	public Class<? extends Serializable> getPrimaryKeyType() {
		return this.primaryKeyType;
	}

	public Class<? extends Serializable> getPojoType() {
		return this.pojoType;
	}

	public PojoInfo getPojoInfo() {
		return this.pojoInfo;
	}

	public boolean isReadOnly() {
		return this.readOnly;
	}

	void setPrimaryKeyType(@Nullable Class<? extends Serializable> primaryKeyType) {
		this.primaryKeyType = primaryKeyType;
	}

	void setPojoType(Class<? extends Serializable> pojoType) {
		this.pojoType = pojoType;
	}

	void setPojoInfo(PojoInfo pojoInfo) {
		this.pojoInfo = pojoInfo;
	}

	void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

}
