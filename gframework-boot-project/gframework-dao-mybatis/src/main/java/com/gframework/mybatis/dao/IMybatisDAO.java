package com.gframework.mybatis.dao;

import java.io.Serializable;

/**
 * 
 * Mybatis的相关DAO操作标准公共父接口.
 * <p>本接口规定的是针对sql数据库的相关操作，并不支持其他类型操作。
 * <p>本接口的所有操作都不应该抛出非 {@link RuntimeException} 异常。所有的非RuntimeException异常必须捕捉并处理。
 * 当发生了程序预料之外的异常清空，才应当发生回滚操作。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 * @param <T> 实体类类型
 */
public interface IMybatisDAO<T extends Serializable>{
}
