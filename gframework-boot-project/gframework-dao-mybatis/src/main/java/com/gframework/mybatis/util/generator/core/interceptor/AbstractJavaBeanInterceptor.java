package com.gframework.mybatis.util.generator.core.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.java.Field;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.TopLevelClass;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * javabean创建拦截器(抽象类).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public abstract class AbstractJavaBeanInterceptor implements JavaBeanInterceptor {

	@Override
	public TopLevelClass createTable(MetaData table, TopLevelClass cls, Configuration configuration) {
		return cls;
	}

	@Override
	public Field createField(MetaData table, TopLevelClass cls, Field field, Column column,
			Configuration configuration) {
		return field;
	}

	@Override
	public Method createSetter(MetaData table, TopLevelClass cls, Method setter, Column column,
			Configuration configuration) {
		return setter;
	}

	@Override
	public Method createGetter(MetaData table, TopLevelClass cls, Method getter, Column column,
			Configuration configuration) {
		return getter;
	}


}
