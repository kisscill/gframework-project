/**
 *    Copyright 2006-2016 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.gframework.mybatis.util.generator.core.util;

import java.util.List;
import java.util.StringTokenizer;

/**
 * 
 * @author Jeff Butler
 */
public class StringUtility {

    /**
     * Utility class. No instances allowed
     */
    private StringUtility() {
        super();
    }

    public static boolean stringHasValue(String s) {
        return s != null && s.length() > 0;
    }

    public static String composeFullyQualifiedTableName(String catalog,
            String schema, String tableName, char separator) {
        StringBuilder sb = new StringBuilder();

        if (stringHasValue(catalog)) {
            sb.append(catalog);
            sb.append(separator);
        }

        if (stringHasValue(schema)) {
            sb.append(schema);
            sb.append(separator);
        } else {
            if (sb.length() > 0) {
                sb.append(separator);
            }
        }

        sb.append(tableName);

        return sb.toString();
    }

    /**
     * 判断一个字符串中是否有空格字符
     * @param s 要判断的参数
     * @return 在任意位置只要存在一个' '空格，就返回true，否则返回false，如果参数为null返回false
     */
    public static boolean stringContainsSpace(String s) {
        return s != null && s.indexOf(' ') != -1;
    }

    /**
     * 将字符串进行java代码逃逸
     * @param s 要进行逃逸的字符串
     * @return 返回处理后的字符串
     */
    public static String escapeStringForJava(String s) {
        StringTokenizer st = new StringTokenizer(s, "\"", true); //$NON-NLS-1$
        StringBuilder sb = new StringBuilder();
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if ("\"".equals(token)) { //$NON-NLS-1$
                sb.append("\\\""); //$NON-NLS-1$
            } else {
                sb.append(token);
            }
        }

        return sb.toString();
    }

    /**
     * 将字符串进行xml代码逃逸
     * @param s 要进行逃逸的字符串
     * @return 返回处理后的字符串
     */
    public static String escapeStringForXml(String s) {
        StringTokenizer st = new StringTokenizer(s, "\"", true); //$NON-NLS-1$
        StringBuilder sb = new StringBuilder();
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if ("\"".equals(token)) { //$NON-NLS-1$
                sb.append("&quot;"); //$NON-NLS-1$
            } else {
                sb.append(token);
            }
        }

        return sb.toString();
    }

    public static boolean isTrue(String s) {
        return "true".equalsIgnoreCase(s); //$NON-NLS-1$
    }

    /**
     * 判断字符串中是否包含有sql模糊查询的通配符（"%"和"_"）
     * @param s 要判断的字符串
     * @return 如果有则返回true，否则返回false
     */
    public static boolean stringContainsSQLWildcard(String s) {
        if (s == null) {
            return false;
        }

        return s.indexOf('%') != -1 || s.indexOf('_') != -1;
    }
    
    /**
     * 如果s1为null或s1等于s2，那么返回true，否则返回false.
     * 
     * @param s1 主要判断的字符串
     * @param s2 用于比较判断的字符串
     * @return s1是否为null或等于s2 
     */
	public static boolean stringIsNullOrEquals(String s1,String s2) {
		if (s1 == null) {
			return true ;
		} else {
			return s1.equals(s2);
		}
	}
	
	public static boolean isEmpty(String str) {
		return str == null || "".equals(str);
	}
	
	/**
	 * 根据长度将字符串进行拆分，主要是为了控制每行显示长度.
	 * <p>如果字符串长度和maxLength差距过大，导致计算丢失精度，则会出现问题。</p>
	 * @param str 要拆分的字符串
	 * @param maxLength 每行最大长度
	 * @return 返回拆分后的字符串，如果字符串为null，则返回数组长度为0
	 */
	public static String[] splitByLength(String str,int maxLength) {
		if (str == null) {
			return new String[0];
		}
		String[] arr = new String[(int) Math.ceil(1.0 * str.length() / maxLength)];
		
		int foot = 0 ;
		for (int x = 0 ; x < str.length() ; x += maxLength) {
			int end = x + maxLength > str.length() ? str.length() : x + maxLength ;
			arr[foot ++] = str.substring(x,end);
		}
		return arr;
	}
	
	/**
	 * 去掉字符串指定后缀（如果存在）
	 * @param str 要处理的字符串
	 * @param suffix 要去除的后缀
	 * @return 返回处理后的字符串
	 */
	public static String trimSuffix(String str,String suffix) {
		if (isEmpty(str) || isEmpty(suffix)) return str;
		
		if (str.endsWith(suffix)) {
			return str.substring(0,str.length() - suffix.length());
		} else {
			return str ;
		}
	}
	
	/**
	 * 去掉字符串指定前缀（如果存在）
	 * @param str 要处理的字符串
	 * @param prefix 要去除的前缀
	 * @return 返回处理后的字符串
	 */
	public static String trimPrefix(String str,String prefix) {
		if (isEmpty(str) || isEmpty(prefix)) return str;
			
		if (str.startsWith(prefix)) {
			return str.substring(prefix.length());
		} else {
			return str ;
		}
	}
	
	/**
	 * 去掉字符串指定后缀，不区分大小写（如果存在）
	 * @param str 要处理的字符串
	 * @param suffix 要去除的后缀
	 * @return 返回处理后的字符串
	 */
	public static String trimSuffixIgnoreCase(String str,String suffix) {
		if (isEmpty(str) || isEmpty(suffix)) return str;
		if (str.length() < suffix.length()) return str ;
		
		if (str.substring(str.length() - suffix.length()).equalsIgnoreCase(suffix)) {
			return str.substring(0,str.length() - suffix.length());
		} else {
			return str ;
		}
	}
	
	/**
	 * 去掉字符串指定前缀，不区分大小写（如果存在）
	 * @param str 要处理的字符串
	 * @param prefix 要去除的前缀
	 * @return 返回处理后的字符串
	 */
	public static String trimPrefixIgnoreCase(String str,String prefix) {
		if (isEmpty(str) || isEmpty(prefix)) return str;
		if (str.length() < prefix.length()) return str ;
		
		if (str.substring(0, prefix.length()).equalsIgnoreCase(prefix)) {
			return str.substring(prefix.length());
		} else {
			return str ;
		}
	}
	
	/**
	 * 去掉字符串指定后缀，不区分大小写（如果存在）
	 * @param str 要处理的字符串
	 * @param suffix 要去除的后缀
	 * @return 返回处理后的字符串
	 */
	public static String trimSuffixIgnoreCase(String str,List<String> suffix) {
		if (isEmpty(str) || suffix == null || suffix.isEmpty()) return str;
		
		for (String suf : suffix) {
			if (str.length() < suf.length()) {
				continue ;
			}
			if (str.substring(str.length() - suf.length()).equalsIgnoreCase(suf)) {
				return str.substring(0,str.length() - suf.length());
			}
		}
		
		return str ;
	}
	
	/**
	 * 去掉字符串指定前缀，不区分大小写（如果存在）
	 * @param str 要处理的字符串
	 * @param prefix 要去除的前缀
	 * @return 返回处理后的字符串
	 */
	public static String trimPrefixIgnoreCase(String str,List<String> prefix) {
		if (isEmpty(str) || prefix == null || prefix.isEmpty()) return str;
		
		for (String pre : prefix) {
			if (str.length() < pre.length()) {
				continue ;
			}
			
			if (str.substring(0, pre.length()).equalsIgnoreCase(pre)) {
				return str.substring(pre.length());
			}
		}
		
		return str ;
	}
	
	public static String nvl(String str,String defaultValue){
		return str == null ? defaultValue : str;
		
	}
	
}
