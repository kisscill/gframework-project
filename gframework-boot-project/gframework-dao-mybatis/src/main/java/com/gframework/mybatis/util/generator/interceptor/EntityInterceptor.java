package com.gframework.mybatis.util.generator.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.java.Field;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.TopLevelClass;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;
import com.gframework.mybatis.util.generator.core.interceptor.JavaBeanInterceptor;

/**
 * 此拦截器主要是为了给POJO添加一些注解，并给与一个接口.
 *
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public class EntityInterceptor implements JavaBeanInterceptor {
	
	@Override
	public TopLevelClass createTable(MetaData table, TopLevelClass cls, Configuration configuration) {
		cls.addImportedType("com.gframework.mybatis.entity.pojo.*");
		cls.addSuperInterface(new FullyQualifiedJavaType("Serializable"));
		if ("SYNONYM".equalsIgnoreCase(table.getTableType())) {
			cls.addAnnotation("com.gframework.mybatis.entity.pojo.SynonymTable");
		} else if ("VIEW".equalsIgnoreCase(table.getTableType())) {
			cls.addAnnotation("com.gframework.mybatis.entity.pojo.ViewTable");
		}
		
		return cls ;
	}

	@Override
	public Field createField(MetaData table, TopLevelClass cls, Field field, Column column,
			Configuration configuration) {
		if (column.isAutoincrement()) {
			field.addAnnotationWithGetter("@Authincrement");
		}
		return field;
	}

	@Override
	public Method createSetter(MetaData table, TopLevelClass cls, Method setter, Column column,
			Configuration configuration) {
		return setter ;
	}

	@Override
	public Method createGetter(MetaData table, TopLevelClass cls, Method getter, Column column,
			Configuration configuration) {
		return getter;
	}

	@Override
	public TopLevelClass afterCreate(MetaData table, TopLevelClass cls, Configuration configuration) {
		return cls;
	}
	



}
