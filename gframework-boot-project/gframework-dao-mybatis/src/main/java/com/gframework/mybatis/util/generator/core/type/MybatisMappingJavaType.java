package com.gframework.mybatis.util.generator.core.type;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Mybatis映射javaType转换工具类.
 * mybatis配置javaType时，默认提供了一些内置别名，当使用本类进行转换时，如果有可用别名，则返回别名，否则返回全称。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class MybatisMappingJavaType {
	/**
	 * java类型和mybatis内置别名映射map
	 */
	private static Map<Class<?>,String> mybatisJavaType = new HashMap<>();
	static {
		mybatisJavaType.put(byte.class,"_byte");
		mybatisJavaType.put(short.class,"_short");
		mybatisJavaType.put(long.class,"_long");
		mybatisJavaType.put(int.class,"_int");
		mybatisJavaType.put(double.class,"_double");
		mybatisJavaType.put(float.class,"_float");
		mybatisJavaType.put(boolean.class,"_boolean");
		mybatisJavaType.put(String.class,"string");
		mybatisJavaType.put(Byte.class,"byte");
		mybatisJavaType.put(Short.class,"short");
		mybatisJavaType.put(Long.class,"long");
		mybatisJavaType.put(Integer.class,"int");
		mybatisJavaType.put(Double.class,"double");
		mybatisJavaType.put(Float.class,"float");
		mybatisJavaType.put(Boolean.class,"boolean");
		mybatisJavaType.put(Date.class,"date");
		mybatisJavaType.put(BigDecimal.class,"bigdecimal");
		mybatisJavaType.put(Object.class,"object");
		mybatisJavaType.put(Map.class,"map");
		mybatisJavaType.put(HashMap.class,"hashmap");
		mybatisJavaType.put(List.class,"list");
		mybatisJavaType.put(ArrayList.class,"arraylist");
		mybatisJavaType.put(Collection.class,"collection");
		mybatisJavaType.put(Iterator.class,"iterator");
	}
	
	/**
	 * 根据Class类型返回一个mybatis可识别的javaType类型，可能转换为mybatis内置别名.
	 * @param type 要转换的类型
	 * @return 返回转换后的名称
	 */
	public static String getMybatisJavaTypeName(Class<?> type) {
		String name = mybatisJavaType.get(type);
		return name == null ? type.getName() : name ;
	}
	
	// prevent instance
	private MybatisMappingJavaType() {
	}
}
