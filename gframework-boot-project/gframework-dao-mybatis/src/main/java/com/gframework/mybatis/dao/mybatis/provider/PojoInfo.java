package com.gframework.mybatis.dao.mybatis.provider;

import javax.persistence.Id;

import org.springframework.lang.Nullable;

/**
 * 用来存储POJO类解析信息的实体类
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class PojoInfo {

	/** 列名称集合 */
	private String[] columNames;
	/** 属性名称集合，顺序和{@link #columNames}一一对应 */
	private String[] fieldNames;
	/** 能过作为mybatis动态参数的属性名称集合，即#{fieldName}形式。顺序和{@link #fieldNames}一一对应 */
	private String[] dynamicParamFieldNames;
	/** 表名称 */
	private String tableName;
	/** 是否有设置主键，会读取POJO类使用{@link Id}注解配置的列作为主键 */
	private boolean hasPrimaryKey;
	/** 主键属性名称 */
	private String pkFieldName;
	/** 主键列名称 */
	@Nullable
	private String pkColumnName;

	public String[] getColumNames() {
		return this.columNames;
	}

	public String getTableName() {
		return this.tableName;
	}

	@Nullable
	public String getPkColumnName() {
		return this.pkColumnName;
	}

	public String[] getFieldNames() {
		return this.fieldNames;
	}

	public String[] getDynamicParamFieldNames() {
		return this.dynamicParamFieldNames;
	}

	public boolean isHasPrimaryKey() {
		return this.hasPrimaryKey;
	}

	public String getPkFieldName() {
		return this.pkFieldName;
	}

	void setHasPrimaryKey(boolean hasPrimaryKey) {
		this.hasPrimaryKey = hasPrimaryKey;
	}

	void setPkFieldName(String pkFieldName) {
		this.pkFieldName = pkFieldName;
	}

	void setDynamicParamFieldNames(String[] dynamicParamFieldNames) {
		this.dynamicParamFieldNames = dynamicParamFieldNames;
	}

	void setFieldNames(String[] fieldNames) {
		this.fieldNames = fieldNames;
	}

	void setColumNames(String[] columNames) {
		this.columNames = columNames;
	}

	void setTableName(String tableName) {
		this.tableName = tableName;
	}

	void setPkColumnName(@Nullable String pkColumnName) {
		this.pkColumnName = pkColumnName;
	}

}