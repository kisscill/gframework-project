package com.gframework.mybatis.util.generator.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 根据自定义条件更新(除主键之外的)非null属性字段.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class Update extends AbstractGenerator implements SqlOperatorGenerator, NeedCanUpdate {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("update");
		xml.addAttribute(new Attribute("id", "update"));
		xml.addAttribute(new Attribute("parameterType", "com.gframework.sqlparam.BeanSqlParam"));
		xml.addComment("根据自定义条件更新(除主键之外的)非null属性字段");

		xml.addElement(new TextElement("UPDATE"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);

		XmlElement set = new XmlElement("set");
		
		for (Column col : table.getColumns()) {
			if (col.isPrimaryKey()) {
				continue;
			}
			XmlElement ife1 = new XmlElement("if");
			Attribute test = new Attribute("test","pojo." + col.getFormatColumnName() + " != null");
			ife1.addAttribute(test);
			ife1.addElement(new TextElement(col.getColumnName() + " = #{pojo." + col.getFormatColumnName() + "},"));
			set.addElement(ife1);
		}
		
		xml.addElement(set);
		xml.addElement(super.getSqlParamWhere());

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null ;
	}

}
