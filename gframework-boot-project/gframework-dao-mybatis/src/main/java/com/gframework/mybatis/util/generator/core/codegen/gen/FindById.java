package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.NeedPrimaryKey;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Parameter;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;
import com.gframework.mybatis.util.generator.core.exception.NoPrimaryKeyException;
import com.gframework.mybatis.util.generator.core.type.MybatisMappingJavaType;

/**
 * 根据主键查询一条数据.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class FindById implements SqlOperatorGenerator,NeedCanUpdate,NeedPrimaryKey {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		Column pk = table.getPrimaryKey();
		if (pk == null) {
			throw new NoPrimaryKeyException(table.getTableName() + " 表没有主键！");
		}
		XmlElement xml = new XmlElement("select");
		xml.addAttribute(new Attribute("id", "findById"));
		xml.addAttribute(new Attribute("parameterType", MybatisMappingJavaType.getMybatisJavaTypeName(pk.getJavaType())));
		xml.addAttribute(new Attribute("resultMap", "basicResultMap"));
		xml.addAttribute(new Attribute("useCache", "true"));
		
		xml.addComment("根据主键查询一条数据");

		xml.addElement(new TextElement("SELECT "));
		int foot = 0 ;
		int maxFoot = table.getColumns().size() - 1;
		for (Column col : table.getColumns()) {
			if (foot ++ == maxFoot) {
				xml.addElement(new TextElement("\t" + col.getColumnName()));
			} else {
				xml.addElement(new TextElement("\t" + col.getColumnName() + ","));
			}
		}
		xml.addElement(new TextElement("FROM "));
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		
		XmlElement where = new XmlElement("where");
		where.addElement(new TextElement(pk.getColumnName() + " = #{_parameter}"));
		xml.addElement(where);
		
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("findById");
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		method.setReturnType(new FullyQualifiedJavaType(pojoName));
		method.setVisibility(JavaVisibility.PUBLIC);
		method.addParameter(new Parameter(new FullyQualifiedJavaType(table.getPrimaryKey().getJavaType().getName()),"id"));
		// XXX 后期有时间了添加注释
		return method;
	}

}
