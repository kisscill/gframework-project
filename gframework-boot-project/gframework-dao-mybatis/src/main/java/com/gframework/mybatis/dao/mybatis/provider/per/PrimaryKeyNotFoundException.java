package com.gframework.mybatis.dao.mybatis.provider.per;

import org.springframework.core.NestedRuntimeException;

/**
 * 当需要使用主键但未找到主键时，抛出此异常.
 * 通常是因为错误的操作了POJO或者未在POJO上指定主键列造成的。
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@SuppressWarnings("serial")
public class PrimaryKeyNotFoundException extends NestedRuntimeException {

	public PrimaryKeyNotFoundException(String msg) {
		super(msg);
	}

	public PrimaryKeyNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
