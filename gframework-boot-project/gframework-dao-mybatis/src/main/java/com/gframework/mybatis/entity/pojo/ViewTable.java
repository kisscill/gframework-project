package com.gframework.mybatis.entity.pojo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 此注解可以注释一个POJO实体类，表示此实体类映射的是一个视图.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ViewTable {
}
