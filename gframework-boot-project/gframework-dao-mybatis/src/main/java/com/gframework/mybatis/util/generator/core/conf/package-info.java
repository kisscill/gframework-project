/**
 * 此包的主要功能时提供操作配置信息生成和解析，并根据配置解析数据库信息，最终返回一个数据库描述信息对象集合。
 * <ul>
 * <li>1、构造{@link com.gframework.mybatis.util.generator.core.conf.Configuration}类对象，设置操作参数</li>
 * <li>2、构造{@link com.gframework.mybatis.util.generator.core.conf.Context}类对象，生成数据库信息</li>
 * <li>3、取得{@link com.gframework.mybatis.util.generator.core.conf.MetaData}接口集合，此接口对象描述了数据表的信息</li>
 * </ul>
 * @author Ghwolf
 *
 */
package com.gframework.mybatis.util.generator.core.conf;