package com.gframework.mybatis.util.generator.core.conf;

import com.gframework.mybatis.util.generator.core.exception.GeneratorException;

/**
 * 当解析配置信息或做相关操作时，如果出现异常，则抛出此异常.<br>
 * 比如数据库连接不上，可能是网络问题，也可能是配置问题，或者某些参数格式不合法等。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class ConfigurationException extends GeneratorException {

	private static final long serialVersionUID = 3211666647968471462L;

	public ConfigurationException() {
		super(EVERY_EXCEPTION_PREFIX + "配置信息有误。");
	}

	public ConfigurationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(EVERY_EXCEPTION_PREFIX + message, cause, enableSuppression, writableStackTrace);
	}

	public ConfigurationException(String message, Throwable cause) {
		super(EVERY_EXCEPTION_PREFIX + message, cause);
	}

	public ConfigurationException(String message) {
		super(EVERY_EXCEPTION_PREFIX + message);
	}

	public ConfigurationException(Throwable cause) {
		super(EVERY_EXCEPTION_PREFIX + "配置信息有误。", cause);
	}

}
