package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;
import com.gframework.mybatis.util.generator.core.type.MybatisMappingJavaType;

/**
 * 设置一个最基本的全表字段都包含的ResultMap节点
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public class BasicResultMap implements SqlOperatorGenerator {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("resultMap");
		String pname = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		xml.addAttribute(new Attribute("type",pname));
		xml.addAttribute(new Attribute("id","basicResultMap"));
		xml.addComment("最为基本的无映射关系的实体类映射resultMap");
		xml.addComment("请勿修改，但你可以去编写一个新的resultMap");
		
		for (Column col : table.getColumns()) {
			XmlElement result = new XmlElement(col.isPrimaryKey() ? "id" : "result");
			result.addAttribute(new Attribute("property",col.getFormatColumnName()));
			result.addAttribute(new Attribute("column",col.getColumnName()));
			result.addAttribute(new Attribute("javaType", MybatisMappingJavaType.getMybatisJavaTypeName(col.getJavaType())));
			if (col.isPrimaryKey()) {
				xml.addElement(0,result);
			} else {
				xml.addElement(result);
			}
		}
		
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null;
	}

}
