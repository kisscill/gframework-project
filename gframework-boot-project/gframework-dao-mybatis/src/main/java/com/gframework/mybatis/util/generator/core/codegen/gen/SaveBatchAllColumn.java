package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Parameter;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 保存多条数据，添加包括null在内的所有属性内容(自动增长列除外).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class SaveBatchAllColumn implements SqlOperatorGenerator,NeedCanUpdate {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("insert");
		xml.addAttribute(new Attribute("id", "saveBatchAllColumn"));
		xml.addAttribute(new Attribute("parameterType", "collection"));
		Column pk = table.getPrimaryKey();
		if (pk != null) {
			xml.addAttribute(new Attribute("useGeneratedKeys","true"));
			xml.addAttribute(new Attribute("keyProperty",pk.getFormatColumnName()));
		}
		xml.addComment("保存多条数据，添加包括null在内的所有属性内容(自动增长列除外)");

		xml.addElement(new TextElement("INSERT INTO"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		xml.addElement(new TextElement("("));
		
		int foot = 0 ;
		int maxFoot = table.getColumns().size();
		for (Column col : table.getColumns()) {
			foot ++ ;
			if (col.isAutoincrement()) {
				continue;
			}
			if (foot == maxFoot) {
				xml.addElement(new TextElement("\t" + col.getColumnName()));
			} else {
				xml.addElement(new TextElement("\t" + col.getColumnName() + ","));
			}
		}
		xml.addElement(new TextElement(") VALUES "));
		
		XmlElement foreach = new XmlElement("foreach");
		foreach.addAttribute(new Attribute("collection","collection"));
		foreach.addAttribute(new Attribute("item","_item"));
		foreach.addAttribute(new Attribute("open","("));
		foreach.addAttribute(new Attribute("close",")"));
		foreach.addAttribute(new Attribute("separator","),("));
		xml.addElement(foreach);

		foot = 0 ;
		for (Column col : table.getColumns()) {
			foot ++ ;
			if (col.isAutoincrement()) {
				continue;
			}
			if (foot == maxFoot) {
				foreach.addElement(new TextElement("#{_item." + col.getFormatColumnName() + "}"));
			} else {
				foreach.addElement(new TextElement("#{_item." + col.getFormatColumnName() + "},"));
			}
		}
		
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("saveBatchAllColumn");
		method.setReturnType(FullyQualifiedJavaType.getIntInstance());
		method.setVisibility(JavaVisibility.PUBLIC);
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		FullyQualifiedJavaType fu = new FullyQualifiedJavaType("java.util.Collection");
		fu.addTypeArgument(new FullyQualifiedJavaType(pojoName));
		method.addParameter(new Parameter(fu,"pojos"));
		// XXX 后期有时间了添加注释
		return method;
	}

}
