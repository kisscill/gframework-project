package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 取得全部数据.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class FindAll implements SqlOperatorGenerator,NeedCanUpdate {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("select");
		xml.addAttribute(new Attribute("id", "findAll"));
		xml.addAttribute(new Attribute("resultMap", "basicResultMap"));
		xml.addAttribute(new Attribute("useCache", "true"));
		
		xml.addComment("取得全部数据");

		xml.addElement(new TextElement("SELECT "));
		int foot = 0 ;
		int maxFoot = table.getColumns().size() - 1;
		for (Column col : table.getColumns()) {
			if (foot ++ == maxFoot) {
				xml.addElement(new TextElement("\t" + col.getColumnName()));
			} else {
				xml.addElement(new TextElement("\t" + col.getColumnName() + ","));
			}
		}
		xml.addElement(new TextElement("FROM "));
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("findAll");
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		FullyQualifiedJavaType type = new FullyQualifiedJavaType("java.util.List");
		type.addTypeArgument(new FullyQualifiedJavaType(pojoName));
		method.setReturnType(type);
		method.setVisibility(JavaVisibility.PUBLIC);
		// XXX 后期有时间了添加注释
		return method;
	}

}
