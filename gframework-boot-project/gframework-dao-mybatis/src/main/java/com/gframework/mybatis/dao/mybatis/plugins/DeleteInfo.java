package com.gframework.mybatis.dao.mybatis.plugins;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.lang.Nullable;

/**
 * 删除信息封装类.<br>
 * 拦截删除操作之后，会将删除的相关信息封装在此接口对象中，传给拦截对象进行进一步处理。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see DeleteHelper
 * @see DeleteHandle
 */
public interface DeleteInfo {

	/**
	 * 本方法负责取得被删除的数据信息集合.<br>
	 * 
	 * @return 返回原始Map形式的数据集合，如果你想要以bean的形式返回，可以调用{@link #getDeleteBean(Class)}
	 *         方法
	 * 
	 * @throws SQLException Sql执行异常
	 */
	public List<Map<String, Object>> getDeleteInfo() throws SQLException;

	/**
	 * 本方法负责取得被删除的数据信息集合并转换为bean.<br>
	 * 本方法相对{@link #getDeleteInfo()}来说性能较差，如果你不需要非得转换为bean，你可以调用
	 * {@link #getDeleteInfo()}方法。
	 * 
	 * @param beanClass 要转换的成为的bean的类型
	 *            接口的子类
	 * @return 将所有的数据转换为bean的形式返回。
	 *         需要注意的是，bean属性的名称可以和数据库列相同，也可以采用驼峰命名法，本方法会自动将数据库列名称转换为驼峰命名法去匹配属性，
	 *         如果没有找到，则不忽略跳过。
	 * 
	 * @param <T> 要转换成为的POJO类型
	 * 
	 * @throws SQLException Sql执行异常
	 */
	public <T> List<T> getDeleteBean(Class<T> beanClass) throws SQLException;

	/**
	 * 本方法负责更新即将要被删除的数据的特定列.<br>
	 * 注意，这里的列指的是列名称，而不是POJO的属性名称<br>
	 * 如果参数为null，则不做任何事情
	 * 
	 * @param editParam 要更新的列和值（key：要更新的列(列名称)，value：要更新的内容）
	 * @return 返回更新的行数
	 * 
	 * @throws InvocationTargetException 当反射调用的方法抛出异常的时候，抛出此异常
	 * @throws IllegalAccessException
	 *             当通过反射取得一个bean的属性或方法而不存在，或者存在安全管理器的时候，抛出此异常;
	 */
	public int updateColumn(@Nullable Map<String, Object> editParam) throws InvocationTargetException, IllegalAccessException;

	/**
	 * 取得where子句内容，包含where，例如（WHERE 1=1）
	 * 
	 * @return 返回where子句
	 */
	public String getWhere();

	/**
	 * 取得操作的表名称
	 * 
	 * @return 返回表名称
	 */
	public String getTableName();

	/**
	 * 继续执行原本的删除操作，同时返回删除的行数.<br>
	 * 如果不调用此方法，则原删除语句将不会被执行。
	 * 
	 * @return 删除的行数
	 * 
	 * @throws InvocationTargetException 当反射调用的方法抛出异常的时候，抛出此异常
	 * @throws IllegalAccessException
	 *             当通过反射取得一个bean的属性或方法而不存在，或者存在安全管理器的时候，抛出此异常;
	 */
	public int proceed() throws InvocationTargetException, IllegalAccessException;
}
