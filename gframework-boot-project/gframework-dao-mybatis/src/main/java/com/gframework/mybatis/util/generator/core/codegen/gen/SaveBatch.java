package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.FullyQualifiedJavaType;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.JavaVisibility;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Parameter;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 保存多条数据，只会添加非null属性内容(自动增长列除外).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class SaveBatch implements SqlOperatorGenerator,NeedCanUpdate {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("insert");
		xml.addAttribute(new Attribute("id", "saveBatch"));
		xml.addAttribute(new Attribute("parameterType", "collection"));
		Column pk = table.getPrimaryKey();
		if (pk != null) {
			xml.addAttribute(new Attribute("useGeneratedKeys","true"));
			xml.addAttribute(new Attribute("keyProperty",pk.getFormatColumnName()));
		}
		xml.addComment("保存多条数据，只会添加非null属性内容(自动增长列除外)");

		xml.addElement(new TextElement("INSERT INTO"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		xml.addElement(new TextElement("("));
		
		int foot = 0 ;
		int maxFoot = table.getColumns().size();
		for (Column col : table.getColumns()) {
			foot ++ ;
			if (col.isAutoincrement()) {
				continue;
			}
			if (foot == maxFoot) {
				xml.addElement(new TextElement("\t" + col.getColumnName()));
			} else {
				xml.addElement(new TextElement("\t" + col.getColumnName() + ","));
			}
		}
		xml.addElement(new TextElement(") VALUES "));
		
		XmlElement foreach = new XmlElement("foreach");
		foreach.addAttribute(new Attribute("collection","collection"));
		foreach.addAttribute(new Attribute("item","_item"));
		foreach.addAttribute(new Attribute("open","("));
		foreach.addAttribute(new Attribute("close",")"));
		foreach.addAttribute(new Attribute("separator","),("));
		xml.addElement(foreach);
		
		XmlElement trim = new XmlElement("trim");
		trim.addAttribute(new Attribute("suffixOverrides",","));
		for (Column col : table.getColumns()) {
			if (col.isAutoincrement()) {
				continue;
			}
			XmlElement choose = new XmlElement("choose");
			XmlElement when = new XmlElement("when");
			XmlElement otherwise = new XmlElement("otherwise");
			when.addAttribute(new Attribute("test","_item." + col.getFormatColumnName() + " != null"));
			when.addElement(new TextElement("#{_item." + col.getFormatColumnName() + "},"));
			otherwise.addElement(new TextElement("DEFAULT,"));
			
			choose.addElement(when);
			choose.addElement(otherwise);
			
			trim.addElement(choose);
		}
		foreach.addElement(trim);
		
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		Method method = new Method("saveBatch");
		method.setReturnType(FullyQualifiedJavaType.getIntInstance());
		method.setVisibility(JavaVisibility.PUBLIC);
		String pojoName = config.getBasePackage() + "." + table.getModuleName() + ".entity.pojo."
				+ table.getFormatTableName();
		FullyQualifiedJavaType fu = new FullyQualifiedJavaType("java.util.Collection");
		fu.addTypeArgument(new FullyQualifiedJavaType(pojoName));
		method.addParameter(new Parameter(fu,"pojos"));
		// XXX 后期有时间了添加注释
		return method;
	}

}
