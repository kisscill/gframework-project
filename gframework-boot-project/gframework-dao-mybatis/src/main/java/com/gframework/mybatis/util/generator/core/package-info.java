/**
 * 数据库逆向工程生成代码核心程序包.<br>
 * 虽然本模块与框架耦合度较高，但是此包依然不做太多“特殊操作”，某些个性化配置和处理依然在同级其他相关包中完成。
 * @author Ghwolf
 */
package com.gframework.mybatis.util.generator.core;