package com.gframework.mybatis.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.lang.Nullable;

import com.gframework.mybatis.dao.IViewDao;
import com.gframework.mybatis.service.impl.ServiceImpl;
import com.gframework.mybatis.service.impl.TableServiceImpl;
import com.gframework.mybatis.service.impl.ViewServiceImpl;
import com.gframework.sqlparam.SqlParam;

/**
 * 基于本套mybatis框架的service通用操作父接口.
 * <p>此接口定义了dao类型必须是{@link IViewDao}接口子类/子接口。
 * 
 * @since 2.0.0
 * @author Ghwolf
 *
 * @see ITableService
 * @see IService
 * @see TableServiceImpl
 * @see ViewServiceImpl
 * @see ServiceImpl
 *
 * @param <M> 当前操作对应的dao接口类型
 * @param <T> 实体类类型
 */
public interface IViewService<M extends IViewDao<T>,T extends Serializable> {
	/**
	 * 获取当前操作的service对应的dao类对象，既{@link IViewDao}子类对象
	 * @return 返回IViewDao接口子类对象，具体类型由子类来进行设置
	 */
	public M getDao();
	
	/**
	 * 获取全部数据
	 * @return 将数据封装为List集合返回，如果没有，则集合长度为0
	 */
	public List<T> listAll() ;
	
	/**
	 * 按照指定条件查询数据
	 * @param sqlParam 查询条件封装类，可以为null，为null则相当于调用{@link #listAll()}
	 * @return 将数据封装为List集合返回，如果没有，则集合长度为0
	 */
	public List<T> list(@Nullable SqlParam sqlParam) ;
	
	/**
	 * 按照指定条件查询数据，并转换为指定类型的对象集合返回
	 * @param sqlParam 查询条件封装类，可以为null，为null则相当于调用{@link #listAll()}
	 * @param returnType 查询结果要转换的类型
	 * @return 将数据封装为List集合返回，如果没有，则集合长度为0
	 */
	public <R> List<R> list(@Nullable SqlParam sqlParam,Class<R> returnType) ;
	
	/**
	 * 获取数据总数
	 * @return 返回数据总数，如果没有则返回0
	 */
	public long count() ;
	
	/**
	 * 根据指定条件查询数据总数
	 * @param sqlParam 查询条件封装类，可以为null，为null则相当于调用{@link #count()}
	 * @return 返回数据总数，如果没有则返回0
	 */
	public long count(SqlParam sqlParam) ;
}
