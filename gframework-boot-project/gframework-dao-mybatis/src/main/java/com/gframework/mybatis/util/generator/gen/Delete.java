package com.gframework.mybatis.util.generator.gen;

import com.gframework.mybatis.util.generator.core.codegen.NeedCanUpdate;
import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 根据自定义条件删除数据.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class Delete extends AbstractGenerator implements SqlOperatorGenerator, NeedCanUpdate {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("delete");
		xml.addAttribute(new Attribute("id", "delete"));
		xml.addAttribute(new Attribute("parameterType", "com.gframework.sqlparam.SqlParam"));
		xml.addComment("根据自定义条件删除数据");

		xml.addElement(new TextElement("DELETE FROM"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);
		
		xml.addElement(super.getSqlParamWhere());

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null ;
	}

}
