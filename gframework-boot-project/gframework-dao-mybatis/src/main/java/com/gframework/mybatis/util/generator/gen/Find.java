package com.gframework.mybatis.util.generator.gen;

import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 根据自定义条件查询多条数据(会查询全部字段).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class Find extends AbstractGenerator implements SqlOperatorGenerator {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("select");
		xml.addAttribute(new Attribute("id", "find"));
		xml.addAttribute(new Attribute("parameterType", "com.gframework.sqlparam.SqlParam"));
		xml.addAttribute(new Attribute("resultMap", "basicResultMap"));
		xml.addAttribute(new Attribute("useCache", "false"));
		xml.addComment("根据自定义条件查询多条数据(会查询全部字段)");

		xml.addElement(new TextElement("SELECT"));

		int row = 0;
		int maxRow = table.getColumns().size() - 1;
		for (Column col : table.getColumns()) {
			if (row ++ == maxRow) {
				xml.addElement(new TextElement("\t" + col.getColumnName()));
			} else {
				xml.addElement(new TextElement("\t" + col.getColumnName() + ","));
			}
		}
		xml.addElement(new TextElement("FROM"));

		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);

		xml.addElement(super.getSqlParamWhere());

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null;
	}

}
