package com.gframework.mybatis.util.generator.core.conf;

/**
 * 一个表的全面描述对象.
 * 本类无任何含义，仅仅是作为可比较的封装类，具体参数详情调用出自己决定。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class DbFullName {

	private String catalog;
	private String schema;
	private String nameReg;

	public DbFullName() {
	}

	public DbFullName(String catalog, String schema, String nameReg) {
		super();
		this.catalog = catalog;
		this.schema = schema;
		this.nameReg = nameReg;
	}

	public String getCatalog() {
		return this.catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getSchema() {
		return this.schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getNameReg() {
		return this.nameReg;
	}

	public void setNameReg(String nameReg) {
		this.nameReg = nameReg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((catalog == null) ? 0 : catalog.hashCode());
		result = prime * result + ((nameReg == null) ? 0 : nameReg.hashCode());
		result = prime * result + ((schema == null) ? 0 : schema.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		DbFullName other = (DbFullName) obj;
		if (catalog == null) {
			if (other.catalog != null) return false;
		} else if (!catalog.equals(other.catalog)) return false;
		if (nameReg == null) {
			if (other.nameReg != null) return false;
		} else if (!nameReg.equals(other.nameReg)) return false;
		if (schema == null) {
			if (other.schema != null) return false;
		} else if (!schema.equals(other.schema)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "DbFullName [catalog=" + catalog + ", schema=" + schema + ", nameReg=" + nameReg + "]";
	}

}
