package com.gframework.mybatis.util.generator.core.conf;

import java.util.ArrayList;
import java.util.List;

import com.gframework.mybatis.util.generator.core.util.JavaBeansUtil;
import com.gframework.mybatis.util.generator.core.util.StringUtility;

/**
 * 本类表示为一个普通表类型的元对象.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
class TableMetaData implements MetaData {

	/**
	 * 表所属catalog
	 */
	private String catalog;
	/**
	 * 表所属schema
	 */
	private String schema;
	/**
	 * 表名称
	 */
	private String tableName;
	/**
	 * 转换为驼峰命名法的表名称
	 */
	private String formatTableName ;
	/**
	 * 表注释
	 */
	private String tableComment;
	/**
	 * 模块名称
	 */
	private String moduleName ;
	/**
	 * 表中所有的列信息
	 */
	private List<Column> columns = new ArrayList<>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columns == null) ? 0 : columns.hashCode());
		result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
		return result;
	}

	void addColumn(Column column) {
		this.columns.add(column);
	}
	
	void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	void setSchema(String schema) {
		this.schema = schema;
	}

	void setTableName(String tableName) {
		this.tableName = tableName;
		this.formatTableName = JavaBeansUtil.getCamelCaseString(tableName, true);
	}
	
	/**
	 * 设置表名称，设置格式化表名称时忽略的名称前缀和后缀
	 * @param tableName 表名称
	 * @param ignorePrefix 忽略的名称前缀(不区分大小写)
	 * @param ignoreSuffix 忽略的名称后缀(不区分大小写)
	 */
	void setTableName(String tableName,List<String> ignorePrefix,List<String> ignoreSuffix) {
		this.tableName = tableName;
		String name = StringUtility.trimPrefixIgnoreCase(tableName, ignorePrefix);
		name = StringUtility.trimSuffixIgnoreCase(name, ignoreSuffix);
		this.formatTableName = JavaBeansUtil.getCamelCaseString(name, true);
	}

	void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

	void setColumns(List<Column> columns) {
		this.columns = columns;
	}
	
	void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	@Override
	public String getModuleName() {
		return this.moduleName;
	}

	@Override
	public String getTableComment() {
		return this.tableComment;
	}

	@Override
	public List<Column> getColumns() {
		return this.columns;
	}

	@Override
	public String getCatalog() {
		return this.catalog;
	}

	@Override
	public String getSchema() {
		return this.schema;
	}

	@Override
	public String getTableName() {
		return this.tableName;
	}
	
	@Override
	public String getFormatTableName() {
		return this.formatTableName ;
	}

	@Override
	public String getTableType() {
		return "TABLE";
	}

	@Override
	public boolean isReadonly() {
		return false;
	}

	@Override
	public Column getPrimaryKey() {
		for (Column col : this.columns) {
			if (col.isPrimaryKey()) {
				return col ;
			}
		}
		return null;
	}


}
