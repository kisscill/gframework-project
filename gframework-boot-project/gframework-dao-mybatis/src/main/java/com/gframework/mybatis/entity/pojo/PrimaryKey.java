package com.gframework.mybatis.entity.pojo;

/**
 * 使用此接口表示的POJO实体类是一个带有主键的表映射实体类，并且可以通过{@link #getPrimaryKey()}方法取得主键对象
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 * @param <K> 主键类型
 */
public interface PrimaryKey<K> {
	/**
	 * 取得实体类主键
	 * @return 返回主键对象，等同于主键属性的getter方法。
	 */
	public K getPrimaryKey() ;
}
