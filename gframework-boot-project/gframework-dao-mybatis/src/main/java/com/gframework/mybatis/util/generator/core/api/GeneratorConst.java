package com.gframework.mybatis.util.generator.core.api;

/**
 * 一些和操作无关和说明信息有关的常量.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public interface GeneratorConst {
	/** 版本信息. */
	public static final String VERSION = "0.3.0" ;
	/** 作者. */
	public static final String AUTHOR = "Ghwolf" ;
	/** 当前版本更新时间. */
	public static final String LAST_UPDATE_DATE = "2020-01-22" ;
	/** 代码版本说明. */
	public static final String VERSION_REMARK = "此代码由gframework的自动生成框架生成，框架版本：" + VERSION + ", 当前版本最后更新时间：" + LAST_UPDATE_DATE ;
	/**
	 * <pre>
	 * 【0.3.0更新内容】
	 * POJO支持生成常量（可以在conf中配置，默认开启），常量内容就是列名称或表名称。
	 * 这样开发不用再去关注列名称的值，而且更改方便，耦合度低。
	 * 
	 * 
	 * </pre>
	 */
}
