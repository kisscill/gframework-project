package com.gframework.mybatis.util.generator.gen;

import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 根据自定义条件查询数据量.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class Count extends AbstractGenerator implements SqlOperatorGenerator{

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("select");
		xml.addAttribute(new Attribute("id", "count"));
		xml.addAttribute(new Attribute("parameterType", "com.gframework.sqlparam.SqlParam"));
		xml.addAttribute(new Attribute("resultType", "int"));
		xml.addAttribute(new Attribute("useCache", "false"));
		xml.addComment("根据自定义条件查询数据量");

		xml.addElement(new TextElement("SELECT COUNT(*) "));
		xml.addElement(new TextElement("FROM"));

		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);

		xml.addElement(super.getSqlParamWhere());

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null;
	}

}
