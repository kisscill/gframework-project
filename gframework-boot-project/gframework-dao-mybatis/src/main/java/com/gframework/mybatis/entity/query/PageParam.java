package com.gframework.mybatis.entity.query;

import java.io.Serializable;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.page.PageMethod;

/**
 * 分页参数信息封装类.
 * 本类是一个抽象类，只包含了分页相关的参数封装和，但是不进行分页查询的处理，分页查询
 * <p>
 * 几乎每个模块都有列表查询，其中就包含条件查询和模糊查询以及分页查询，你可以根据自己的业务
 * 编写自己的条件封装bean，并继承此抽象类，就可以实现分页参数的封装。
 * </p>
 *
 * @since 1.0.0
 * @author Ghwolf
 */
public class PageParam implements Serializable{
	private static final long serialVersionUID = -6238908163660403248L;
	/**
	 * 默认当前页码
	 */
	private static final int DEFAULT_CURRENT_PAGE = 1;
	/**
	 * 默认每页显示行数
	 */
	private static final int DEFAULT_LINE_SIZE = 10;

	/**
	 * 当前页数
	 */
	private Integer pageNum = DEFAULT_CURRENT_PAGE;
	/**
	 * 每页显示行数
	 */
	private Integer pageSize = DEFAULT_LINE_SIZE;
	/**
	 * 排序字段[可选]，可以写多个，用逗号分隔，最后加上asc或desc可以控制排序方式，也可以不加.<br>
	 * 其实就是order by子句内容
	 */
	private String orderBy;
	/**
	 * 开始行数[可选]
	 */
	private Integer startRow;
	/**
	 * 结束行数[可选]
	 */
	private Integer endRow;
	/**
	 * 是否查询总数，默认true
	 */
	private boolean count = true;

	/**
	 * 执行PageHelper的分页相关操作方法，并返回 {@link Page}类对象，此对象在执行完一次查询后会产生数据。
	 * 
	 * @return Page类对象，在执行一次查询操作后会有数据出现，该数据就是分页后的数据
	 * @see PageHelper
	 */
	public <T> Page<T> pageHelper() {
		Integer cp = this.pageNum < 1 ? DEFAULT_CURRENT_PAGE : this.pageNum;
		Integer ls = this.pageSize < 1 ? DEFAULT_LINE_SIZE : this.pageSize;
		Page<T> page = PageMethod.startPage(cp, ls, this.count);
		if (this.orderBy != null) {
			page.setOrderBy(this.orderBy);
		}
		if (this.startRow != null) {
			page.setStartRow(this.startRow);
		}
		if (this.endRow != null) {
			page.setStartRow(this.endRow);
		}
		return page;
	}

	public Integer getPageNum() {
		return this.pageNum;
	}

	public void setPageNum(Integer pageNum) {
		if (pageNum != null && pageNum <= 0) {
			pageNum = DEFAULT_CURRENT_PAGE;
		}
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(Integer pageSize) {
		if (pageSize != null && pageSize <= 0) {
			pageSize = DEFAULT_LINE_SIZE;
		}
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return this.orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Integer getStartRow() {
		return this.startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getEndRow() {
		return this.endRow;
	}

	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	public boolean isCount() {
		return this.count;
	}

	public void setCount(boolean count) {
		this.count = count;
	}

}