package com.gframework.mybatis.util.generator.core.codegen.gen;

import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 一个名称为tableName的sql节点，内容是表名称.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public class TableNameSql implements SqlOperatorGenerator {

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("sql");
		xml.addAttribute(new Attribute("id","tableName"));
		xml.addElement(new TextElement(table.getTableName()));
		xml.addComment("数据库表名称");
		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null;
	}

}
