package com.gframework.mybatis.dao.mybatis.plugins;

import java.sql.SQLException;

/**
 * 删除处理操作接口.<br>
 * 如果你需要使用到mybatis的删除拦截器，那么你就需要指定一个删除拦截处理对象，这个对象必须是实现此接口的，
 * 你可以使用预设的对象，也可以自定义新的类实现此接口。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface DeleteHandle {

	/**
	 * 删除拦截，处理删除操作.<br>
	 * 注意，参数DeleteInfo不一定是线程安全的操作对象，所以请不要使用多线程对该对象进行异步操作，否则可能造成参数错乱。
	 * 
	 * @param info DeleteInfo接口对象，包含了删除数据的各种处理方式。
	 * @return 返回给调用处的对象，即sql返回内容
	 * 
	 * @throws SQLException SQL执行异常
	 * @throws ReflectiveOperationException 反射异常
	 * @throws Exception 其他异常
	 */
	public Object interceptor(DeleteInfo info) throws Exception;
}
