package com.gframework.mybatis.entity.pojo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 被此注解注释的属性或属性相关的setter、getter方法，则表示此属性是一个自动增长列.<br>
 * 那么在进行添加数据操作的时候，通常不会将此字段设置到INSERT语句中
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
public @interface Authincrement {

}
