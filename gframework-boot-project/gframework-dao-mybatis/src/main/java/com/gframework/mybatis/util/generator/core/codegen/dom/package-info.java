/**
 * 此包中的类大都是针对类，xml等文件结构的封装类。
 * 如类有属性，方法等，那么对应的就有Method和Field类。
 * @author Ghwolf
 *
 */
package com.gframework.mybatis.util.generator.core.codegen.dom;