package com.gframework.mybatis.util.generator.core.type;

import com.gframework.mybatis.util.generator.core.exception.GeneratorException;

/**
 * 当出现一个未知的JdbcType时，抛出此异常.<br>
 * 此异常通常可以通过版本的升级来解决，如果你所使用的数据库是新的，但是对应的相关代码并未升级，那么可能会出现此错误。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class UnknowJdbcTypeException extends GeneratorException {

	private static final long serialVersionUID = 3211666647968471462L;

	public UnknowJdbcTypeException() {
		super(EVERY_EXCEPTION_PREFIX + "未知的JDBC类型。");
	}

	public UnknowJdbcTypeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(EVERY_EXCEPTION_PREFIX + message, cause, enableSuppression, writableStackTrace);
	}

	public UnknowJdbcTypeException(String message, Throwable cause) {
		super(EVERY_EXCEPTION_PREFIX + message, cause);
	}

	public UnknowJdbcTypeException(String message) {
		super(EVERY_EXCEPTION_PREFIX + message);
	}

	public UnknowJdbcTypeException(Throwable cause) {
		super(EVERY_EXCEPTION_PREFIX + "未知的JDBC类型。", cause);
	}

}
