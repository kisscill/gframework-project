package com.gframework.mybatis.util.generator.gen;

import com.gframework.mybatis.util.generator.core.codegen.SqlOperatorGenerator;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.Attribute;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.TextElement;
import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * 根据自定义条件更新全部字段(除主键之外的).
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class UpdateAllColumn extends AbstractGenerator implements SqlOperatorGenerator{

	@Override
	public XmlElement createSqlXml(MetaData table, Configuration config) {
		XmlElement xml = new XmlElement("update");
		xml.addAttribute(new Attribute("id", "updateAllColumn"));
		xml.addAttribute(new Attribute("parameterType", "com.gframework.sqlparam.BeanSqlParam"));
		xml.addComment("根据自定义条件更新全部字段(除主键之外的)");

		xml.addElement(new TextElement("UPDATE"));
		
		XmlElement tableName = new XmlElement("include");
		tableName.addAttribute(new Attribute("refid", "tableName"));
		xml.addElement(tableName);

		XmlElement set = new XmlElement("set");
		
		for (Column col : table.getColumns()) {
			if (col.isPrimaryKey()) {
				continue;
			}
			set.addElement(new TextElement(col.getColumnName() + " = #{pojo." + col.getFormatColumnName() + "},"));
		}
		
		xml.addElement(set);
		xml.addElement(super.getSqlParamWhere());

		return xml;
	}

	@Override
	public Method createMethod(MetaData table, Configuration config) {
		return null ;
	}

}
