package com.gframework.mybatis.util.generator.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.xml.XmlElement;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;
import com.gframework.mybatis.util.generator.core.interceptor.XmlElementInterceptor;

/**
 * 此拦截器主要是为了添加一些xml注释以及增加一些resultMap的typeHandle.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public class XmlInterceptor implements XmlElementInterceptor {

	@Override
	public XmlElement createXmlElement(MetaData table, XmlElement element, Configuration configuration) {
		return element;
	}

	@Override
	public XmlElement createRootElement(MetaData table, XmlElement root, Configuration configuration) {
		root.addComment("不建议修改此xml的任何内容，重新生成可以覆盖此代码");
		return root ;
	}

}
