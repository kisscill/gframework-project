/**
 * 次包中的类是提供给外部的"接口"，如果要使用此功能，首先查看此包中的类中的方法即可。
 * @author Ghwolf
 *
 */
package com.gframework.mybatis.util.generator.core.api;