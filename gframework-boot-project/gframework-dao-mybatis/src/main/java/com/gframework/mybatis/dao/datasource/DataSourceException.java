package com.gframework.mybatis.dao.datasource;

import org.springframework.core.NestedRuntimeException;

/**
 * 数据源获取，操作相关异常
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class DataSourceException extends NestedRuntimeException {

	private static final String DEFAULT_ERROR_MSG = "数据源操作异常！";

	public DataSourceException() {
		super(DEFAULT_ERROR_MSG);
	}

	public DataSourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataSourceException(String message) {
		super(message);
	}

	public DataSourceException(Throwable cause) {
		super(DEFAULT_ERROR_MSG, cause);
	}
}
