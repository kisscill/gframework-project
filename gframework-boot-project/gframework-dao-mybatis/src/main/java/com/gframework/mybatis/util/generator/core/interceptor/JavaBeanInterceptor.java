package com.gframework.mybatis.util.generator.core.interceptor;

import com.gframework.mybatis.util.generator.core.codegen.dom.java.Field;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.Method;
import com.gframework.mybatis.util.generator.core.codegen.dom.java.TopLevelClass;
import com.gframework.mybatis.util.generator.core.conf.Column;
import com.gframework.mybatis.util.generator.core.conf.Configuration;
import com.gframework.mybatis.util.generator.core.conf.MetaData;

/**
 * javabean创建拦截器.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface JavaBeanInterceptor {

	/**
	 * 创建类拦截，
	 * 当创建好一个类的时候，进行拦截，返回值为最终生成的类结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的表
	 * @param cls 当前已经构造好的类对象
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public TopLevelClass createTable(MetaData table, TopLevelClass cls, Configuration configuration);

	/**
	 * javabean属性创建拦截，
	 * 当创建好一个属性之后，进行拦截，返回值为最终生成的属性结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的表
	 * @param cls 当前已经构造好的属性对象
	 * @param field 属性结构对象
	 * @param column 当前操作的列
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public Field createField(MetaData table, TopLevelClass cls, Field field, Column column,
			Configuration configuration);
	
	/**
	 * setter方法创建拦截，
	 * 当创建好一个setter方法之后，进行拦截，返回值为最终生成的方法结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的表
	 * @param cls 当前已经构造好的方法对象
	 * @param setter setter方法
	 * @param column 当前操作的列
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public Method createSetter(MetaData table, TopLevelClass cls, Method setter, Column column,
			Configuration configuration);

	/**
	 * getter方法创建拦截，
	 * 当创建好一个getter方法之后，进行拦截，返回值为最终生成的方法结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的表
	 * @param cls 当前已经构造好的方法对象
	 * @param getter getter方法
	 * @param column 当前操作的列
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public Method createGetter(MetaData table, TopLevelClass cls, Method getter, Column column,
			Configuration configuration);
	
	/**
	 * 在，类、属性、方法全部创建完成后执行的拦截操作，
	 * 当一个类的全部信息都创建好的时候，进行拦截，返回值为最终生成的类结构，你可以修改，甚至重写或者返回null，则表示不生成。
	 * 
	 * @param table 当前操作的表
	 * @param cls 当前已经构造好的类对象
	 * @param configuration 全局配置信息
	 * @return 返回最终生成结果
	 */
	public TopLevelClass afterCreate(MetaData table, TopLevelClass cls, Configuration configuration) ;
}
