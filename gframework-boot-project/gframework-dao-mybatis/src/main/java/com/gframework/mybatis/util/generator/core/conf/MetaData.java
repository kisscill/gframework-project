package com.gframework.mybatis.util.generator.core.conf;

import java.util.List;

/**
 * 此接口表示为数据库中的元对象（表，视图，同义词，虚拟表等）.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public interface MetaData {

	/**
	 * 取得元对象所在的catalog，如果数据库不支持catalog则返回null
	 */
	public String getCatalog();

	/**
	 * 取得元对象所在的schema，如果数据库不支持schema则返回null
	 */
	public String getSchema();

	/**
	 * 取得表名称，如果想要取得Java命名规范，经过格式化的表名称，请使用{@link #getFormatTableName()}
	 * 
	 * @return 表名称
	 */
	public String getTableName();

	/**
	 * 取得 从数据库命名规范转换为java命名规范的表名称.<br>
	 * 如果想要取得原始表名称，请使用{@link #getTableName()}
	 * 
	 * @return 驼峰命名表名称
	 */
	public String getFormatTableName();

	/**
	 * 取得表类型（VIEW、TABLE、SYNONYM、...）
	 * 
	 * @return 返回表类型
	 */
	public String getTableType();

	/**
	 * 取得元对象的注释信息.
	 * 
	 * @return 返回表注释
	 */
	public String getTableComment();

	/**
	 * 取得元对象的所有列信息
	 */
	public List<Column> getColumns();

	/**
	 * 取得当前表所属的模块名称
	 * 
	 * @return 返回模块名称
	 */
	public String getModuleName();

	/**
	 * 是否只读.
	 * 
	 * @return true：只读，通常为视图，false：可写
	 */
	public boolean isReadonly();
	
	/**
	 * 取得主键列，如果没有主键返回null，不考虑复合主键.
	 * 
	 * @return 如果存在主键则返回，否则返回null
	 */
	public Column getPrimaryKey() ;
}
