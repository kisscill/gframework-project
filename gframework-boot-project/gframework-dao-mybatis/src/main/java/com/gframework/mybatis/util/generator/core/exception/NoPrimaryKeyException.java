package com.gframework.mybatis.util.generator.core.exception;

/**
 * 当一个表没有主键，但是却生成了主键相关方法时，抛出磁异常.<br>
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public class NoPrimaryKeyException extends GeneratorException {

	private static final long serialVersionUID = -5901449230917011882L;
	private static final String EXCEPTION_MESSAGE_PREFIX = "【自动生成框架异常】";
	protected static final String EVERY_EXCEPTION_PREFIX = " -> 子异常：";

	public NoPrimaryKeyException() {
		super(EXCEPTION_MESSAGE_PREFIX);
	}

	public NoPrimaryKeyException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(EXCEPTION_MESSAGE_PREFIX + message, cause, enableSuppression, writableStackTrace);
	}

	public NoPrimaryKeyException(String message, Throwable cause) {
		super(EXCEPTION_MESSAGE_PREFIX + message, cause);
	}

	public NoPrimaryKeyException(String message) {
		super(EXCEPTION_MESSAGE_PREFIX + message);
	}

	public NoPrimaryKeyException(Throwable cause) {
		super(EXCEPTION_MESSAGE_PREFIX, cause);
	}
}
