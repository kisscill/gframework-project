package com.gframework.mybatis.util.generator.core.codegen;

/**
 * 此接口表示当前操作需要表有主键才可以使用.
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public interface NeedPrimaryKey {
}
