package com.gframework.autoconfigure;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.gframework.mybatis.config.MybatisConfig;
import com.gframework.mybatis.dao.DaoMethodAspect;

/**
 * gframework 框架的mybatis自动装配类
 * @since 2.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(DataSource.class)
@Import({MybatisConfig.class,DaoMethodAspect.class})
public class GframeworkMybatisAutoConfiguration {
	
}
