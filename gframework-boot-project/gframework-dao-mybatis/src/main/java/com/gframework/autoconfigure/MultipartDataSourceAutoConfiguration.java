package com.gframework.autoconfigure;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.gframework.mybatis.dao.datasource.DynamicDataSourceManager;
import com.zaxxer.hikari.HikariDataSource;

/**
 * 多数据源自动装配类.
 * <p>注册的bean会进入到{@link DynamicDataSourceAutoConfiguration}进行管理，
 * 同时目前仅支持{@link HikariDataSource}数据源。
 * 
 * @since 2.0.0
 * @author Ghwolf
 *
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureAfter(DynamicDataSourceAutoConfiguration.class)
@ConditionalOnBean(value=DynamicDataSourceManager.class)
@ConditionalOnClass(HikariDataSource.class)
@EnableConfigurationProperties(MultipartDataSourceProperties.class)
public class MultipartDataSourceAutoConfiguration implements InitializingBean{
	
	@Autowired
	private MultipartDataSourceProperties dataSourceProperties ;
	
	@Autowired
	private DynamicDataSourceManager dynamicDataSourceManager;

	@Override
	public void afterPropertiesSet() throws Exception {
		dataSourceProperties.getMultipart().forEach((k,v) -> {
			HikariDataSource ds = new HikariDataSource();
			ds.copyStateTo(ds);
			dynamicDataSourceManager.register(k, ds);
		});
		
	}
	
}
