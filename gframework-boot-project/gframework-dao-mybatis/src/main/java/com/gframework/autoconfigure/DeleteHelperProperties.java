package com.gframework.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * DeleteHelper配置
 * @since 2.0.0
 * @author Ghwolf
 *
 */
@ConfigurationProperties(prefix = "gframework.mybatis.deletehelper")
public class DeleteHelperProperties {
	/**
	 * 全局拦截策略，如果没有指定，则会走默认策略：<br>
	 * true表示默认进行拦截处理(这将意味着所有删除逻辑都会)，false表示不进行拦截处理（默认）。
	 */
	private boolean doInterceptor = false;
	
	public void setDoInterceptor(boolean doInterceptor) {
		this.doInterceptor = doInterceptor;
	}

	public boolean isDoInterceptor() {
		return this.doInterceptor;
	}
}