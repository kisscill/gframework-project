package com.gframework.autoconfigure;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;

import com.gframework.mybatis.dao.mybatis.plugins.DeleteHandle;
import com.gframework.mybatis.dao.mybatis.plugins.DeleteHelper;

/**
 * DeleteHelper逻辑删除mybatis插件自动装配类
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods = true)
@ConditionalOnClass({ DeleteHelper.class })
@ConditionalOnBean({ SqlSessionFactory.class })
@EnableConfigurationProperties(DeleteHelperProperties.class)
@AutoConfigureAfter(MybatisAutoConfiguration.class)
public class DeleteHelperAutoConfiguration extends AbstractMybatisPluginConfiguration {

	@Autowired
	private DeleteHelperProperties properties;

	@Autowired(required = false)
	@Nullable
	private DeleteHandle defaultDeleteHandle;

	@Override
	protected Interceptor getPlugin() {
		return new DeleteHelper(properties.isDoInterceptor(), defaultDeleteHandle);
	}

}
