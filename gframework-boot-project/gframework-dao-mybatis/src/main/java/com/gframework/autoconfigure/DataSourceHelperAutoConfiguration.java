package com.gframework.autoconfigure;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gframework.mybatis.dao.datasource.DynamicDataSourceManager;
import com.gframework.mybatis.dao.mybatis.plugins.DataSourceHelper;

/**
 * DataSourceHelper多数据源mybatis自动装配类
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods = true)
@ConditionalOnClass({DataSourceHelper.class,DynamicDataSourceManager.class})
@ConditionalOnSingleCandidate(DynamicDataSourceManager.class)
@ConditionalOnBean({SqlSessionFactory.class,DataSourceAutoConfiguration.class})
@AutoConfigureAfter({ DynamicDataSourceAutoConfiguration.class, 
		DataSourceAutoConfiguration.class , MybatisAutoConfiguration.class})
public class DataSourceHelperAutoConfiguration extends AbstractMybatisPluginConfiguration{

	@Bean
	public DataSourceHelper getDataSourceHelper(){
		return new DataSourceHelper();
	}
	
	@Override
	protected Interceptor getPlugin() {
		return this.getDataSourceHelper().getDataSourceHelperInterceptor();
	}

}
