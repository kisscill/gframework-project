package com.gframework.autoconfigure;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.zaxxer.hikari.HikariConfig;

/**
 * 多数据源配置信息
 * @since 2.0.0
 * @author Ghwolf
 *
 */
@ConfigurationProperties("gframework.datasource")
public class MultipartDataSourceProperties {
	
	/**
	 * 多数据源配置，map形式，key表示datasource的id，value则是HikariConfig配置
	 */
	private Map<String,HikariConfig> multipart ;

	
	public Map<String, HikariConfig> getMultipart() {
		return this.multipart;
	}

	
	public void setMultipart(Map<String, HikariConfig> multipart) {
		this.multipart = multipart;
	}
	
	

}
