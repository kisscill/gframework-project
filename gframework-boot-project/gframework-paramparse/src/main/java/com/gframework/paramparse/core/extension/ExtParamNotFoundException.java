package com.gframework.paramparse.core.extension;

import org.springframework.core.NestedRuntimeException;

/**
 * 当参数解析器依赖一些额外的环境参数，但是并未取得或参数不正确时，抛出此异常。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class ExtParamNotFoundException extends NestedRuntimeException {

	/**
	 * 创建异常对象，自定义异常信息
	 * @param msg 异常信息
	 */
	public ExtParamNotFoundException(String msg) {
		super(msg);
	}
	/**
	 * 创建异常对象，自定义异常信息和子异常
	 * @param msg 异常信息
	 * @param cause 子异常
	 */
	public ExtParamNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * 取得一个用于描述指定扩展参数不存在的异常信息
	 * @param paramKey 参数key值
	 * @return 返回异常对象
	 */
	public static ExtParamNotFoundException non(String paramKey) {
		return new ExtParamNotFoundException("扩展参数：" + paramKey + " 不存在！");
	}
	/**
	 * 取得一个用于描述指定扩展参数类型不正确的异常信息
	 * @param paramKey 参数key值
	 * @param needClass 需要的类型
	 * @param currentClass 当前类型
	 * @return 返回异常对象
	 */
	public static ExtParamNotFoundException typeError(String paramKey,Class<?> needClass,Class<?> currentClass) {
		return new ExtParamNotFoundException("扩展参数：" + paramKey + " 类型不正确！期望是 [" + needClass + "] ，实际为 [" + currentClass + "]");
	}
	
}
