package com.gframework.paramparse.points;

import java.util.Optional;

import com.gframework.paramparse.core.AnalyticFormatException;
import com.gframework.paramparse.core.ParamParser;
import com.gframework.paramparse.core.extension.ExtParam;
import com.gframework.paramparse.core.extension.ExtParamNotFoundException;

/**
 * 多段式参数解析器基础接口.<br>
 * <strong>参数解析器解析格式定义：</strong>
 * 详情可以参考：{@link PointSegParamParseContext}
 * <p>
 * 在任何地方只要创建此接口的子类，并被spring管理，就可以生效。
 * </p>
 *
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see PointSegParamParseContext
 *
 */
public interface PointSegParamParser extends ParamParser<String[]>{
	
	/**
	 * 参数解析方法.<br>
	 * <p>
	 * 需要注意的是，本方法传递的参数不是一个完整的需要解析的字符串，仅仅是一个参数内容，并且不包含特定的参数头和参数尾部，例如:
	 * <li>user.id</li>
	 * <li>name</li>
	 * <li>user.car.name.a</li>
	 * 通过此方法可以将该参数解析并返回，如果解析错误或参数不存在，也不应该出现错误，而是返回null
	 * </p>
	 * 
	 * @param param 需要被解析的参数，仅为参数部分，不包含参数头和参数尾
	 * @param extParam 自定义扩展参数获取操作类，不为null
	 * @return 返回解析后的内容，如果没有或出现问题，应当返回null
	 */
	@Override
	public Optional<String> parse(String param,String[] vo, ExtParam extParam) throws ExtParamNotFoundException;

	/**
	 * 初始化解析格式.<br>
	 * 此方法会在解析器初始化的时候执行，需要返回一个合法的参数解析格式，但是不包含参数头和参数尾，例如：
	 * <li>user.ip</li>
	 * <li>user</li>
	 * <li>user.car.name.a</li>
	 * <li>user.car.*</li>
	 * @return 返回解析格式
	 * @throws AnalyticFormatException 解析格式不合法
	 */
	public String initAnalyticFormat() throws AnalyticFormatException;


}
