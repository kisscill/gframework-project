/**
 * 参数解析器扩展操作相关类所在包，此操作主要是让参数解析器可以支持自定义动态参数，
 * 解析时调用出可以传入自定义参数，来影响部分解析器的处理。
 * 或者某些解析器也可以必须依赖某些特定参数。
 * 
 * @author Ghwolf
 */
package com.gframework.paramparse.core.extension;