package com.gframework.paramparse.core;

import com.gframework.lang.IndexRange;

/**
 * 解析参数范围操作接口.
 * 含有特殊参数标记的参数解析上下文应当实现此接口，以提供寻找下一个参数标记所在位置的方法。
 * 同时也提供了一些包含参数的字符串的一些便捷查询方法
 * 
 * @since 1.0.0
 * @author Ghwolf
 *
 */
public interface AnalysisParamScope {
	/**
	 * 取得前缀标记字符串长度
	 * @return 返回前缀标记字符串长度
	 */
	public int prefixLength() ;
	/**
	 * 取得后缀标记字符串长度
	 * @return 返回前后标记字符串长度
	 */
	public int suffixLength() ;
	/**
	 * 从头开始寻找第一个参数标记所在字符串中的开始索引，如果没有返回{@code -1}.
	 * 
	 * @param str 要处理的字符串，如果为null，则一定返回{@code -1}
	 * @return 返回下一个参数开始标记所在位置，如果没有找到返回{@code -1}
	 * 
	 * @implSepc 此方法不应该出现异常
	 */
	public int nextPrefix(String str);
	/**
	 * 从头开始寻找第一个参数标记所在字符串中的结尾索引，如果没有返回{@code -1}.
	 * 
	 * @param str 要处理的字符串，如果为null，则一定返回{@code -1}
	 * @return 返回下一个参数结尾标记所在位置，如果没有找到返回{@code -1}
	 * 
	 * @implSepc 此方法不应该出现异常
	 */
	public int nextSuffix(String str);
	
	/**
	 * 从头开始寻找第一个参数标记范围所在字符串中的开始和结尾索引，如果没有找到，则会返回一个start和end均为{@code -1}的对象，即便开始索引不是{@code -1}.
	 * 
	 * @param str 要处理的字符串，如果为null，则一定返回start和end均为{@code -1}的对象
	 * @return 返回下一个参数结尾标记所在位置，如果没有找到，则start和end都为{@code -1}，你可以通过{@link IndexRange#hasRange()}来判断是否找到了范围
	 * 
	 * @implSepc 此方法不应该出现异常
	 */
	public IndexRange next(String str);
	/**
	 * 下一个参数标记所在字符串中的开始索引，如果没有返回{@code -1}.
	 * 
	 * @param str 要处理的字符串，如果为null，则一定返回{@code -1}
	 * @param formIndex 寻找开始索引
	 * @return 返回下一个参数开始标记所在位置，如果没有找到返回{@code -1}
	 * 
	 * @implSepc 此方法不应该出现异常
	 */
	public int nextPrefix(String str,int formIndex);
	/**
	 * 下一个参数标记所在字符串中的结尾索引，如果没有返回{@code -1}.
	 * 
	 * @param str 要处理的字符串，如果为null，则一定返回{@code -1}
	 * @param formIndex 寻找开始索引
	 * @return 返回下一个参数结尾标记所在位置，如果没有找到返回{@code -1}
	 * 
	 * @implSepc 此方法不应该出现异常
	 */
	public int nextSuffix(String str,int formIndex);
	
	/**
	 * 下一个参数标记范围所在字符串中的开始和结尾索引，如果没有找到，则会返回一个start和end均为{@code -1}的对象，即便开始索引不是{@code -1}.
	 * 
	 * @param str 要处理的字符串，如果为null，则一定返回start和end均为{@code -1}的对象
	 * @param formIndex 寻找开始索引
	 * @return 返回下一个参数结尾标记所在位置，如果没有找到，则start和end都为{@code -1}，你可以通过{@link IndexRange#hasRange()}来判断是否找到了范围
	 * 
	 * @implSepc 此方法不应该出现异常
	 */
	public IndexRange next(String str,int formIndex);
	
	/**
	 * 此方法可以用于查询指定字符在字符串中的索引位置，并忽略字符串中的参数.
	 * <pre>
	 * 此方法只会去寻找不属于参数的字符串内容，既：
	 * 如果在字符串 'hello #{id} call id.' 中寻找 'id'
	 * 会忽略 '#{id}' 中的id，会寻找后面的id。
	 * </pre>
	 * 
	 * @param source 被搜索的字符串
	 * @param ch 要查找的字符
	 * @return 返回所在索引位置，没有返回{@code -1}
	 */
	public int indexOfIgnoreParam(String source,int ch);
	
	/**
	 * 此方法可以用于从特定位置开始查询指定字符在字符串中的索引位置，并忽略字符串中的参数.
	 * <pre>
	 * 此方法只会去寻找不属于参数的字符串内容，既：
	 * 如果在字符串 'hello #{id} call id.' 中寻找 'id'
	 * 会忽略 '#{id}' 中的id，会寻找后面的id。
	 * </pre>
	 * 
	 * @param source 被搜索的字符串
	 * @param ch 要查找的字符
	 * @param formIndex 开始索引，如果小于0则按照0处理
	 * @return 返回所在索引位置，没有返回{@code -1}
	 */
	public int indexOfIgnoreParam(String source,int ch,int formIndex);
	/**
	 * 此方法可以用于从特定位置开始查询指定字符串在字符串中的索引位置，并忽略字符串中的参数.
	 * <pre>
	 * 此方法只会去寻找不属于参数的字符串内容，既：
	 * 如果在字符串 'hello #{id} call id.' 中寻找 'id'
	 * 会忽略 '#{id}' 中的id，会寻找后面的id。
	 * </pre>
	 * 
	 * @param source 被搜索的字符串
	 * @param str 要查找的字符串
	 * @return 返回所在索引位置，没有返回{@code -1}
	 */
	public int indexOfIgnoreParam(String source,String str);
	/**
	 * 此方法可以用于从特定位置开始查询指定字符串在字符串中的索引位置，并忽略字符串中的参数.
	 * <pre>
	 * 此方法只会去寻找不属于参数的字符串内容，既：
	 * 如果在字符串 'hello #{id} call id.' 中寻找 'id'
	 * 会忽略 '#{id}' 中的id，会寻找后面的id。
	 * </pre>
	 * 
	 * @param source 被搜索的字符串
	 * @param str 要查找的字符串
	 * @param formIndex 开始索引，如果小于0则按照0处理
	 * @return 返回所在索引位置，没有返回{@code -1}
	 */
	public int indexOfIgnoreParam(String source,String str,int formIndex);

}
