package com.gframework.paramparse.core;

import java.util.Optional;

import com.gframework.paramparse.core.extension.ExtParam;
import com.gframework.paramparse.core.extension.ExtParamNotFoundException;

/**
 * 参数解析器根接口.<br>
 * 参数解析器负责对某一个参数的解析操作，并返回解析结果。
 * 解析过程和方式不做限制，并且可以在解析时灵活的传递一个参数，此参数类型由子或子类决定。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @param <P> 解析参数时传递的辅助参数类型
 * 
 * @see ParamParseContext
 */
public interface ParamParser<P> {

	/**
	 * 参数解析方法.<br>
	 * <p>
	 * 通过此方法可以将该参数解析并返回，如果解析错误或参数不存在，也不应该出现错误，而是返回null
	 * </p>
	 * <p>
	 * 如果你的解析器需要依赖一些与调用环境有关的自定义参数，那么可以通过customParam取得，
	 * 如果不存在或不正确，应当抛出{@link ExtParamNotFoundException}异常。
	 * </p>
	 * 
	 * @param param 需要被解析的参数
	 * @param vo 这是一个可选的用于辅助解析操作的参数，一定不是null，但长度可能为0
	 * @param extParam 额外扩展参数，不为null
	 * @return 返回解析后的内容，如果没有或出现问题，应当返回null
	 * @throws ExtParamNotFoundException 当所需的自定义参数不存在时，抛出此异常
	 */
	public Optional<String> parse(String param, P vo, ExtParam extParam) throws ExtParamNotFoundException;
	
	/**
	 * 该参数解析器解析的结果是否允许被缓存(根据参数key进行缓存).<br>
	 * 部分参数可能通过一些逻辑进行动态获取，每次都不一样，这类参数不应该被缓存。相反一些固定的参数可以被缓存。但不是设置了true就一定会被缓存。
	 * 如果参数长度过长，也不会被缓存。
	 * @return true允许，false不允许
	 */
	public boolean allowCache() ;
}
