package com.gframework.paramparse.core.extension;

import java.util.HashMap;
import java.util.Map;

/**
 * 基于hashmap进行存储的自定义扩展参数存储集合
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see ExtParamContext
 * @see ExtParam
 */
public class HashMapExtParam extends ExtParamContext {

	/**
	 * 存储参数的集合
	 */
	private Map<String, Object> map;

	public HashMapExtParam() {
		this.map = new HashMap<>();
	}

	public HashMapExtParam(int initialCapacity, float loadFactor) {
		this.map = new HashMap<>(initialCapacity, loadFactor);
	}

	public HashMapExtParam(int initialCapacity) {
		this.map = new HashMap<>(initialCapacity);
	}

	public HashMapExtParam(Map<? extends String, ? extends Object> m) {
		this.map = new HashMap<>(m);
	}

	@Override
	protected Object getValue(String key) {
		return this.map.get(key);
	}
	
	/**
	 * 添加一个新的参数并返回当前对象以继续添加
	 * @param key 参数key
	 * @param value 参数值
	 * @return 返回当前对象
	 */
	public HashMapExtParam put(String key,Object value) {
		this.map.put(key, value);
		return this ;
	}
	
	/**
	 * 取得当前保存的参数数量
	 * @return 返回有多少个参数，没有返回0
	 */
	public int size() {
		return this.map.size();
	}
	
	/**
	 * 清空所有参数
	 */
	public void clear() {
		this.map.clear();
	}

}
