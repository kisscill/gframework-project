package com.gframework.paramparse.points;

import com.gframework.datastructure.trie.WildcardTrieTree.ProcessResult;
import com.gframework.paramparse.core.AnalysisParamScope;
import com.gframework.paramparse.core.ParamParseContext;
import com.gframework.paramparse.core.extension.ExtParamParse;

/**
 * 多段式参数的解析操作上下文.<br>
 * <p>
 * 此接口规定了一套支持通配符的多段式参数的解析功能，可以通过此接口对象来对一个字符串中所有的参数做解析处理而后返回解析后的字符串
 * </p>
 * 
 * <pre>
 * <strong>参数格式定义：</strong>
 * 参数必须是由{@code "#{}"}包为起来的，内部是一串以"."分割的字符串，例如：
 * #{ip}
 * #{user.id}
 * #{user.car.name}
 * </pre>
 * 
 * <pre>
 * <strong>参数解析器解析格式定义：</strong>
 * 解析器定义解析格式只需要定义{@code "#{}"} 内的字符串内容即可，但是由两种定义方式：
 * 1、单一参数解析：
 * 当定义：user.id时，只能处理#{user.id}参数。
 * 2、通配符参数解析：
 * 通配符只有一种就是星号："*"，他可以出现两个地方：
 * <strong>非最后出现</strong>：只能表示一个不含点"."的字符串，而且必须存在，例如：
 * 当定义：user.*.a时，可以处理#{user.id.a},#{user.name.a}等参数，不能处理：#{user.a},#{user.xx.yy.a},#{user.name.a.b}
 * <strong>最后出现</strong>：可以匹配到最后所有的字符串，包括点"."，而且不是必须存在的，例如：
 * 当定义：user.*时，可以处理#{user},#{user.id},#{user.card.name},#{user.a.b.c.d.e}
 * 3、通配符参数解析注意事项：
 * 通配符左右不能由其他字符，即：user.na*。参数字符串以"."分割，分割内容可以时一个字符串，也可以是一个"*"表示匹配任意。
 * 通配符可以用在任何地方，也可以使用多个，例如以下都是合法的：
 * user.*
 * user.*.id
 * user.*.*.name
 * *.user.car.*.*.name
 * 
 * 参数中的内容暂不支持转义字符处理，参数内容不能含有参数头和参数尾部分，但是部分组合字符可以通过特定参数取得。
 * 例如你要就是要就是希望字符串中包含内容"#{}"而不想让他被当作参数解析，
 * 可以寻找特定的参数解析器，例如（仅作说明，并非真实存在）：#{parse} 参数会被转化为 #{}
 * 
 * 4、定义新的参数解析器
 * 你只需要让一个类实现 {@link PointSegParamParser} 接口，然后被spring管理即可。
 * 
 * </pre>
 * <p>
 * <strong>关于优先级：</strong><br>
 * 优先级采用越精准优先级越高，无通配符匹配优先级最高，通配符匹配的，越精准优先级越高，例如：
 * <br>
 * 以下几种写法从上到下优先级依次降低：
 * <ol>
 * <li>user.car.name.a</li>
 * <li>user.car.name.*</li>
 * <li>user.car.*.a</li>
 * <li>user.car.*.*</li>
 * <li>user.car.*</li>
 * <li>user.*.name.a</li>
 * <li>user.*.name.*</li>
 * <li>user.*.*.a</li>
 * <li>user.*.*.*</li>
 * <li>user.*.*</li>
 * <li>user.*</li>
 * <li>*.car.name.a</li>
 * <li>*.car.name.*</li>
 * <li>*.car.*.a</li>
 * <li>*.car.*.*</li>
 * <li>*.*.name.a</li>
 * <li>*.*.name.*</li>
 * <li>*.*.*.a</li>
 * <li>*.*.*.*</li>
 * <li>*.*.*</li>
 * <li>*.*</li>
 * <li>*</li>
 * </ol>
 * </p>
 * <p>
 *
 * @since 1.0.0
 * @author Ghwolf
 * @see PointSegParamParser
 */
public interface PointSegParamParseContext extends ParamParseContext,ExtParamParse,AnalysisParamScope {

	/**
	 * 根据参数取得适合这个参数且优先级最高的解析器.<br>
	 * 
	 * @param param 即将进行处理的参数，不包含参数头和参数尾
	 * @return 如果存在可以处理该参数的解析器，则返回最优先的，返回对象一定不是null，但是value可能是null
	 */
	public ProcessResult<PointSegParamParser> getParser(String param);
	
	/**
	 * 取得参数前缀，不为null或空字符串
	 * @return 参数前缀
	 */
	public String getParamPrefix() ;
	
	/**
	 * 取得参数后缀，不为null或空字符串
	 * @return 参数后缀
	 */
	public String getParamSuffix() ;

}
