package com.gframework.paramparse.points;

import org.springframework.core.NestedRuntimeException;

/**
 * {@link PointSegParserTree} 接口对象初始化异常，通常是因为还未初始化就调用了其方法或重复初始化导致的异常。
 *
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class ParserTreeInitException extends NestedRuntimeException {

	public ParserTreeInitException(String msg) {
		super(msg);
	}

	public ParserTreeInitException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
