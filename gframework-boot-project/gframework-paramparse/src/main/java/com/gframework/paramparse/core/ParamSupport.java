package com.gframework.paramparse.core;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.TYPE_USE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 使用此注解表示该字段或变量支持参数解析.
 * 
 * <p>此注解对程序没有太多实际影响，但是你也可以为其扩展功能，因为他是Runtime的.
 * 通常我们可以使用此注解对方法，属性，参数进行标记，来告诉使用者它是支持参数解析的，
 * 但是具体的解析操作逻辑还是需要手动来实现。因此此注解更多的是一个解释说明的作用。
 * 
 * <p>但是从开发规范的角度来说，将所有会被参数解析的变量加上此注解，
 * 可以更好的知道哪些参数会被解析，哪些不会。
 * 
 * <p>可以再Map泛型上使用，在key上使用表示key会被解析，value上使用表示value会被解析
 * 
 * @since 1.0.0
 * @author Ghwolf
 * 
 * @see ParamParseContext
 * @see ParamParser
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { CONSTRUCTOR, FIELD, METHOD, PARAMETER, TYPE ,TYPE_USE})
public @interface ParamSupport {
}
