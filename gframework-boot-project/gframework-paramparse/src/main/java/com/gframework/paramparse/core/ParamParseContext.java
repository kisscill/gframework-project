package com.gframework.paramparse.core;

import org.springframework.lang.Nullable;

import com.gframework.paramparse.core.extension.ExtParamParse;

/**
 * 此接口是用于操作参数解析的根接口。这是参数解析的上下文，
 * 但此接口不定义具体的参数处理方式和格式，这些都由子接口完成。
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see ParamParser
 * @see ExtParamParse
 */
public interface ParamParseContext {

	/**
	 * 对一个字符串做解析，替换参数，而后返回处理后的字符串.
	 * 
	 * @param str 要处理的字符串
	 * @return 返回处理后的字符串(一定不是null)，如果参数为null则返回""
	 */
	public String parse(@Nullable String str);

}
