package com.gframework.paramparse.core.extension;

import java.util.Optional;

/**
 * 自定义扩展参数获取接口.
 * 你可以通过抽象类 {@link ExtParamContext} 来获取此接口对象
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see ExtParamContext
 */
public interface ExtParam {

	/**
	 * 根据参数key取得自定义扩展参数.<br>
	 * 
	 * @param key 参数key值
	 * @param valueType 值类型，如果类型不符，则抛出异常。
	 * @return 使用Optional类封装返回结果，这意味着结果可能为null
	 * @throws ExtParamNotFoundException 当参数值类型错误时，抛出此异常
	 */
	public <T> Optional<T> get(String key, Class<T> valueType) throws ExtParamNotFoundException;

	/**
	 * 根据参数key取得自定义扩展参数(如果参数为null，则抛出异常).<br>
	 * 
	 * @param key 参数key值，如果不存在则抛出异常
	 * @param valueType 值类型，如果类型不符，则抛出异常。
	 * @return 使用Optional类封装返回结果，这意味着结果可能为null
	 * @throws ExtParamNotFoundException 参数不存在或参数值类型错误时，抛出此异常
	 */
	public <T> T getNotNull(String key, Class<T> valueType) throws ExtParamNotFoundException;

	/**
	 * 根据参数key取得自定义扩展参数.<br>
	 * 
	 * @param key 参数key值
	 * @param valueType 值类型
	 * @param defaultValue 如果无此参数，或类型不符合，则返回的默认值
	 * @return 如果存在并且类型符合则返回，否则返回默认值
	 */
	public <T> T get(String key, Class<T> valueType, T defaultValue);

}
