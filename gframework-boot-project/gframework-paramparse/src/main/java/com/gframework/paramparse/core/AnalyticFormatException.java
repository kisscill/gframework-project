package com.gframework.paramparse.core;

import org.springframework.core.NestedRuntimeException;

/**
 * 参数解析器 解析格式 错误异常.<br>
 * 在初始化参数解析器时，解析格式不符合规范时，抛出此异常。
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
@SuppressWarnings("serial")
public class AnalyticFormatException extends NestedRuntimeException {

	public AnalyticFormatException(String msg) {
		super(msg);
	}

	public AnalyticFormatException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
