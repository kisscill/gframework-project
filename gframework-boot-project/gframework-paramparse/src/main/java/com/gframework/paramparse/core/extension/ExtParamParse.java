package com.gframework.paramparse.core.extension;

import org.springframework.lang.Nullable;

import com.gframework.paramparse.core.ParamParseContext;
import com.gframework.paramparse.core.ParamParser;

/**
 * 此接口可以为解析器扩展一个支持传递自定义参数的参数解析操作，
 * 
 * @since 1.0.0
 * @author Ghwolf
 * @see ParamParser
 * @see ParamParseContext
 */
public interface ExtParamParse {

	/**
	 * 对一个字符串做解析，替换参数，而后返回处理后的字符串.
	 * <p>
	 * 可以传入自定义参数。由于部分解析器在解析参数时，需要依赖一些当前环境的参数，因此可以通过此方法传递。
	 * </p>
	 * 
	 * @param str 要处理的字符串，可为null
	 * @param extParam 自定义额外依赖参数
	 * @return 返回处理后的字符串(一定不是null)，如果参数为null则返回""
	 */
	public String parse(@Nullable String str, ExtParam extParam);

}
