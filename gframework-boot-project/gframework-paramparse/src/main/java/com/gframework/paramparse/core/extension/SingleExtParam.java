package com.gframework.paramparse.core.extension;

/**
 * 仅包含一个参数对的参数信息获取对象.
 * 
 * @since 1.0.0
 * @author Ghwolf
 */
public class SingleExtParam extends ExtParamContext {

	private String key;
	private Object value;

	public SingleExtParam(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	@Override
	protected Object getValue(String key) {
		if (this.key == null) {
			return null;
		} else {
			 return this.key.equals(key) ? this.value : null ;
		}
	}

}
