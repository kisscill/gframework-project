/**
 * 参数解析器及相关操作类所在包.<br>
 * <p>
 * 参数解析有2大核心接口：
 * <ol>
 * <li>解析器核心上下文操作接口：{@link com.gframework.paramparse.core.ParamParseContext}。
 * 此接口时唯一对外提供的操作接口，用于直接的参数解析操作</li>
 * <li>解析器操作接口：{@link com.gframework.paramparse.core.ParamParser}
 * 每一个此接口子类都是一个参数解析操作，通过扩展此接口子类可以实现更多的参数处理。</li>
 * </ol>
 * 以上接口均为操作根接口，实际上参数解析可能会分更多的类型，最好在根接口上定义子接口，去定义自己的解析标准，
 * 而后才去定义实现类。
 * </p>
 * 
 * @author Ghwolf
 */
package com.gframework.paramparse.core;