package com.gframework.autoconfigure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gframework.paramparse.points.GenericPointSegParamParseContext;
import com.gframework.paramparse.points.PointSegParamParseContext;
import com.gframework.paramparse.points.PointSegParamParser;

/**
 * 参数解析器自动装配类.
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@Configuration(proxyBeanMethods=false)
public class ParamParseAutoConfiguration {

	@Bean
	public PointSegParamParseContext getPointSegParamParseContext(PointSegParamParser[] parsers){
		return new GenericPointSegParamParseContext(parsers);
	}
	
}
