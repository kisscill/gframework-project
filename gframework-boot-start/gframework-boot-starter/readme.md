## gframework项目专属继承父pom

为简化开发，减少配置而定制的可直接使用父pom，但是并不具备通用性。

此父pom具有以下特点：

1. 已经内置的阿里云和spring仓库作为maven仓库，无需在进行指定。
2. 已经内置了assembly预设配置，默认使用assembly打包。
3. 已经内置了JDK1.8编译器和完整的maven打包插件配置，无需在进行任何maven插件配置，打包采用的是assembly插件进行打包。
4. 已经内置了springboot项目开发所需要的大部分依赖包。
5. 已经继承了spring-boot-starter-parent的2.3.0.RELEASE版本的父pom。

### properties配置

当然，也有一些配置可以根据需要在你的pom中，配置到properties节点中，分别是：

1、 start-class**(必须)**：主类名称
```xml
<properties>
	<start-class>你的主类全名</start-class>
</properties>
```
2、 java.se.version[可选]：使用的java版本号，默认1.8
```xml
<properties>
	<java.se.version>1.8</java.se.version>
</properties>
```
3、skipAssembly[可选]：设置为true表示关闭assembly打包插件，默认false
```xml
<properties>
	<skipAssembly>true</skipAssembly>
</properties>
```


### 打包介绍

打包采用的是lib分离模式打包，这样方便做程序更新，你不需要每次都打几十兆的jar包去更新。启动和停止程序都有可以直接使用
的sh脚本。

在`src/main/prod-conf`下放置的是应用到生产环境的配置文件，同时他也是-Xbootclasspath/a的路径，因此：

* 对于application配置文件，则会取代jar包中的对应的文件，不会产生覆盖。
* 对于其他配置文件，也会被优先读取 

#### 打包结构

打包结果是一个`tar.gz`压缩包，里面目录结构如下：

* bin：启动脚本所在目录。
* lib：依赖包和程序包所在目录。
* conf：这里放到是`src/main/prod-conf`目录下的application配置文件，同时也是Xbootclasspath

**assembly.xml预设配置和示例，可以参考`gframework-boot-assemblies`项目说明！**