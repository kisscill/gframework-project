package test.gframework.flyway;

import com.gframework.boot.flyway.FlywayDevRunner;

/**
 * 通过main方法不启动springboot容器执行flyway
 * @since 2.0.0
 * @author Ghwolf
 *
 */
public class MainRunFlyway {
	public static void main(String[] args) throws Exception{
		FlywayDevRunner.migrate();
	}
}
