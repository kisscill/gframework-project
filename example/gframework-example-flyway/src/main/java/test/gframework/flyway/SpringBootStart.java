package test.gframework.flyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gframework.annotation.flyway.EnableFlywayExtend;

/**
 * springboot使用flyway扩展包示例
 * @since 2.0.0
 * @author Ghwolf
 *
 */
@SpringBootApplication
@EnableFlywayExtend	// 必须要满足SpringApplication.run的任意启动类有这个注解、同时spring.flyway.enabled=true才会生效
public class SpringBootStart {
	
	public static void main(String[] args) throws Exception {
		// 由于gframework-boot-flyway没有依赖spring-web相关开发包，所以这里运行完毕后程序会直接结束退出
		SpringApplication.run(SpringBootStart.class, args);
	}

}
