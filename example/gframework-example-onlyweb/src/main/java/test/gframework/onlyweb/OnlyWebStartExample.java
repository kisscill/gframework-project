package test.gframework.onlyweb;

import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.gframework.GframeworkSpringBootStart;

/**
 * 这是一个忽略数据库操作，仅需要web操作的启动类
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@SpringBootApplication(exclude = {
		DataSourceAutoConfiguration.class,
		MybatisAutoConfiguration.class
})
public class OnlyWebStartExample {
	
	public static void main(String[] args) {
		SpringApplication.run(new Class[]{
				OnlyWebStartExample.class,
				GframeworkSpringBootStart.class
		},args);
	}

}
