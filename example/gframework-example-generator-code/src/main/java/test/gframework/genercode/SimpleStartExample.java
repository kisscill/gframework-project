package test.gframework.genercode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gframework.GframeworkSpringBootStart;

/**
 * 先启动一次生成表
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@SpringBootApplication
public class SimpleStartExample {
	
	public static void main(String[] args) {
		SpringApplication.run(new Class[]{
				SimpleStartExample.class,
				GframeworkSpringBootStart.class
		},args);
	}

}
