package test.gframework.genercode;

import com.gframework.mybatis.util.generator.main.GeneratorCodeByEnvironment;

public class GeneratorCode {
	public static void main(String[] args) throws Exception {
		GeneratorCodeByEnvironment.config()
			.setBasePackage("com.gframework.biz")
			.run("t_sys_.*", "sys");
	}
}
