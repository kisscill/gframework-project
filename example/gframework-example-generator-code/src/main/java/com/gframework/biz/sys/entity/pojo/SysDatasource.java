package com.gframework.biz.sys.entity.pojo;

import com.gframework.mybatis.entity.pojo.*;
import java.io.Serializable;
import javax.persistence.*;

/** 
 * {@value #TABLE_NAME} 表对应的POJO实体类.<br>
 * 数据源表，目前是按照HikariDataSource数据源来配置的
 *
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
@Table(name=SysDatasource.TABLE_NAME)
public class SysDatasource implements Serializable {
	private static final long serialVersionUID = -367666468L;

	/**
	 * 主键.
	 */
	@Column(name=DS_ID, nullable=false, length=50)
	@Id
	private String dsId;

	/**
	 * 描述.
	 */
	@Column(name=REMARK, nullable=true, length=255)
	private String remark;

	/**
	 * JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中.
	 */
	@Column(name=JNDI, nullable=true, length=30)
	private String jndi;

	/**
	 * 驱动包，可不填，会自动识别.
	 */
	@Column(name=DRIVER_CLASS_NAME, nullable=true, length=60)
	private String driverClassName;

	/**
	 * 连接URL.
	 */
	@Column(name=DB_URL, nullable=false, length=255)
	private String dbUrl;

	/**
	 * 连接用户.
	 */
	@Column(name=USERNAME, nullable=false, length=32)
	private String username;

	/**
	 * 连接密码(建议加密保存).
	 */
	@Column(name=PASSWORD, nullable=false, length=64)
	private String password;

	/**
	 * 连接池名称，会用于线程名称.
	 */
	@Column(name=POOL_NAME, nullable=true, length=30)
	private String poolName;

	/**
	 * 最大连接数[可选].
	 */
	@Column(name=MAX_POOL_SIZE, nullable=true, precision=10)
	private Integer maxPoolSize;

	/**
	 * 最小连接数[可选].
	 */
	@Column(name=MIN_POOL_SIZE, nullable=true, precision=10)
	private Integer minPoolSize;

	/**
	 * 配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在.
	 */
	@Column(name=IDLE_TIMEOUT, nullable=true, precision=10)
	private Integer idleTimeout;

	/**
	 * 配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收.
	 */
	@Column(name=MAX_LIFETIME, nullable=true, precision=10)
	private Integer maxLifetime;

	/**
	 * 获取连接对象超时时间（秒）.
	 */
	@Column(name=LOGIN_TIMEOUT, nullable=true, precision=10)
	private Integer loginTimeout;

	/**
	 * 设置池等待连接验证为活动连接的最大毫秒数.
	 */
	@Column(name=VALIDATION_TIMEOUT, nullable=true, precision=10)
	private Integer validationTimeout;

	/**
	 * 设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取.
	 */
	@Column(name=CONNECTION_TIMEOUT, nullable=true, precision=10)
	private Integer connectionTimeout;

	/**
	 * 设置要执行的SQL查询以测试连接的有效性.
	 */
	@Column(name=CONNECTION_TEST_QUERY, nullable=true, length=255)
	private String connectionTestQuery;

	/**
	 * 是否启用,1:是，0:否.
	 */
	@Column(name=ENABLED, nullable=false, precision=3)
	private Integer enabled;

	/**
	 * 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 */
	@Column(name=BUILTIN, nullable=false, precision=3)
	private Integer builtin;

	/**
	 * 删除标记,1:是，0:否.
	 */
	@Column(name=DELETED, nullable=false, precision=3)
	private Integer deleted;

	/** 当前POJO对应表名称：{@value} */
	public static final String TABLE_NAME = "t_sys_datasource";

	/** dsId 属性 对应的列名称：{@value}。 主键 */
	public static final String DS_ID = "DS_ID";

	/** remark 属性 对应的列名称：{@value}。 描述 */
	public static final String REMARK = "REMARK";

	/** jndi 属性 对应的列名称：{@value}。 JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中 */
	public static final String JNDI = "JNDI";

	/** driverClassName 属性 对应的列名称：{@value}。 驱动包，可不填，会自动识别 */
	public static final String DRIVER_CLASS_NAME = "DRIVER_CLASS_NAME";

	/** dbUrl 属性 对应的列名称：{@value}。 连接URL */
	public static final String DB_URL = "DB_URL";

	/** username 属性 对应的列名称：{@value}。 连接用户 */
	public static final String USERNAME = "USERNAME";

	/** password 属性 对应的列名称：{@value}。 连接密码(建议加密保存) */
	public static final String PASSWORD = "PASSWORD";

	/** poolName 属性 对应的列名称：{@value}。 连接池名称，会用于线程名称 */
	public static final String POOL_NAME = "POOL_NAME";

	/** maxPoolSize 属性 对应的列名称：{@value}。 最大连接数[可选] */
	public static final String MAX_POOL_SIZE = "MAX_POOL_SIZE";

	/** minPoolSize 属性 对应的列名称：{@value}。 最小连接数[可选] */
	public static final String MIN_POOL_SIZE = "MIN_POOL_SIZE";

	/** idleTimeout 属性 对应的列名称：{@value}。 配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在 */
	public static final String IDLE_TIMEOUT = "IDLE_TIMEOUT";

	/** maxLifetime 属性 对应的列名称：{@value}。 配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收 */
	public static final String MAX_LIFETIME = "MAX_LIFETIME";

	/** loginTimeout 属性 对应的列名称：{@value}。 获取连接对象超时时间（秒） */
	public static final String LOGIN_TIMEOUT = "LOGIN_TIMEOUT";

	/** validationTimeout 属性 对应的列名称：{@value}。 设置池等待连接验证为活动连接的最大毫秒数 */
	public static final String VALIDATION_TIMEOUT = "VALIDATION_TIMEOUT";

	/** connectionTimeout 属性 对应的列名称：{@value}。 设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取 */
	public static final String CONNECTION_TIMEOUT = "CONNECTION_TIMEOUT";

	/** connectionTestQuery 属性 对应的列名称：{@value}。 设置要执行的SQL查询以测试连接的有效性 */
	public static final String CONNECTION_TEST_QUERY = "CONNECTION_TEST_QUERY";

	/** enabled 属性 对应的列名称：{@value}。 是否启用,1:是，0:否 */
	public static final String ENABLED = "ENABLED";

	/** builtin 属性 对应的列名称：{@value}。 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改 */
	public static final String BUILTIN = "BUILTIN";

	/** deleted 属性 对应的列名称：{@value}。 删除标记,1:是，0:否 */
	public static final String DELETED = "DELETED";

	public SysDatasource() {
		
	}

	/**
	 * 设置 主键.
	 * @param dsId 主键
	 */
	public void setDsId(String dsId) {
		this.dsId = dsId ;
	}

	/**
	 * 取得 主键.
	 * @return 主键
	 */
	@Column(name=DS_ID, nullable=false, length=50)
	@Id
	public String getDsId() {
		return this.dsId ;
	}

	/**
	 * 设置 描述.
	 * @param remark 描述
	 */
	public void setRemark(String remark) {
		this.remark = remark ;
	}

	/**
	 * 取得 描述.
	 * @return 描述
	 */
	@Column(name=REMARK, nullable=true, length=255)
	public String getRemark() {
		return this.remark ;
	}

	/**
	 * 设置 JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中.
	 * @param jndi JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中
	 */
	public void setJndi(String jndi) {
		this.jndi = jndi ;
	}

	/**
	 * 取得 JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中.
	 * @return JNDI名称，程序不应在此名称前后增加其他标识。此名称就是该数据源的JNDI全称。因此如果需要兼容web容器，需要再配置的时候增加一些前缀
            如果程序运行环境有JNDIContext，则设置，否则不设置。
            如果修改JNDI，那么就的绑定的JNDI则会取消绑定。
            如果JNDI名称重复，则不会绑定，但是错误会输出在日志中
	 */
	@Column(name=JNDI, nullable=true, length=30)
	public String getJndi() {
		return this.jndi ;
	}

	/**
	 * 设置 驱动包，可不填，会自动识别.
	 * @param driverClassName 驱动包，可不填，会自动识别
	 */
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName ;
	}

	/**
	 * 取得 驱动包，可不填，会自动识别.
	 * @return 驱动包，可不填，会自动识别
	 */
	@Column(name=DRIVER_CLASS_NAME, nullable=true, length=60)
	public String getDriverClassName() {
		return this.driverClassName ;
	}

	/**
	 * 设置 连接URL.
	 * @param dbUrl 连接URL
	 */
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl ;
	}

	/**
	 * 取得 连接URL.
	 * @return 连接URL
	 */
	@Column(name=DB_URL, nullable=false, length=255)
	public String getDbUrl() {
		return this.dbUrl ;
	}

	/**
	 * 设置 连接用户.
	 * @param username 连接用户
	 */
	public void setUsername(String username) {
		this.username = username ;
	}

	/**
	 * 取得 连接用户.
	 * @return 连接用户
	 */
	@Column(name=USERNAME, nullable=false, length=32)
	public String getUsername() {
		return this.username ;
	}

	/**
	 * 设置 连接密码(建议加密保存).
	 * @param password 连接密码(建议加密保存)
	 */
	public void setPassword(String password) {
		this.password = password ;
	}

	/**
	 * 取得 连接密码(建议加密保存).
	 * @return 连接密码(建议加密保存)
	 */
	@Column(name=PASSWORD, nullable=false, length=64)
	public String getPassword() {
		return this.password ;
	}

	/**
	 * 设置 连接池名称，会用于线程名称.
	 * @param poolName 连接池名称，会用于线程名称
	 */
	public void setPoolName(String poolName) {
		this.poolName = poolName ;
	}

	/**
	 * 取得 连接池名称，会用于线程名称.
	 * @return 连接池名称，会用于线程名称
	 */
	@Column(name=POOL_NAME, nullable=true, length=30)
	public String getPoolName() {
		return this.poolName ;
	}

	/**
	 * 设置 最大连接数[可选].
	 * @param maxPoolSize 最大连接数[可选]
	 */
	public void setMaxPoolSize(Integer maxPoolSize) {
		this.maxPoolSize = maxPoolSize ;
	}

	/**
	 * 取得 最大连接数[可选].
	 * @return 最大连接数[可选]
	 */
	@Column(name=MAX_POOL_SIZE, nullable=true, precision=10)
	public Integer getMaxPoolSize() {
		return this.maxPoolSize ;
	}

	/**
	 * 设置 最小连接数[可选].
	 * @param minPoolSize 最小连接数[可选]
	 */
	public void setMinPoolSize(Integer minPoolSize) {
		this.minPoolSize = minPoolSize ;
	}

	/**
	 * 取得 最小连接数[可选].
	 * @return 最小连接数[可选]
	 */
	@Column(name=MIN_POOL_SIZE, nullable=true, precision=10)
	public Integer getMinPoolSize() {
		return this.minPoolSize ;
	}

	/**
	 * 设置 配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在.
	 * @param idleTimeout 配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在
	 */
	public void setIdleTimeout(Integer idleTimeout) {
		this.idleTimeout = idleTimeout ;
	}

	/**
	 * 取得 配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在.
	 * @return 配置连接在连接池中的最大空闲时间（毫秒），如果为0则表示永久存在
	 */
	@Column(name=IDLE_TIMEOUT, nullable=true, precision=10)
	public Integer getIdleTimeout() {
		return this.idleTimeout ;
	}

	/**
	 * 设置 配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收.
	 * @param maxLifetime 配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收
	 */
	public void setMaxLifetime(Integer maxLifetime) {
		this.maxLifetime = maxLifetime ;
	}

	/**
	 * 取得 配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收.
	 * @return 配置连接在连接池中的最大生存时间（毫秒），和idle-timeout不同之处在于，它指代的是多久没被使用就回收，而这个是指不管用没用，存在这么久了就会等待空闲时立刻回收
	 */
	@Column(name=MAX_LIFETIME, nullable=true, precision=10)
	public Integer getMaxLifetime() {
		return this.maxLifetime ;
	}

	/**
	 * 设置 获取连接对象超时时间（秒）.
	 * @param loginTimeout 获取连接对象超时时间（秒）
	 */
	public void setLoginTimeout(Integer loginTimeout) {
		this.loginTimeout = loginTimeout ;
	}

	/**
	 * 取得 获取连接对象超时时间（秒）.
	 * @return 获取连接对象超时时间（秒）
	 */
	@Column(name=LOGIN_TIMEOUT, nullable=true, precision=10)
	public Integer getLoginTimeout() {
		return this.loginTimeout ;
	}

	/**
	 * 设置 设置池等待连接验证为活动连接的最大毫秒数.
	 * @param validationTimeout 设置池等待连接验证为活动连接的最大毫秒数
	 */
	public void setValidationTimeout(Integer validationTimeout) {
		this.validationTimeout = validationTimeout ;
	}

	/**
	 * 取得 设置池等待连接验证为活动连接的最大毫秒数.
	 * @return 设置池等待连接验证为活动连接的最大毫秒数
	 */
	@Column(name=VALIDATION_TIMEOUT, nullable=true, precision=10)
	public Integer getValidationTimeout() {
		return this.validationTimeout ;
	}

	/**
	 * 设置 设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取.
	 * @param connectionTimeout 设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取
	 */
	public void setConnectionTimeout(Integer connectionTimeout) {
		this.connectionTimeout = connectionTimeout ;
	}

	/**
	 * 取得 设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取.
	 * @return 设置客户端等待来自池的连接的最大毫秒数。如果超过此时间而没有连接可用，将从javax.sql.DataSource.getConnection()获取
	 */
	@Column(name=CONNECTION_TIMEOUT, nullable=true, precision=10)
	public Integer getConnectionTimeout() {
		return this.connectionTimeout ;
	}

	/**
	 * 设置 设置要执行的SQL查询以测试连接的有效性.
	 * @param connectionTestQuery 设置要执行的SQL查询以测试连接的有效性
	 */
	public void setConnectionTestQuery(String connectionTestQuery) {
		this.connectionTestQuery = connectionTestQuery ;
	}

	/**
	 * 取得 设置要执行的SQL查询以测试连接的有效性.
	 * @return 设置要执行的SQL查询以测试连接的有效性
	 */
	@Column(name=CONNECTION_TEST_QUERY, nullable=true, length=255)
	public String getConnectionTestQuery() {
		return this.connectionTestQuery ;
	}

	/**
	 * 设置 是否启用,1:是，0:否.
	 * @param enabled 是否启用,1:是，0:否
	 */
	public void setEnabled(Integer enabled) {
		this.enabled = enabled ;
	}

	/**
	 * 取得 是否启用,1:是，0:否.
	 * @return 是否启用,1:是，0:否
	 */
	@Column(name=ENABLED, nullable=false, precision=3)
	public Integer getEnabled() {
		return this.enabled ;
	}

	/**
	 * 设置 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 * @param builtin 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改
	 */
	public void setBuiltin(Integer builtin) {
		this.builtin = builtin ;
	}

	/**
	 * 取得 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改.
	 * @return 是否内置,1:是，0:否。,通常区分内置和用户自定义是为了区分哪些允许用户修改
	 */
	@Column(name=BUILTIN, nullable=false, precision=3)
	public Integer getBuiltin() {
		return this.builtin ;
	}

	/**
	 * 设置 删除标记,1:是，0:否.
	 * @param deleted 删除标记,1:是，0:否
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted ;
	}

	/**
	 * 取得 删除标记,1:是，0:否.
	 * @return 删除标记,1:是，0:否
	 */
	@Column(name=DELETED, nullable=false, precision=3)
	public Integer getDeleted() {
		return this.deleted ;
	}
}