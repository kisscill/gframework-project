package com.gframework.biz.sys.entity.pojo;

import com.gframework.mybatis.entity.pojo.*;
import java.io.Serializable;
import javax.persistence.*;

/** 
 * {@value #TABLE_NAME} 表对应的POJO实体类.<br>
 * 图标表，此表存储两种类型的图标：
1、base64小图标（最多64kb）
2、class指定的图
 *
 * <p>此代码由gframework的自动生成框架生成，框架版本：0.3.0, 当前版本最后更新时间：2020-01-22</p>
 * @since 2020-09-16
 * @author Ghwolf
 * @version 0.3.0
 */
@Table(name=SysIcon.TABLE_NAME)
public class SysIcon implements Serializable {
	private static final long serialVersionUID = -894343944L;

	/**
	 * 主键.
	 */
	@Column(name=ICON_ID, nullable=false, precision=19)
	@Id
	@Authincrement
	private Long iconId;

	/**
	 * 图标名称.
	 */
	@Column(name=ICON_NAME, nullable=false, length=50)
	private String iconName;

	/**
	 * 图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型.
	 */
	@Column(name=ICON_TYPE, nullable=true, length=20)
	private String iconType;

	/**
	 * 图标内容，非base64图标内容可以在这里存储.
	 */
	@Column(name=ICON_VALUE, nullable=true, length=128)
	private String iconValue;

	/**
	 * 图标BASE64值，当值是base64编码时，在这里存储.
	 */
	@Column(name=ICON_BASE64, nullable=true, length=65535)
	private String iconBase64;

	/**
	 * 描述.
	 */
	@Column(name=REMARK, nullable=true, length=255)
	private String remark;

	/** 当前POJO对应表名称：{@value} */
	public static final String TABLE_NAME = "t_sys_icon";

	/** iconId 属性 对应的列名称：{@value}。 主键 */
	public static final String ICON_ID = "ICON_ID";

	/** iconName 属性 对应的列名称：{@value}。 图标名称 */
	public static final String ICON_NAME = "ICON_NAME";

	/** iconType 属性 对应的列名称：{@value}。 图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型 */
	public static final String ICON_TYPE = "ICON_TYPE";

	/** iconValue 属性 对应的列名称：{@value}。 图标内容，非base64图标内容可以在这里存储 */
	public static final String ICON_VALUE = "ICON_VALUE";

	/** iconBase64 属性 对应的列名称：{@value}。 图标BASE64值，当值是base64编码时，在这里存储 */
	public static final String ICON_BASE64 = "ICON_BASE64";

	/** remark 属性 对应的列名称：{@value}。 描述 */
	public static final String REMARK = "REMARK";

	public SysIcon() {
		
	}

	/**
	 * 设置 主键.
	 * @param iconId 主键
	 */
	public void setIconId(Long iconId) {
		this.iconId = iconId ;
	}

	/**
	 * 取得 主键.
	 * @return 主键
	 */
	@Column(name=ICON_ID, nullable=false, precision=19)
	@Id
	@Authincrement
	public Long getIconId() {
		return this.iconId ;
	}

	/**
	 * 设置 图标名称.
	 * @param iconName 图标名称
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName ;
	}

	/**
	 * 取得 图标名称.
	 * @return 图标名称
	 */
	@Column(name=ICON_NAME, nullable=false, length=50)
	public String getIconName() {
		return this.iconName ;
	}

	/**
	 * 设置 图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型.
	 * @param iconType 图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型
	 */
	public void setIconType(String iconType) {
		this.iconType = iconType ;
	}

	/**
	 * 取得 图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型.
	 * @return 图标类型,这个类型具体值由程序维护，但基本可以划分为：base64、字体库图标，url图标等类型
	 */
	@Column(name=ICON_TYPE, nullable=true, length=20)
	public String getIconType() {
		return this.iconType ;
	}

	/**
	 * 设置 图标内容，非base64图标内容可以在这里存储.
	 * @param iconValue 图标内容，非base64图标内容可以在这里存储
	 */
	public void setIconValue(String iconValue) {
		this.iconValue = iconValue ;
	}

	/**
	 * 取得 图标内容，非base64图标内容可以在这里存储.
	 * @return 图标内容，非base64图标内容可以在这里存储
	 */
	@Column(name=ICON_VALUE, nullable=true, length=128)
	public String getIconValue() {
		return this.iconValue ;
	}

	/**
	 * 设置 图标BASE64值，当值是base64编码时，在这里存储.
	 * @param iconBase64 图标BASE64值，当值是base64编码时，在这里存储
	 */
	public void setIconBase64(String iconBase64) {
		this.iconBase64 = iconBase64 ;
	}

	/**
	 * 取得 图标BASE64值，当值是base64编码时，在这里存储.
	 * @return 图标BASE64值，当值是base64编码时，在这里存储
	 */
	@Column(name=ICON_BASE64, nullable=true, length=65535)
	public String getIconBase64() {
		return this.iconBase64 ;
	}

	/**
	 * 设置 描述.
	 * @param remark 描述
	 */
	public void setRemark(String remark) {
		this.remark = remark ;
	}

	/**
	 * 取得 描述.
	 * @return 描述
	 */
	@Column(name=REMARK, nullable=true, length=255)
	public String getRemark() {
		return this.remark ;
	}
}