package com.gframework.samplecode;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

interface D{
	public void fun1();
	public void fun2();
}
class E implements D{
	public E(){
		System.out.println("######## new E");
	}

	@Override
	public void fun1() {
		System.out.println(" fun1 ###");
	}

	@Override
	public void fun2() {
		System.out.println(" fun2 ###");
		fun1();
	}
	
}

class C implements InvocationHandler{

	public Object t ;
	C(Object t){
		this.t = t;
	}
	public Object get(){
		return Proxy.newProxyInstance(C.class.getClassLoader(),t.getClass().getInterfaces(),this);
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("$$$ proxy - " + method.getName());
		return method.invoke(t, args);
	}
	
}

public class JavaProxyDemo {
	static E ee = new E();
	public static void main(String[] args) {
		
		C c = new C(new E());
		D d = (D) c.get();
		d.fun2();
	}
}
