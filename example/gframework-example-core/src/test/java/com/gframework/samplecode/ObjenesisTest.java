package com.gframework.samplecode;

import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;

public class ObjenesisTest {
	static class A {
		public A(String str,int i) {
			System.out.println("Hello");
		}
	}
	public static void main(String[] args) {
		Objenesis obj = new ObjenesisStd();
		A a = obj.getInstantiatorOf(A.class).newInstance();
		System.out.println(a);
	}
}
