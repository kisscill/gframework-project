package com.gframework.samplecode;

import java.lang.reflect.Modifier;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

/**
 * <pre>
 * CtClass需要关注的方法：
 * freeze : 冻结一个类，使其不可修改；
 * isFrozen : 判断一个类是否已被冻结；
 * prune : 删除类不必要的属性，以减少内存占用。调用该方法后，许多方法无法将无法正常使用，慎用；
 * defrost : 解冻一个类，使其可以被修改。如果事先知道一个类会被defrost， 则禁止调用 prune 方法；
 * detach : 将该class从ClassPool中删除；
 * writeFile : 根据CtClass生成 .class 文件；
 * toClass : 通过类加载器加载该CtClass。
 * 
 * CtMethod中的一些重要方法：
 * insertBefore : 在方法的起始位置插入代码；
 * insterAfter : 在方法的所有 return 语句前插入代码以确保语句能够被执行，除非遇到exception；
 * insertAt : 在指定的位置插入代码；
 * setBody : 将方法的内容设置为要写入的代码，当方法被 abstract修饰时，该修饰符被移除；
 * make : 创建一个新的方法。
 * </pre>
 * @author Ghwolf
 *
 */
public class JavassistTest {
	
	public static void main(String[] args) throws Exception{
		// 创建一个新的ClassPool，这样它可以被回收，同时它内部的CtClass也可以被回收，以避免内存溢出
		ClassPool cp = new ClassPool(true);
		CtClass cls = cp.makeClass("demo.BB");
		cls.addInterface(cp.get(AA.class.getName()));
		CtMethod f = new CtMethod(CtClass.voidType, "fun", null, cls);
		f.setModifiers(Modifier.PUBLIC);
		f.setBody("{"
				+ "System.out.println(\"Hello\");"
				+ "System.out.println(\"World\");"
				+ "}");
		cls.addMethod(f);
		AA a = (AA)cls.toClass().newInstance();
		a.fun();
	}

}
interface AA{
	public void fun();
}