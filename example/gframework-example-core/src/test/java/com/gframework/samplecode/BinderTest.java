package com.gframework.samplecode;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;

class AB {

	private AD b;
	private int i;

	public AD getB() {
		return this.b;
	}

	public void setB(AD b) {
		this.b = b;
	}

	public int getI() {
		return this.i;
	}

	public void setI(int i) {
		this.i = i;
	}

}

class AD {

	String s;

	public String getS() {
		return this.s;
	}

	public void setS(String s) {
		this.s = s;
	}

}

public class BinderTest {

	public static void main(String[] args) {
		Map<String,Object> map = new HashMap<>();
		map.put("a.i",123);
		MapPropertySource m = new MapPropertySource("ss",map);
		AB ab = Binder.get(new StandardEnvironment() {
			@Override
			protected void customizePropertySources(MutablePropertySources propertySources) {
				super.customizePropertySources(propertySources);
				propertySources.addLast(m);
			}
		}).bind("a", AB.class).get();
		System.out.println(ab.getI());
		System.out.println(ab.getB());
	}
}
