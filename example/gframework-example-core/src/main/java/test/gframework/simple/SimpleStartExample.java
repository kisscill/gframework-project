package test.gframework.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gframework.GframeworkSpringBootStart;

/**
 * 基础启动类样例，这是一个最简单也是功能最完善的启动类，同时你也可以在本项目中看到配置较为完整的application.yml配置文件
 * 
 * @since 2.0.0
 * @author Ghwolf
 */
@SpringBootApplication
public class SimpleStartExample {
	
	public static void main(String[] args) {
		SpringApplication.run(new Class[]{
				SimpleStartExample.class,
				GframeworkSpringBootStart.class
		},args);
	}

}
